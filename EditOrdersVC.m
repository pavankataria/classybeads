//
//  EditOrdersVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 14/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "EditOrdersVC.h"

@interface EditOrdersVC (){
    NSMutableArray *filteredOrderLineDataArray;
    BOOL noLoadingErrors;
    NSMutableArray *finalArray;
    NSMutableArray *allDiscountsTotalArray;
    OrderTotalFooterVC *detailTableFooter;

}

@end

@implementation EditOrdersVC
@synthesize orderTable = _orderTable;
@synthesize detailOrderTable = _detailOrderTable;
@synthesize serverRequest;

NSString *const RequestGetOrdersStateOne = @"Get/Orders";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"View Orders";

    // Do any additional setup after loading the view from its nib.
    [self setNavigationItems];
    filteredOrderLineDataArray = [[NSMutableArray alloc] init];
    finalArray = [[NSMutableArray alloc] init];
    allDiscountsTotalArray =[[NSMutableArray alloc] init];
    
    detailTableFooter = [[OrderTotalFooterVC alloc] init];
    [detailTableFooter.view setHidden:YES];
    [_detailOrderTable setTableFooterView:detailTableFooter.view];
}

-(void)viewDidAppear:(BOOL)animated{
    [self retrieveOrders];
}
-(void)setNavigationItems{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Orders";

    
//    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit Order" style:UIBarButtonItemStylePlain target:self action:@selector(editOrder)];
//    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editOrder)];//:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];

    UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(sendEmail)];//:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];

    UIBarButtonItem *emptyButton = [[UIBarButtonItem alloc] initWithTitle:@"  " style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationItem setRightBarButtonItems:@[editButton, emptyButton, emptyButton, emailButton] animated:YES];

    
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    

}
-(NSString*)getAttachment{
    NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
    OrdersDataModel *odm = [orderDataArray objectAtIndex:selectedIndex.row];

    NSString *readmePath = [[NSBundle mainBundle] pathForResource:@"invoiceHtmlDesign.html" ofType:nil];
    NSString *pdfAttachment = [NSString stringWithContentsOfFile:readmePath encoding:NSUTF8StringEncoding error:NULL];
    
    if(readmePath == nil){
        NSLog(@"Path not found");
    }
    else{
        NSLog(@"Path found %@", readmePath);
    }
    

//    NSString *welcomeMessage = [NSString stringWithFormat:@"Hi %@, thank you for your purchase. Please find attached an invoice for your order. If you have any queries, please do not hesitate to contact me on %@", odm.orderCustomerName, @"07716911849"];

    
    NSString *invoiceTableRows = [self getHTMLForInvoiceTableForOrder:odm];
    NSString *mainTable = [self getHTMLForMainTableForOrder:odm];

    pdfAttachment = [pdfAttachment stringByReplacingOccurrencesOfString:@"$invoiceTableRows" withString:invoiceTableRows];



    pdfAttachment = [pdfAttachment stringByReplacingOccurrencesOfString:@"$mainTable" withString:mainTable];

    NSLog(@"emailBody:\n\n%@", pdfAttachment);

    
    
    return pdfAttachment;
    
}
-(NSString*)getHTMLForInvoiceTableForOrder:(OrdersDataModel*)odm{
    NSMutableString *invoiceTableHTML = [[NSMutableString alloc] init];
    
    [invoiceTableHTML appendString:[NSString stringWithFormat:@"<tr><th>Order ID</th><td>%@</td></tr>", odm.orderReferenceNumber]];
    [invoiceTableHTML appendString:[NSString stringWithFormat:@"<tr><th>Order Date</th><td>%@</td></tr>", [odm getOrderCreatedDateWithDateFormatString:@"EEEE, d MMMM yyyy"]]];
    [invoiceTableHTML appendString:[NSString stringWithFormat:@"<tr><th>Customer Id</th><td>%@</td></tr>", odm.orderCustomerName]];
    return invoiceTableHTML;
}
-(NSString*)getHTMLForMainTableForOrder:(OrdersDataModel*)odm{
    NSMutableString *mainTableString = [[NSMutableString alloc] init];
    NSString *beginningOfTableHTMLString = @"<table id='orderTable'>";
    NSArray *headings =@[@"Qty", @"Code", @"Price", @"Total", @"Minus<br>Discount", @"Line<br>Total"];
    NSString *tableHeadersHTMLString = [AppConfig getHTMLForTableWithHeadings:headings];
    
    [mainTableString appendString:beginningOfTableHTMLString];
    [mainTableString appendString:tableHeadersHTMLString];
    NSMutableArray *orderLines = [self getOrderLinesForOrder:odm];
    for(int i = 1; i < [orderLines count]+1; i++){
        
        //Also input whether row is even or not for CSS style
        [mainTableString appendString:[[orderLines objectAtIndex:i-1] orderLineConvertToTableDataRowIsEven:((i%2 == 0)?YES:NO)]];
    }
    
    
    
    
    [mainTableString appendString:[NSString stringWithFormat:@"<tr rowspan='2'><td colspan='%d'></td><td></td><td></td><td></td></tr>", (int)([headings count]-3)]];
    NSString *endOfTableHTMLString = @"</table>";
    [mainTableString appendString:endOfTableHTMLString];

    
    
    return mainTableString;
}
-(NSString*)getEmailBody2{
    NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
    OrdersDataModel *odm = [orderDataArray objectAtIndex:selectedIndex.row];

    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:[self getAttachment]];
    
    NSString *readmePath = [[NSBundle mainBundle] pathForResource:@"style.html" ofType:nil];
    NSString *styleHTMLString = [NSString stringWithContentsOfFile:readmePath encoding:NSUTF8StringEncoding error:NULL];
    
    
    if(readmePath == nil){
        NSLog(@"Path not found");
    }
    else{
        NSLog(@"Path found %@", readmePath);
    }
    
    
    NSLog(@"styleOfHTMLString %@", styleHTMLString);
    NSString *startOfFileHTMLString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">%@</style></head><body>", styleHTMLString];
    
    
    NSString *beginningOfEmailBody = [NSMutableString stringWithFormat:@"<p><b>Hello %@,<br>here are your receipt details:<br> Order Reference #:</b> %@ <br><b>Purchase Date:</b> %@</p><br>", odm.orderCustomerName, odm.orderReferenceNumber, odm.orderCreatedDate];
    
    
    
    [emailBody appendString:startOfFileHTMLString];
    [emailBody appendString:beginningOfEmailBody];
    
    NSString *beginningOfTableHTMLString = @"<table style=\"width:300px\" cellspacing='0'>";
    
    
    NSString *tableHeadersHTMLString = [AppConfig getHTMLForTableWithHeadings:@[@"Qty", @"Code", @"Price", @"Total", @"Minus<br>Discount", @"Line<br>Total"]];
    
    [emailBody appendString:beginningOfTableHTMLString];
    [emailBody appendString:tableHeadersHTMLString];
    NSMutableArray *orderLines = [self getOrderLinesForOrder:odm];
    for(int i = 1; i < [orderLines count]+1; i++){
        
        //Also input whether row is even or not for CSS style
        [emailBody appendString:[[orderLines objectAtIndex:i-1] orderLineConvertToTableDataRowIsEven:((i%2 == 0)?YES:NO)]];
    }
    NSString *endOfTableHTMLString = @"</table>";
    [emailBody appendString:endOfTableHTMLString];
    
    NSString *endOfFileHTMLString = @"</body></html>";
    [emailBody appendString:endOfFileHTMLString];
    return emailBody;
}


-(void)sendEmail{
    if(![_orderTable indexPathForSelectedRow]){
        [SVProgressHUD showErrorWithStatus:@"Select an order to send a receipt" maskType:SVProgressHUDMaskTypeBlack];// afterDelay:1.5];
        return;
    }
    
    NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
    OrdersDataModel *odm = [orderDataArray objectAtIndex:selectedIndex.row];
    if([odm.orderCustomerEmail isEqual:[NSNull null]]){
        
//        [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"User: %@, does not have an email address, set email address for person in the customers panel", odm.orderCustomerName]];// afterDelay:1.5];
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"User: %@, does not have an email address, go to customers panel to set their email address", odm.orderCustomerName] maskType:SVProgressHUDMaskTypeBlack];

        NSLog(@"NO EMAIL FOR %@", odm.orderCustomerName);
        return;
    }
    
    NSString *pdfAttachment = [self getAttachment];
    self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:pdfAttachment pathForPDF:[@"~/Documents/invoiceReceipt.pdf" stringByExpandingTildeInPath]
                                            delegate:self
                                            pageSize:kPaperSizeA4
                                             margins:UIEdgeInsetsMake(10, 5, 10, 5)];

    
    
    NSString *emailTitle = [NSString stringWithFormat:@"Classy Beads Receipt: %@", odm.orderReferenceNumber];
//    NSString *messageBody = @"Hey, check this out!";
    NSArray *toRecipents = [NSArray arrayWithObject:odm.orderCustomerEmail];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    
    
    
    NSString *newFilePath = self.PDFCreator.PDFpath;
    
    NSData * pdfData = [NSData dataWithContentsOfFile:newFilePath];
    
    
    NSString *fileName = [NSString stringWithFormat:@"%@", odm.orderReferenceNumber];
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z0-9_]+" options:0 error:nil];
    fileName = [regex stringByReplacingMatchesInString:fileName options:0 range:NSMakeRange(0, fileName.length) withTemplate:@"-"];
    NSLog(@"source: %@\nnewFileName: %@", newFilePath, fileName);

    
    [mc addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"%@.pdf",fileName]];

    

    NSString *welcomeMessage = [NSString stringWithFormat:@"Hello %@,<br>Thank you for your purchase, please find attached an invoice for your order. If you have any queries, please do not hesitate to contact me on %@", odm.orderCustomerName, @"07716911849"];

    [mc setMessageBody:welcomeMessage isHTML:YES];
//    NSLog(@"EMAIL BODY:\n\n%@", emailBody);
    
    
    
    
    

    
    
//    // Determine the file name and extension
//    NSArray *filepart = [file componentsSeparatedByString:@"."];
//    NSString *filename = [filepart objectAtIndex:0];
//    NSString *extension = [filepart objectAtIndex:1];
//    
//    // Get the resource path and read the file using NSData
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
//    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
//    
//    // Determine the MIME type
//    NSString *mimeType;
//    if ([extension isEqualToString:@"jpg"]) {
//        mimeType = @"image/jpeg";
//    } else if ([extension isEqualToString:@"png"]) {
//        mimeType = @"image/png";
//    } else if ([extension isEqualToString:@"doc"]) {
//        mimeType = @"application/msword";
//    } else if ([extension isEqualToString:@"ppt"]) {
//        mimeType = @"application/vnd.ms-powerpoint";
//    } else if ([extension isEqualToString:@"html"]) {
//        mimeType = @"text/html";
//    } else if ([extension isEqualToString:@"pdf"]) {
//        mimeType = @"application/pdf";
//    }
//    
//    // Add attachment
//    [mc addAttachmentData:fileData mimeType:mimeType fileName:filename];
//    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}
                                  
                                  
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)editOrder{
    if([_orderTable indexPathForSelectedRow]){
        
        NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
        OrdersDataModel *odm = [[OrdersDataModel alloc] init];
        odm = [orderDataArray objectAtIndex:selectedIndex.row];
        
        [odm setOrderLinesDataArray:[self getOrderLinesForOrder:odm]];

        TakeOrderVC *takeOrderVC = [[TakeOrderVC alloc] initInOrderEditModeWithOrderObject:odm];
        
        [((PKNavigationController*)[self.sideMenuViewController mainViewController]) pushViewController:takeOrderVC animated:YES];
    }
    else{
        [SVProgressHUD showErrorWithStatus:@"Select an order to edit" maskType:SVProgressHUDMaskTypeBlack];
    }
    
}
-(void)show:(id)sender{
    //    [[[AppDelegate getAppDelegateReference] splitViewController] toggleMasterView:sender];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}


#pragma mark - ASIHTTPRequest Delegate methods
-(void)retrieveOrders{
    [SVProgressHUD showWithStatus:@"Getting orders" maskType:SVProgressHUDMaskTypeBlack];
//    [editOrdersSegmentControl setSelectedSegmentIndex:0];
//    [NoResultsView noResultsView:noResultsContainer
//              setContainerHidden:NO];
//    [NoResultsView noResultsView:noResultsContainer setCenterWithAutoCorrection:self.view.center];
//    [NoResultsView noResultsView:noResultsContainer hideTables:YES tables:_orderTable, _detailOrderTable, nil];
//    [NoResultsView noResultsView:noResultsContainer setContainerText:@"Loading orders"];
//    
//    
    [self retrieveWithRequestStringType:RequestGetOrdersStateOne withDictionary:nil];
    [filteredOrderLineDataArray removeAllObjects];
    [orderDataArray removeAllObjects];
//    [filteredOrderDataArray removeAllObjects];
    [_orderTable reloadData];
    [_detailOrderTable reloadData];
    
}


-(void)retrieveWithRequestStringType:(NSString*)typeOfRequest withDictionary:(NSMutableDictionary*)inputdictionary{

    
    NSLog(@"Retrieve %@ method called", typeOfRequest);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", RequestGetOrdersStateOne];
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    if([typeOfRequest isEqualToString:RequestGetOrdersStateOne]){
        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestGetOrdersStateOne forKey:@"RequestType"]];
    }
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
    
}

-(void)requestSucceeded:(ASIHTTPRequest*)request{
    
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"MY STRING:%@ ", myString);
    NSDictionary *JSONDictionary = [myString JSONValue];
    
    switch (statusCode) {
        case 400:
        case 401:
        {
            NSLog(@"display error message");
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error because: %@",[[request.responseString JSONValue] objectForKey:@"Message"]]];
            
            break;
        }
        case 200:
            NSLog(@"status code = 200 so successful");
            
            //orderRetrieval succeeded
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestGetOrdersStateOne]){
                //                [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
                [SVProgressHUD dismiss];
                noLoadingErrors = TRUE;
                [self setupOrderTableWithDictionary:[JSONDictionary objectForKey:@"orderResults"]];
                //[self setupFactoriesSVWithDictionary:[JSONDictionary objectForKey:@"factoriesResult"]];
                [self setupDetailedOrderArrayWithDictionary:[JSONDictionary objectForKey:@"detailedOrderResults"]];
//                [orderTableRefreshControl endRefreshing];
            }
            
            break;
        default:
            NSLog(@"went to none: %d or %@", statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]);
            break;
    }
}
-(void)requestFailed:(ASIHTTPRequest *)request{
    [SVProgressHUD dismiss];

    if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestGetOrdersStateOne]){
        //############# check internet connection over here
        NSLog(@"FLOPPED: %@, so trying again", RequestGetOrdersStateOne);
        [self retrieveOrders];
    }
}


-(void)setupDetailedOrderArrayWithDictionary:(NSDictionary*)inputJSONDictionary{
    //NSLog(@"detailed order array: %@", inputJSONDictionary);
    if(detailOrderDataArray == nil){
        detailOrderDataArray = [[NSMutableArray alloc] init];
    }
    [detailOrderDataArray removeAllObjects];
    [filteredOrderLineDataArray removeAllObjects];
    
    for(id key in inputJSONDictionary){
        OrderLineDataModel *odm = [[OrderLineDataModel alloc] initWithJSONData:key];
        [detailOrderDataArray addObject:odm];
    }
}
-(void)setupOrderTableWithDictionary:(NSDictionary*)inputJSONDictionary{
    NSLog(@"ORDER ARRAY: %@", inputJSONDictionary);
    if(orderDataArray == nil){
        orderDataArray = [[NSMutableArray alloc] init];
    }
    [orderDataArray removeAllObjects];
    
    for(id key in inputJSONDictionary){
        OrdersDataModel *odm = [[OrdersDataModel alloc] initWithJSONData:key];
        [orderDataArray addObject:odm];
    }
    
    [_orderTable reloadData];
    //so that any data on the right is also reloaded with new data thats come in from above.
    [_detailOrderTable reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UITableView Data Source

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"cell at indexPath: %d became deselected", indexPath.row);
    if(tableView == _orderTable){
        AgentDisplayCell *cell = (AgentDisplayCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        CATransition *animation = [CATransition animation];
        animation.duration = 0.6;
        animation.type = kCATransitionFade;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [cell.agentBalanceLabel.layer addAnimation:animation forKey:@"changeTextTransition"];
        
        // Change the text
        cell.agentBalanceLabel.text = @"";
        OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];
        [odm setStateSelected:NO];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    int section = 0;
    if(tableView == _orderTable){
        section = 1;
    }
    else if(tableView == _detailOrderTable){
        section = [finalArray count];
        if(section > 0){
            [detailTableFooter.view setHidden:NO];
        }
        else{
            [detailTableFooter.view setHidden:YES];

        }
        
    }
    else{
        section = 0;
    }
    return section;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows = 0;
    if(tableView == _orderTable){
        rows = [orderDataArray count];
    }
    else if(tableView == _detailOrderTable){
        if(section == 0){
            rows = [[finalArray objectAtIndexSafe:section] count];
        }
        else{
            rows = [[finalArray objectAtIndexSafe:section] count]+1;
        }
    }
    else{
        rows = 0;
    }
    return rows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        if([orderDataArray count] == 0){
            static NSString *cellID = @"loadingcell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            if(indexPath.row == 3){
                if(noLoadingErrors){
                    cell.textLabel.text = [NSString stringWithFormat:@"No orders are pending"];
                }
                else{
                    cell.textLabel.text = [NSString stringWithFormat:@"Loading Orders"];
                }
                [cell.textLabel setBackgroundColor:[UIColor clearColor]];
                
                //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            return cell;
        }
        else{
            static NSString *fellID = @"productcell";

            AgentDisplayCell *orderDisplayCell = (AgentDisplayCell *)[tableView dequeueReusableCellWithIdentifier:fellID];
            if ( ! orderDisplayCell)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AgentDisplayCell" owner:self options:nil];
                orderDisplayCell = [nib objectAtIndex:0];

                [orderDisplayCell.agentNameLabel setTextColor:[UIColor blackColor]];
                [orderDisplayCell.agentNameLabel setFont:[UIFont fontWithName:@"CartoGothicStd-Bold" size:23.0]];

                CGRect bF = [orderDisplayCell.agentBalanceLabel frame];
                [orderDisplayCell.agentBalanceLabel setFrame:CGRectMake(bF.origin.x, 30, bF.size.width, orderDisplayCell.agentStateLabel.frame.size.height)];
                [orderDisplayCell.agentBalanceLabel setFont:[UIFont systemFontOfSize:20]];
                
                orderDisplayCell.agentBalanceLabel.adjustsFontSizeToFitWidth = YES;
                orderDisplayCell.agentNameLabel.adjustsFontSizeToFitWidth = YES;
                
                
                [orderDisplayCell.agentNameLabel setHighlightedTextColor:APP_COLOR_WHITE];
                [orderDisplayCell.agentBalanceLabel setHighlightedTextColor:APP_COLOR_WHITE];
                
                orderDisplayCell.selectedBackgroundView = [PreferencesConfig7 getDefaultSelectedBackgroundView];
                [orderDisplayCell setBackgroundColor:[UIColor whiteColor]];//
                
            }
            OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];

            orderDisplayCell.agentNameLabel.text = odm.orderReferenceNumber;
            orderDisplayCell.agentStateLabel.text = @"";
            [orderDisplayCell.agentStateLabel setTextColor:[UIColor darkGrayColor]];
            
            orderDisplayCell.agentCardinalDirectionLabel.text = @"";

            [orderDisplayCell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            
            if([odm stateSelected]){
                orderDisplayCell.agentBalanceLabel.text = [NSString stringWithFormat:@"£%@", odm.orderTotalAmount];
            }
            else{
                orderDisplayCell.agentBalanceLabel.text = @"";
            }
            

//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:fellID];
            
//            if (!cell) {
//                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:fellID];
//                
//                [cell.textLabel setFont:[UIFont fontWithName:@"CartoGothicStd-Bold" size:23.0]];
//                [cell.textLabel setTextColor:[UIColor blackColor]];
//                cell.textLabel.adjustsFontSizeToFitWidth = YES;
//                [cell.textLabel setHighlightedTextColor:APP_COLOR_WHITE];
//                cell.selectedBackgroundView = [PreferencesConfig7 getDefaultSelectedBackgroundView];
//                [cell setBackgroundColor:[UIColor whiteColor]];
//            }
//            [cell.textLabel setText:odm.orderReferenceNumber];
            return orderDisplayCell;
        }
    }
    else if(tableView == _detailOrderTable){
        
        
        NSLog(@"final array coount %d", [finalArray count]);
        if([finalArray count] == 0){
            static NSString *cellID = @"loadingdetailcell";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            if(indexPath.row == 2){
                cell.textLabel.text = [NSString stringWithFormat:@"Select an order to view"];
                [cell.textLabel setBackgroundColor:[UIColor clearColor]];
                //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@""];
                [cell.textLabel setBackgroundColor:[UIColor clearColor]];
            }
            return cell;
        }
        else{
            
            if([finalArray count] > 0){
                NSArray *carray = [finalArray objectAtIndex:indexPath.section];
                if(indexPath.row < [carray count]){

//                if(indexPath.row != [finalArray indexOfObject:[[finalArray objectAtIndex:indexPath.section] lastObject]]){
//                    
                    
                    OrderLineDataModel *oldm = [[finalArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
                    
                    static NSString *cellID = @"CellIdentifier33";
                    
                    OrderLineCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
                    if (cell == nil)
                    {
                        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderLineCell" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        //                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    }
                    // ... set up the cell here ...
                    
                    //            [cell setOrderLineQuantityValue:[NSString stringWithFormat:@"%d", [oldm.orderLineQuantity integerValue]]];
                    //            [cell setOrderLineCodeValue:[NSString stringWithFormat:@"%@", oldm.orderLineCode]];
                    //            [cell setOrderLinePriceValue:[NSString stringWithFormat:@"%.2f", [oldm.orderLinePrice floatValue]]];
                    //            [cell setOrderLineTotalValue:[NSString stringWithFormat:@"= %.2f", [oldm.orderLineOriginalTotal floatValue]]];
                    
                    
                    
                    
                    
                    [cell setOrderLineQuantityValue:[NSString stringWithFormat:@"%@", oldm.orderLineQuantity]];
                    [cell setOrderLineCodeValue:oldm.orderLineCode];
                    
                    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
                    [fmt setPositiveFormat:@"0.##"];
                    [cell setOrderLinePriceValue:[NSString stringWithFormat:@"£%@", oldm.orderLinePrice]];
                    [cell setOrderLineTotalValue:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:oldm.orderLineOriginalTotal]]];
                    [cell setOrderLineDiscountTypeValue:oldm.orderLineDiscountDescription];
                    [cell setOrderLineLineTotalValue:[NSString stringWithFormat:@"%.2f", [oldm.orderLineFinalTotalPrice floatValue]]];
                    return cell;
                }
                else{
                    static NSString *CellIdentifier = @"TotalCrossDiscountCell";
                    TotalCrossDiscountCell *cell = (TotalCrossDiscountCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    cell = (TotalCrossDiscountCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TotalCrossDiscountCell" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                    }
                    NSLog(@"discountsTotalArray: %@", allDiscountsTotalArray);
                    NSLog(@"discounts Count %d", [allDiscountsTotalArray count]);
                    
                    TotalCrossDiscountDataModel *currentOLDM = [allDiscountsTotalArray objectAtIndex:indexPath.section-1];
                    [cell setTCDTotalDiscountValue:currentOLDM.tCDTotalDiscountValue];
                    [cell setTCDTotalFinalValue:currentOLDM.tCDTotalFinalValue];
                    [cell setTCDTotalQuantityValue:currentOLDM.tCDTotalQuantityValue];
                    return cell;
                }
            }
            return  nil;
        }
    }
    else{
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        return 70;
    }
    else{
        return 50;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == _orderTable){
        return [PreferencesConfig7 tableView:tableView heightForHeaderInSection:section];
    }
    else if(tableView == _detailOrderTable){
        if(section == 0){
            return 50;
        }
        else return 25;
    }else{
        return 0;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(tableView == _orderTable){
        return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section withFontSize:APP_DEFAULT_HEADER_FONT_SIZE];
    }
    else{
        return nil;//[PreferencesConfig7 tableView:tableView editOrderViewForHeaderInSection:section];

    }
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(tableView == _orderTable) return @"Orders";
    else if(tableView == _detailOrderTable){
        if(section == 0){
            return @"Order Items";
        }
        return @"Discounts";
    }
    else return @"ERROR CONTACT PAVAN";
}

#pragma mark - UITableView Delegate Methods
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        
        if([_orderTable indexPathForSelectedRow]){
            NSIndexPath *selectedIndexPath = [_orderTable indexPathForSelectedRow];
            if(selectedIndexPath.row == indexPath.row){
                return nil;
            }
            return indexPath;
        }
        else{
            return indexPath;
        }

    }
    
    else{
        return indexPath;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _orderTable){
        if([orderDataArray count] > 0){
         
            OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];
            [self animateAgentBalance:YES withTable:tableView indexPath:indexPath];

//            [self enableDeleteButton:YES];
            
            [filteredOrderLineDataArray removeAllObjects];
            
            filteredOrderLineDataArray = [self getOrderLinesForOrder:odm];
            
            //NSLog(@"filtered order line data array: %@", filteredOrderLineDataArray);
            
            finalArray = [[AppConfig setupReceiptArrayWithArray:filteredOrderLineDataArray] mutableCopy];
            allDiscountsTotalArray = [[NSMutableArray alloc] initWithArray:[AppConfig getDiscountsArrayFromFinalArray:finalArray]];

            [_detailOrderTable reloadData];
            [AppConfig calculateTotalWithItemsWithArray:finalArray withAllDiscountsArray:allDiscountsTotalArray withOrderTotalFooter:detailTableFooter];
            /*[_detailOrderTable beginUpdates];
            [_detailOrderTable deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
            [_detailOrderTable insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            [_detailOrderTable endUpdates];
            */
        }
    }
}
-(void)animateAgentBalance:(BOOL)shouldAnimate withTable:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath{
    if(shouldAnimate){
        OrdersDataModel *odm = [orderDataArray objectAtIndex:indexPath.row];

        [odm setStateSelected:YES];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setPositiveFormat:@"###,###,###,###,##0"];
        NSLog(@"selected agent: %@ with %@ balance", odm.orderCustomerName, odm.orderTotalAmount);
//        NSInteger ba = [odm.orderTotalAmount floatValue];
//        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        [numberFormatter setMaximumFractionDigits:2];
        [numberFormatter setPaddingPosition:NSNumberFormatterPadBeforePrefix];
        [numberFormatter setPaddingCharacter:@"0"];
        [numberFormatter setMinimumIntegerDigits:10];
        NSString *finalStringBalance = [NSString stringWithFormat:@"£   %.02f", [[numberFormatter numberFromString:odm.orderTotalAmount] floatValue]];
//        NSLog(@"NUMBER : %@",[numberFormatter numberFromString:odm.orderTotalAmount]);
        NSLog(@"final string balance: %@", finalStringBalance);
        AgentDisplayCell *cell = (AgentDisplayCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        
        CATransition *animation = [CATransition animation];
        animation.duration = 1.0;
        animation.type = kCATransitionFade;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [cell.agentBalanceLabel.layer addAnimation:animation forKey:@"changeTextTransition"];
        
        // Change the text
        cell.agentBalanceLabel.text = finalStringBalance;
        
    }
}

-(NSMutableArray*)getOrderLinesForOrder:(OrdersDataModel*)odm{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for(OrderLineDataModel *o in detailOrderDataArray){
        if(odm.orderId == o.orderLineOrderId){
            [tempArray addObject:o];
        }
    }
    return tempArray;
}





//==========================================================
#pragma mark NDHTMLtoPDFDelegate
//==========================================================

- (void)HTMLtoPDFDidSucceed:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
    NSLog(@"%@",result);
//    self.resultLabel.text = result;
}

- (void)HTMLtoPDFDidFail:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
    NSLog(@"%@",result);
//    self.resultLabel.text = result;
}

@end
