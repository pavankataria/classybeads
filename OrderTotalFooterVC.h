//
//  OrderTotalFooterVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 07/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTotalFooterVC : UIViewController{
    IBOutlet UILabel *totalItemsLabel;
    IBOutlet UILabel *discountTotalCostLabel;
    IBOutlet UILabel *totalCostLabel;
    IBOutlet UILabel *subTotalCostLabel;
    
}

//@property (nonatomic, assign) NSUInteger totalItems;
//@property (nonatomic, assign) CGFloat totalCost;
//@property (nonatomic, assign) CGFloat totalCost;


-(void)setTotalItems:(NSUInteger)total;
-(void)setTotalCost:(CGFloat)total;
-(void)setDiscountTotal:(CGFloat)discountTotal;
-(void)setSubTotalCost:(CGFloat)discountTotal;

-(CGFloat)getTotalCost;
@end