//
//  PKTableViewController.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerDataModel.h"
#import "OrdersDataModel.h"

#import "NoResultsCell.h"

#import "CustomArrowAccessoryView.h"
#import "AppDelegate.h"

@protocol ParentTableViewControllerDelegate <NSObject>

-(void)retrieveDataSetAgain;

@end

@protocol ParentTableViewControllerDelegate;

@interface PKTableViewController : UITableViewController{
//    id <ParentTableViewControllerDelegate> delegate;
    
    NSString *requestType;
}

//@property (weak, nonatomic) IBOutlet UITableView *customersTable;

@property (nonatomic, weak) id<ParentTableViewControllerDelegate> delegate;


@property (strong, nonatomic) UIRefreshControl *customerTableRefreshControl;

@property (retain, nonatomic) ASIHTTPRequest *serverRequest;
@property (retain, nonatomic) NSMutableArray *originalDataSetArray;
@property (retain, nonatomic) NSMutableDictionary *customerTableIndices;

//the output array after predicates and searches have been implemented on the original array
@property (retain, nonatomic) NSMutableArray *currentArrayDisplayed;
@property (retain, nonatomic) NSMutableArray *dataSetFilteredArray;
@property (assign) BOOL loading;

-(NSMutableAttributedString*)getRefreshControlAttributedStringDefaultWithString:(NSString *)s;
-(void)setRefreshControlToDefault;
-(void)setupTableWithDictionary:(NSDictionary*)inputJSONDictionary withRequest:(NSString*)request;
-(void)setupOrderTableWithDictionary:(NSDictionary*)inputJSONDictionary;
@end