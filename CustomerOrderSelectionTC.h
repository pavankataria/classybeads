//
//  CustomerOrderSelectionTC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PKTableViewController.h"
#import "TakeOrderVC.h"
#import "TakeOrderVersion2VC.h"
@interface CustomerOrderSelectionTC : PKTableViewController

@property (retain, nonatomic) ASIHTTPRequest *serverRequest;
@end
