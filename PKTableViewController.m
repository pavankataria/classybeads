//
//  PKTableViewController.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKTableViewController.h"

@interface PKTableViewController ()

@end

@implementation PKTableViewController
@synthesize originalDataSetArray;
@synthesize dataSetFilteredArray;
@synthesize customerTableIndices;
@synthesize customerTableRefreshControl;
@synthesize currentArrayDisplayed;
@synthesize serverRequest;
@synthesize loading;
//@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSMutableAttributedString*)getRefreshControlAttributedStringDefaultWithString:(NSString *)s{
    NSMutableAttributedString *a = [[NSMutableAttributedString alloc] initWithString:s];
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: APP_COLOR_WHITE,
                                        };
    [a setAttributes:refreshAttributes range:NSMakeRange(0, a.length)];
    
    return a;
}
-(void)setRefreshControlToDefault{
    customerTableRefreshControl.attributedTitle = [self getRefreshControlAttributedStringDefaultWithString:@"Refresh Data"];
    
}

-(void)setupOrderTableWithDictionary:(NSDictionary*)inputJSONDictionary{
//    NSLog(@"ORDER ARRAY: %@", inputJSONDictionary);
    if(originalDataSetArray == nil){
        originalDataSetArray = [[NSMutableArray alloc] init];
    }
    if(dataSetFilteredArray == nil){
        dataSetFilteredArray = [[NSMutableArray alloc] init];
    }
    [dataSetFilteredArray removeAllObjects];
    [originalDataSetArray removeAllObjects];
    
    for(id key in inputJSONDictionary){
        OrdersDataModel *odm = [[OrdersDataModel alloc] initWithJSONData:key];
        NSLog(@"ref: %@", odm.orderReferenceNumber);
        [originalDataSetArray addObject:odm];
        [dataSetFilteredArray addObject:odm];
    }
    [self.tableView reloadData];
    //so that any data on the right is also reloaded with new data thats come in from above.
//    [_detailOrderTable reloadData];
}


-(void)setupTableWithDictionary:(NSDictionary*)inputJSONDictionary withRequest:(NSString*)request {
//    [self.tableView setBackgroundColor:[UIColor clearColor]];
    requestType = request;
    if (originalDataSetArray == nil) {
        originalDataSetArray = [[NSMutableArray alloc] init];
    }
    [originalDataSetArray removeAllObjects];
    
    [self.tableView reloadData];
    
    
    NSMutableArray *unsortedDataSetArray = [[NSMutableArray alloc] init];
    
    for(id key in inputJSONDictionary){
        if([requestType isEqualToString:GetOrdersRequest]){
            OrdersDataModel *currentDataModel = [[OrdersDataModel alloc] initWithJSONData:key];
//            [unsortedDataSetArray addObject:currentDataModel];
        }
        else if([requestType isEqualToString:GetCustomersRequest2]){
//            NSLog(@"wooo");
            CustomerDataModel *currentDataModel = [[CustomerDataModel alloc] initWithJSONData:key];
            [unsortedDataSetArray addObject:currentDataModel];
        }
    }
    
    //##############################
    //SORT ARRAY
    NSSortDescriptor *sortDescriptor;
    
   
    if([requestType isEqualToString:GetCustomersRequest2]){
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"customerName" ascending:YES];
    
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        originalDataSetArray = [[unsortedDataSetArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
        
        [self setTablePropertiesWithArray:originalDataSetArray];
            
    }
    
}
-(void)setTablePropertiesWithArray:(NSMutableArray*)inputArray{
    currentArrayDisplayed = inputArray;
    NSLog(@"set table properties with array %@", inputArray);
    //##############################
    //CREATE INDICES
    // Loop through the books and create our keys
    if (customerTableIndices == nil) {
        customerTableIndices = [[NSMutableDictionary alloc] init];
    }
    [customerTableIndices removeAllObjects];
    
    BOOL found;
    for (CustomerDataModel *agent in currentArrayDisplayed){
        NSString *c = [agent.customerName substringToIndex:1];
        found = NO;
        
        for (NSString *str in [customerTableIndices allKeys]){
            if ([str isEqualToString:c]){
                found = YES;
                break;
            }
        }
        if (!found){
            NSLog(@"character: %@", c);
            [customerTableIndices setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    //##############################
    //POPULATE INDICES
    for (CustomerDataModel *customer in currentArrayDisplayed){
        [[customerTableIndices objectForKey:[customer.customerName substringToIndex:1]] addObject:customer];
    }
    
    //    NSLog(@"Agent index: %@", agentTableIndices);
    
    if([currentArrayDisplayed count] == 0){
        //        [NoResultsView  noResultsView:noResultsContainer showNoDataImageFound:YES withText:@"No agents" forTable:_agentsTable];
    }
    else{
        //        [NoResultsView hideNoResultsContainer:noResultsContainer withTableView:_agentsTable];
    }
    [self.tableView reloadData];
}

#pragma mark - UITableView data source methods

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //    NSLog(@"a");
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if([dataSetFilteredArray count] > 0){
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if([currentArrayDisplayed count] > 0){
            return [customerTableIndices count];
        }
        //if no data loaded return 1 section
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [PreferencesConfig7 tableView:tableView heightForHeaderInSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    //    return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section];
    return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section withFontSize:APP_DEFAULT_HEADER_FONT_SIZE];
    
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //    NSLog(@"b");
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    else{
        if([currentArrayDisplayed count] > 0){
            return [[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        }
        else{
            return 0;
        }
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"c");
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if(dataSetFilteredArray.count > 0){
            return [dataSetFilteredArray count];
        }
    }
    else{
        if([currentArrayDisplayed count] > 0){
            //return [agentsDataArray count];
            return [[customerTableIndices valueForKey:[[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
        }
        //if no data loaded display 3 cells so we can display loading cell near the middle of the table
    }
    return 3;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    else{
        if([currentArrayDisplayed count] > 0){
            //http://jomnius.blogspot.co.uk/2012/02/how-to-get-magnifying-glass-into.html
            NSMutableArray *indexList = [NSMutableArray arrayWithCapacity:[[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] count]+1];
            
            [indexList addObject:UITableViewIndexSearch];
            for (NSString *item in [[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]){
                
                [indexList addObject:[item substringToIndex:1]];
            }
            return indexList;
        }
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    if (index == 0)
    {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    return index-1; // due magnifying glass icon
}


#pragma mark - UITableView delegate methods



-(void)setupExtraVariables{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    customerTableRefreshControl = refreshControl;
    
    refreshControl.tintColor = APP_COLOR_WHITE;
    //    NSMutableAttributedString *a = [[NSMutableAttributedString alloc] initWithString:s];
    //    [a addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSRangeFromString(s)];
    //    refreshControl.attributedTitle = a;
    
    [self setRefreshControlToDefault];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    
    [refreshControl setBackgroundColor:APP_COLOR_MAIN];
    [self.tableView addSubview:customerTableRefreshControl];
    
    //    noResultsContainer = [[NoResultsView alloc] initWithPosition:_agentsTable.bounds withText:@"Loading" isHidden:NO];
    //    [NoResultsView noResultsView:noResultsContainer showNoDataImageFound:YES withText:@"Loading" forTable:_agentsTable];
    //
    //    [self.view addSubview:noResultsContainer];
    
}
-(void)handleRefresh:(id)sender{
    customerTableRefreshControl.attributedTitle = [self getRefreshControlAttributedStringDefaultWithString:@"Refreshing data"];
    
    NSLog(@"refreshing orders");
    //    noLoadingErrors = false;
    
    
    if([self.delegate respondsToSelector:@selector(retrieveDataSetAgain)]){
        [self.delegate retrieveDataSetAgain];
    };
    
}

@end
