//
//  AddCustomerVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 15/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "AddCustomerVC.h"
//#import <QuartzCore/QuartzCore.h>
@interface AddCustomerVC (){
    BOOL EDIT_MODE;
    NSString *_memberName;
    NSString *_memberPhone;
    NSString *_memberEmail;
}

@end

@implementation AddCustomerVC
@synthesize serverRequest;
@synthesize delegate;

NSString *const RequestCreateCustomer = @"Create/Customer";
NSString *const RequestUpdateCustomer = @"Update/Customer";
NSString *const RequestDeleteCustomer = @"Delete/Customer";

-(id)initWithCustomerObject:(CustomerDataModel*)customerObject{
    self = [super init];
    if(!self) return nil;
    EDIT_MODE = YES;
    _memberName = customerObject.customerName;
    _memberPhone = [customerObject.customerPhone isEqual:[NSNull null]] ? @"" : customerObject.customerPhone;
    _memberEmail = [customerObject.customerEmail isEqual:[NSNull null]] ? @"" : customerObject.customerEmail;
    customer = customerObject;
    
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self setNavigationButtons];
    self.title = @"Add Customer Details";
//    [self.view.layer setCornerRadius:20.0];
//    [self.view.layer setMasksToBounds:YES];
//    self.view.layer.opaque = NO;
    
    memberName = [[PkPlaceHolderTextField alloc] initWithFrame:CGRectMake(93, 77, 358, 60)];
    memberPhone = [[PkPlaceHolderTextField alloc] initWithFrame:CGRectMake(93, 166, 358, 60)];
    memberEmail =  [[PkPlaceHolderTextField alloc] initWithFrame:CGRectMake(93, 255, 358, 60)];
    
    [memberName setFont:[UIFont fontWithName:@"CartoGothicStd-Book" size:30.0]];
    [memberPhone setFont:[UIFont fontWithName:@"CartoGothicStd-Book" size:30.0]];
    [memberEmail setFont:[UIFont fontWithName:@"CartoGothicStd-Book" size:30.0]];
    
    [memberName setAutocorrectionType:UITextAutocorrectionTypeNo];
    [memberEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    memberName.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    if(EDIT_MODE){
        [memberName setPlaceholder:@"Full name"];
        [memberPhone setPlaceholder:@"Phone number not set yet"];
        [memberEmail setPlaceholder:@"Email not set yet"];
    }
    else{
        [memberName setPlaceholder:@"Full name"];
        [memberPhone setPlaceholder:@"Phone number"];
        [memberEmail setPlaceholder:@"Email"];
    }
    [memberPhone setKeyboardType:UIKeyboardTypeNumberPad];
    [memberEmail setKeyboardType:UIKeyboardTypeEmailAddress];

    
    [memberName setTextColor:APP_COLOR_WHITE];//Creates a lighter shade50*1/4*3/2, 156*1/2*3/2, 210*3/4*3/2, 255)];
    [memberPhone setTextColor:APP_COLOR_WHITE];//Creates a lighter shade50*1/4*3/2, 156*1/2*3/2, 210*3/4*3/2, 255)];
    [memberEmail setTextColor:APP_COLOR_WHITE];//Creates a lighter shade50*1/4*3/2, 156*1/2*3/2, 210*3/4*3/2, 255)];

    
    [memberName setDelegate:self];
    [memberPhone setDelegate:self];
    [memberEmail setDelegate:self];
    
    [self addStyleToButton:_customerActionButton];
    [self addStyleToButton:_cancelButton];
    
    [self addLineBelowLabel:memberName];
    [self addLineBelowLabel:memberEmail];
    [self addLineBelowLabel:memberPhone];
    
    
    
    [self.view addSubview:memberName];
    [self.view addSubview:memberEmail];
    [self.view addSubview:memberPhone];
    
    
    if(EDIT_MODE){
        NSLog(@"Customer edit mode");
        
        NSLog(@"n: %@ p: %@ e: %@", _memberName, _memberPhone, _memberEmail);
        [memberName setText:_memberName];
        [memberPhone setText:_memberPhone];
        [memberEmail setText:_memberEmail];

    }
}
-(void)setNavigationButtons{
    UIBarButtonItem *customerActionButton;
    UIBarButtonItem *deleteCustomerButton;
    UIBarButtonItem *emptyButton = [[UIBarButtonItem alloc] initWithTitle:@"  " style:UIBarButtonItemStylePlain target:self action:@selector(emptyCall)];

    if(EDIT_MODE){
        
        customerActionButton = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStylePlain target:self action:@selector(updateCustomerRequest) ];
        deleteCustomerButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(confirmDeleteCustomer) ];
        [self.navigationItem setRightBarButtonItems:@[customerActionButton, emptyButton, deleteCustomerButton] animated:YES];
    }
    else{
        customerActionButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addCustomerRequest)];
        [self.navigationItem setRightBarButtonItems:@[customerActionButton] animated:YES];

    }
    //    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(cancelModalView)];
}
-(void)emptyCall{
    
}
-(void)confirmDeleteCustomer{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Customer" message:[NSString stringWithFormat:@"Are you sure you want to delete customer: %@? Yes to delete, No to cancel ", customer.customerName] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 20;
    alert.delegate = self;
    [alert show];
}
-(void)updateCustomerRequest{
    if([[memberName text] length] <= 2){
        [SVProgressHUD showErrorWithStatus:@"Name is too short" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    else{
        [SVProgressHUD showWithStatus:@"Updating member" maskType:SVProgressHUDMaskTypeBlack];
        [self retrieveWithRequestStringType:RequestUpdateCustomer withDictionary:nil];
    }

}


-(void)addCustomerRequest{
    if([[memberName text] length] <= 2){
        [SVProgressHUD showErrorWithStatus:@"Name is too short" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    else{
        [SVProgressHUD showWithStatus:@"Adding member" maskType:SVProgressHUDMaskTypeBlack];
        [self retrieveWithRequestStringType:RequestCreateCustomer withDictionary:nil];
    }
}
-(void)retrieveWithRequestStringType:(NSString*)typeOfRequest withDictionary:(NSMutableDictionary*)inputdictionary{

    NSLog(@"loading = TRUE");
    NSLog(@"Retrieve %@ method called", typeOfRequest);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", typeOfRequest];
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    NSMutableDictionary *completeDataArray = [[NSMutableDictionary alloc] init];

    if([typeOfRequest isEqualToString:RequestCreateCustomer]){
        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestCreateCustomer forKey:@"RequestType"]];
        if( ! [self validateAndPrepareAllData:&completeDataArray]){
            return;
        }
    }
    else{
        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestUpdateCustomer forKey:@"RequestType"]];
        if( ! [self validateAndPrepareAllData:&completeDataArray]){
            return;
        }
    }


    NSString *jsonString = [completeDataArray JSONRepresentation];
    NSLog(@"Customer object: %@", jsonString);
    
    
    [serverRequest appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];

    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
}



-(BOOL)validateAndPrepareCustomerIdData:(NSMutableDictionary**)myCompleteDataArray{
    NSLog(@"showing customer id : %d", (int)customer.customerId);
    [*myCompleteDataArray setObject:[NSNumber numberWithInteger:customer.customerId] forKey:@"customerid"];
    return YES;
}
-(BOOL)validateAndPrepareAllData:(NSMutableDictionary**)myCompleteDataArray{
    [*myCompleteDataArray setObject:[memberName text] forKey:@"customername"];
    [*myCompleteDataArray setObject:[NSNumber numberWithInteger:customer.customerId] forKey:@"customerid"];
    if([[memberPhone text] length] > 0){
        [*myCompleteDataArray setObject:[memberPhone text] forKey:@"customerphone"];
    }
    else{
        [*myCompleteDataArray setObject:@"" forKey:@"customerphone"];
        
    }
    if([[memberEmail text] length] > 0){
        if([PreferencesConfig7 NSStringIsValidEmail:[memberEmail text]]){
            [*myCompleteDataArray setObject:[memberEmail text] forKey:@"customeremail"];
        }
        else{
            [SVProgressHUD dismissWithError:@"Email address is not valid" afterDelay:2];
            return FALSE;
        }
    }
    else{
        [*myCompleteDataArray setObject:@"" forKey:@"customeremail"];
    }
    return YES;
}

-(void)requestSucceeded:(ASIHTTPRequest*)request{
    NSLog(@"loading = false");
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"my string: %@", myString);
    NSDictionary *JSONDictionary = [myString JSONValue];
    
    switch (statusCode) {
        case 400:
        case 401:
        {
            NSLog(@"display error message");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[request.responseString JSONValue] objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            break;
        }
        case 200:
            NSLog(@"status code = 200 so successful");
            
            //statesRetrieval succeeded
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestCreateCustomer]){
                [self.delegate addedCustomerVCWithCustomer:[[CustomerDataModel alloc] initWithJSONData:[JSONDictionary objectForKey:@"CustomerObject"]]];
                [self cancelModalView];

            }
            else if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestUpdateCustomer]){
                [self.delegate editedCustomerVCWithCustomer:[[CustomerDataModel alloc] initWithJSONData:[JSONDictionary objectForKey:@"CustomerObject"]]];
                [self cancelModalView];
            }
            else if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestDeleteCustomer]){
                [self.delegate deletedCustomer:customer];

                [self cancelModalView];
            }
            break;
        default:
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"went to none: %d or %@", (int)statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]]];
            NSLog(@"went to none: %d or %@", (int)statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]);
            break;
    }
    [SVProgressHUD dismiss];
}
-(void)requestFailed:(ASIHTTPRequest*)request{
    NSLog(@"loading = false");
    if([[[request userInfo] objectForKey:@"RquestType"] isEqualToString:RequestCreateCustomer]){
        NSLog(@"retrieving states failed so trying again");
        [self addCustomerRequest];
    }
}

-(void)addStyleToButton:(UIButton*)button{
    [[button titleLabel] setFont:[UIFont fontWithName:@"CartoGothicStd-Book" size:30.0]];
    [button setTintColor:APP_COLOR_MAIN];//50*1/4, 156*1/2, 210*3/4, 255)];//COLOR_WITH_RGB(255, 255, 255, 255)];
}
-(void)cancelModalView{
    NSLog(@"cancel modal view");
    [((PKNavigationController*)[self.sideMenuViewController mainViewController]) popToRootViewControllerAnimated:YES];
//    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)addLineBelowLabel:(UITextField*)label{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(label.frame)-40,
                                                            CGRectGetMaxY(label.frame)-3,
                                                            CGRectGetWidth(label.frame)+80,
                                                            2)];
    
    
    [line setBackgroundColor:APP_COLOR_MAIN];
    [self.view addSubview:line];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)characters
{
    if(textField == memberName){
        NSMutableCharacterSet *blockedCharacters = [NSMutableCharacterSet alphanumericCharacterSet];
        [blockedCharacters addCharactersInString:@" "];
        blockedCharacters = [[blockedCharacters invertedSet] mutableCopy];
        
        NSUInteger numberOfSpaces = [[[NSString stringWithFormat:@"%@%@", textField.text, characters] componentsSeparatedByString:@" "] count];
        NSLog(@"number of spaces: %d", (int)numberOfSpaces);
        if(numberOfSpaces > 2){
            return NO;
        }
        return ([characters rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
    }
    else if(textField == memberPhone){
        NSCharacterSet *allowedCharacters = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        return ([characters rangeOfCharacterFromSet:allowedCharacters].location == NSNotFound);
    }
    else if(textField == memberEmail){
//        NSMutableCharacterSet *allowedCharacters = [NSCharacterSet alphanumericCharacterSet];
//        [allowedCharacters addCharactersInString:@"_-.@"];
//        allowedCharacters = [[allowedCharacters invertedSet] mutableCopy];
//        [allowedCharacters addCharactersInString:@" "];
        NSCharacterSet *blockedCharacters = [[NSCharacterSet characterSetWithCharactersInString:[ALPHA_NUMERIC stringByAppendingString:@".!#$%&'*+-/=?^_`{|}~@"]] invertedSet];
        return ([characters rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);

    }
    return NO;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag ==20){
        NSLog(@"alertview button inde x%d", (int)buttonIndex);
        if(buttonIndex == 1){
            [self retrieveWithRequestStringType:RequestDeleteCustomer withDictionary:nil];
        }
        else{
            return;
        }
    }
}

@end
