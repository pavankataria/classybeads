//
//  PKTokenTextField.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 23/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//



#import <UIKit/UIKit.h>

//@class TITokenField, TIToken;
//
////==========================================================
//#pragma mark - Delegate Methods -
////==========================================================
//@protocol PKTokenFieldDelegate <UITextFieldDelegate>
//@optional
//- (BOOL)tokenField:(TITokenField *)tokenField willAddToken:(TIToken *)token;
//- (void)tokenField:(TITokenField *)tokenField didAddToken:(TIToken *)token;
//- (BOOL)tokenField:(TITokenField *)tokenField willRemoveToken:(TIToken *)token;
//- (void)tokenField:(TITokenField *)tokenField didRemoveToken:(TIToken *)token;
//
//- (BOOL)tokenField:(TITokenField *)field shouldUseCustomSearchForSearchString:(NSString *)searchString;
//- (void)tokenField:(TITokenField *)field performCustomSearchForSearchString:(NSString *)searchString withCompletionHandler:(void (^)(NSArray *results))completionHandler;
//
//- (void)tokenField:(TITokenField *)tokenField didFinishSearch:(NSArray *)matches;
//- (NSString *)tokenField:(TITokenField *)tokenField displayStringForRepresentedObject:(id)object;
//- (NSString *)tokenField:(TITokenField *)tokenField searchResultStringForRepresentedObject:(id)object;
//- (NSString *)tokenField:(TITokenField *)tokenField searchResultSubtitleForRepresentedObject:(id)object;
//- (UIImage *)tokenField:(TITokenField *)tokenField searchResultImageForRepresentedObject:(id)object;
//- (UITableViewCell *)tokenField:(TITokenField *)tokenField resultsTableView:(UITableView *)tableView cellForRepresentedObject:(id)object;
//- (CGFloat)tokenField:(TITokenField *)tokenField resultsTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
//@end

@interface PKTokenFieldInternalDelegate : NSObject <UITextFieldDelegate>
@end

#import <UIKit/UIKit.h>

@interface PKTokenTextField : UITextField
//@property (nonatomic, weak) id <PKTokenFieldDelegate> delegate;


@end
