//
//  SectionHeaderSubTotalVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 06/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderSubTotalVC : UIViewController{
    IBOutlet UILabel *sectionHeaderTitleLabel;
}


@property (nonatomic, retain) NSString *sectionHeaderTitle;

-(void)setSectionHeaderTitle:(NSString *)sectionTitle;
@end
