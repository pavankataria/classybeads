//
//  SectionFooterSubTotalViewVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 06/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "SectionFooterSubTotalViewVC.h"

@interface SectionFooterSubTotalViewVC ()

@end

@implementation SectionFooterSubTotalViewVC
@synthesize tCDTotalDiscountValue;
@synthesize tCDTotalFinalValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setTCDTotalDiscountValue:(CGFloat)dv{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    [totalDiscountLabel setText:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:[NSNumber numberWithFloat:dv]]]];
}
-(void)setTCDTotalFinalValue:(CGFloat)fv{
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    [totalFinalLabel setText:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:[NSNumber numberWithFloat:fv]]]];
}
-(void)setTCDTotalQuantityValue:(int)qv{
    [totalQuantityLabel setText:[NSString stringWithFormat:@"%d", qv]];
}
@end
