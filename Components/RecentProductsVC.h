//
//  ProductsRecentlyAddedVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 18/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PkNumberPadVC.h"

@interface RecentProductsVC : UIViewController<UITableViewDataSource, UITableViewDelegate, PkNumberPadDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UITableView *productsTable;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@end
