//
//  SectionContainer.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 06/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "SectionContainer.h"

@implementation SectionContainer

@synthesize
sectionDiscountOrderLines,
sectionDiscountHeaderTitle,
sectionDiscountSearchString,
sectionDiscountUniqueLinkingString,

sectionDiscountInitialQuantityOffer,
sectionDiscountFinalQuantityOffer,

sectionDiscountQuantityTotal,
sectionDiscountDiscountTotal,
sectionDiscountFinalTotal;



-(id)initWithContainerLinkingString:(NSString*)string andSectionTitleHeader:(NSString*)sectionTitleHeader{
    self = [super init];
    if(!self) return nil;
   
    self.sectionDiscountOrderLines = [[NSMutableArray alloc] init];
    self.sectionDiscountUniqueLinkingString = string;
    self.sectionDiscountHeaderTitle = sectionTitleHeader;
    return self;
}
-(id)initWithUniqueLinkingStringAndWithSectionTitleHeader:(NSString*)headerTitle withInitial:(int)initial andFinal:(int)final{
    self = [super init];
    if(!self) return nil;
    NSUUID  *UUID = [NSUUID UUID];
    NSString* stringUUID = [UUID UUIDString];
    self.sectionDiscountUniqueLinkingString = stringUUID;
    self.sectionDiscountOrderLines = [[NSMutableArray alloc] init];
    self.sectionDiscountHeaderTitle = headerTitle;
    self.sectionDiscountInitialQuantityOffer = initial;
    self.sectionDiscountFinalQuantityOffer = final;
    NSLog(@"section container: %d for %d", self.sectionDiscountInitialQuantityOffer, sectionDiscountFinalQuantityOffer);
    return self;
    
}
-(void)sectionDiscountCalculateFooterDetailValues{
    self.sectionDiscountQuantityTotal = 0;
    self.sectionDiscountDiscountTotal = 0;
    self.sectionDiscountFinalTotal = 0;
    
    NSUInteger totalItems = 0;
    CGFloat subTotal = 0.0f, discountTotal = 0.0f, finalTotal = 0.0f;
    
    for(OrderLineDataModel *currentOL in self.sectionDiscountOrderLines){
        [currentOL setOrderLineLinkingString:self.sectionDiscountUniqueLinkingString];
        
        if([currentOL.orderLineOriginalTotal floatValue] > 0){
            totalItems += [currentOL.orderLineQuantity integerValue];
            subTotal += [currentOL.orderLineOriginalTotal floatValue];
            
            if([self.sectionDiscountSearchString isEqualToString:@"none"]){
                discountTotal += [currentOL.orderLineDiscount floatValue];
            }
        }

        
    }
    
    
    
    
    finalTotal = subTotal - discountTotal;
    
    self.sectionDiscountQuantityTotal = totalItems;
//    [self.section setSubTotalCost:subTotal];
    self.sectionDiscountDiscountTotal = discountTotal;
    self.sectionDiscountFinalTotal = finalTotal;
    NSLog(@"description: %@", [self description]);
    
    
    
    
    NSLog(@"Calculation number: %.2f", [AppConfig aOf:self.sectionDiscountInitialQuantityOffer bOf:self.sectionDiscountFinalTotal cOf:[NSNumber numberWithInt:self.sectionDiscountInitialQuantityOffer] dOf:[NSNumber numberWithFloat:sectionDiscountFinalTotal]]);

}

-(void)setOrderLinesArray:(NSMutableArray *)_sectionOrderLines{
    self.sectionDiscountOrderLines = _sectionOrderLines;
}

-(NSString*)description{
    return [NSString stringWithFormat:@"%@ <%p>", NSStringFromClass([self class]), self];
}
@end
