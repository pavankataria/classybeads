//
//  CustomerDetailsReceiptVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 05/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "CustomerDetailsReceiptVC.h"

@interface CustomerDetailsReceiptVC (){
    NSMutableArray *invoicesArray;
    NSString *referenceNumberFromServer;
    NSString *purchaseDate;
}

@end

@implementation CustomerDetailsReceiptVC
@synthesize PDFCreator;
-(id)initWithInvoiceDataArray:(NSMutableArray*)invoiceArray andReferenceNumber:(NSString*)ref{
    self = [super init];
    if(!self) return nil;
    
    invoicesArray = invoiceArray;
    referenceNumberFromServer = ref;
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSString*)getAttachment{
//    NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
//    OrdersDataModel *odm = [orderDataArray objectAtIndex:selectedIndex.row];
    
    NSString *readmePath = [[NSBundle mainBundle] pathForResource:@"invoiceHtmlDesign.html" ofType:nil];
    NSString *pdfAttachment = [NSString stringWithContentsOfFile:readmePath encoding:NSUTF8StringEncoding error:NULL];
    
    if(readmePath == nil){
        NSLog(@"Path not found");
    }
    else{
        NSLog(@"Path found %@", readmePath);
    }
    
    
    //    NSString *welcomeMessage = [NSString stringWithFormat:@"Hi %@, thank you for your purchase. Please find attached an invoice for your order. If you have any queries, please do not hesitate to contact me on %@", odm.orderCustomerName, @"07716911849"];
    
    
    NSString *invoiceTableRows = [self getHTMLForInvoiceTableForOrder:nil];
    NSString *mainTable = [self getHTMLForMainTableForOrder:nil];
    
    pdfAttachment = [pdfAttachment stringByReplacingOccurrencesOfString:@"$invoiceTableRows" withString:invoiceTableRows];
    
    
    
    pdfAttachment = [pdfAttachment stringByReplacingOccurrencesOfString:@"$mainTable" withString:mainTable];
    
    NSLog(@"emailBody:\n\n%@", pdfAttachment);
    
    
    
    return pdfAttachment;
    
}

-(NSString*)getHTMLForInvoiceTableForOrder:(OrdersDataModel*)odm{
    NSMutableString *invoiceTableHTML = [[NSMutableString alloc] init];
    
    [invoiceTableHTML appendString:[NSString stringWithFormat:@"<tr><th>Order ID</th><td>%@</td></tr>", referenceNumberFromServer]];
    [invoiceTableHTML appendString:[NSString stringWithFormat:@"<tr><th>Order Date</th><td>%@</td></tr>", purchaseDate]];
    [invoiceTableHTML appendString:[NSString stringWithFormat:@"<tr><th>Customer Id</th><td>%@</td></tr>", name.text]];
    return invoiceTableHTML;
}
-(NSString*)getHTMLForMainTableForOrder:(OrdersDataModel*)odm{
    NSMutableString *mainTableString = [[NSMutableString alloc] init];
    NSString *beginningOfTableHTMLString = @"<table id='orderTable'>";
    NSArray *headings =@[@"Qty", @"Code", @"Price", @"Total", @"Minus<br>Discount", @"Line<br>Total"];
    NSString *tableHeadersHTMLString = [AppConfig getHTMLForTableWithHeadings:headings];
    
    [mainTableString appendString:beginningOfTableHTMLString];
    [mainTableString appendString:tableHeadersHTMLString];
    NSMutableArray *orderLines = [self getOrderLinesForOrder:odm];
    for(int i = 1; i < [orderLines count]+1; i++){
        
        //Also input whether row is even or not for CSS style
        [mainTableString appendString:[[orderLines objectAtIndex:i-1] orderLineConvertToTableDataRowIsEven:((i%2 == 0)?YES:NO)]];
    }
    [mainTableString appendString:[NSString stringWithFormat:@"<tr rowspan='2'><td colspan='%d'></td><td></td><td></td><td></td></tr>", (int)([headings count]-3)]];
    NSString *endOfTableHTMLString = @"</table>";
    [mainTableString appendString:endOfTableHTMLString];
    return mainTableString;
}

-(NSString*)getEmailBody{
//    NSIndexPath *selectedIndex = [_orderTable indexPathForSelectedRow];
//    OrdersDataModel *odm = [orderDataArray objectAtIndex:selectedIndex.row];
    
    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:[self getAttachment]];
    
    NSString *readmePath = [[NSBundle mainBundle] pathForResource:@"style.html" ofType:nil];
    NSString *styleHTMLString = [NSString stringWithContentsOfFile:readmePath encoding:NSUTF8StringEncoding error:NULL];
    
    
    if(readmePath == nil){
        NSLog(@"Path not found");
    }
    else{
        NSLog(@"Path found %@", readmePath);
    }
    
    
    NSLog(@"styleOfHTMLString %@", styleHTMLString);
    NSString *startOfFileHTMLString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">%@</style></head><body>", styleHTMLString];
    
    
    

    
    NSString *beginningOfEmailBody = [NSMutableString stringWithFormat:@"<p><b>Hello%@,<br>here is your receipt information with:<br> Order Reference #:</b> %@ <br><b>Purchase Date:</b> %@</p><br>", ([name.text length] == 0 ? @"" : name.text), referenceNumberFromServer,purchaseDate];
    
    
    
    [emailBody appendString:startOfFileHTMLString];
    [emailBody appendString:beginningOfEmailBody];
    
    NSString *beginningOfTableHTMLString = @"<table style=\"width:300px\" cellspacing='0'>";
    
    
    NSString *tableHeadersHTMLString = [AppConfig getHTMLForTableWithHeadings:@[@"Qty", @"Code", @"Price", @"Total", @"Minus<br>Discount", @"Line<br>Total"]];
    
    [emailBody appendString:beginningOfTableHTMLString];
    [emailBody appendString:tableHeadersHTMLString];
    NSMutableArray *orderLines = [self getOrderLinesForOrder:nil];
    for(int i = 1; i < [orderLines count]+1; i++){
        
        //Also input whether row is even or not for CSS style
        [emailBody appendString:[[orderLines objectAtIndex:i-1] orderLineConvertToTableDataRowIsEven:((i%2 == 0)?YES:NO)]];
    }
    NSString *endOfTableHTMLString = @"</table>";
    [emailBody appendString:endOfTableHTMLString];
    
    NSString *endOfFileHTMLString = @"</body></html>";
    [emailBody appendString:endOfFileHTMLString];
    return emailBody;
}
-(NSMutableArray*)getOrderLinesForOrder:(OrdersDataModel*)odm{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for(OrderLineDataModel *o in invoicesArray){
        if(odm.orderId == o.orderLineOrderId){
            [tempArray addObject:o];
        }
    }
    return tempArray;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE, d MMMM yyyy"];
    purchaseDate = [dateFormatter stringFromDate:[NSDate date]];
    
    
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Finish" style:UIBarButtonItemStylePlain target:self action:@selector(done)];

}
-(void)done{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(IBAction)sendEmail:(id)sender{
    [SVProgressHUD dismiss];
    if( ! [self NSStringIsValidEmail:email.text]){
        [SVProgressHUD showErrorWithStatus:@"Email is not valid"];
        return;
    }
    if( [name.text length] == 0){
        [SVProgressHUD showErrorWithStatus:@"Name is not valid"];
        return;
    }
    
    NSString *pdfAttachment = [self getAttachment];
    self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:pdfAttachment pathForPDF:[@"~/Documents/invoiceReceipt.pdf" stringByExpandingTildeInPath]
                                            delegate:self
                                            pageSize:kPaperSizeA4
                                             margins:UIEdgeInsetsMake(10, 5, 10, 5)];
    
    

    
    NSString *emailTitle = [NSString stringWithFormat:@"Classy Beads Receipt: %@", referenceNumberFromServer];
    //    NSString *messageBody = @"Hey, check this out!";
    NSArray *toRecipents = [NSArray arrayWithObject:email.text];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    //    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    
    
    
    NSString *newFilePath = self.PDFCreator.PDFpath;
    
    NSData * pdfData = [NSData dataWithContentsOfFile:newFilePath];
    
    
    NSString *fileName = [NSString stringWithFormat:@"%@", referenceNumberFromServer];
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z0-9_]+" options:0 error:nil];
    fileName = [regex stringByReplacingMatchesInString:fileName options:0 range:NSMakeRange(0, fileName.length) withTemplate:@"-"];
    NSLog(@"source: %@\nnewFileName: %@", newFilePath, fileName);
    
    
    [mc addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"%@.pdf",fileName]];
    
    
    
    NSString *welcomeMessage = [NSString stringWithFormat:@"Hello %@,<br>Thank you for your purchase, please find attached an invoice for your order. If you have any queries, please do not hesitate to contact me on %@", name, @"07903707729"];
    
    [mc setMessageBody:welcomeMessage isHTML:YES];
    [self presentViewController:mc animated:YES completion:NULL];
    
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




//==========================================================
#pragma mark NDHTMLtoPDFDelegate
//==========================================================

- (void)HTMLtoPDFDidSucceed:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
    NSLog(@"%@",result);
    //    self.resultLabel.text = result;
}

- (void)HTMLtoPDFDidFail:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
    NSLog(@"%@",result);
    //    self.resultLabel.text = result;
}


@end
