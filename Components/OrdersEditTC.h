//
//  OrdersEditTC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrdersDataModel.h"
#import "TakeOrderVC.h"

@interface OrdersEditTC : UITableViewController
@property (retain, nonatomic) ASIHTTPRequest *serverRequest;

@end
