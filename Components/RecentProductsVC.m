//
//  ProductsRecentlyAddedVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 18/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "RecentProductsVC.h"
#import "Product.h"
#import "DisplayPadVC.h"

@interface RecentProductsVC (){
    PkNumberPadVC *numberPad;
    DisplayPadVC *displayPad;
    
    NSArray *modifiedProductsArray;
}

@end

@implementation RecentProductsVC
//@synthesize productsTable = _productsTable;

@synthesize managedObjectContext;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Products";
    self.managedObjectContext = [AppDelegate getAppDelegateReference].managedObjectContext;

    
    // Do any additional setup after loading the view from its nib.
    [self setNavigationItems];
    
    [self settingUpView];
    [self settingUpVariables];
    [self settingUpTables];
    
    
    
    
}
-(void)settingUpTables{
    [_productsTable setDelegate:self];
    [_productsTable setDataSource:self];
    [_productsTable reloadData];
}
-(void)settingUpVariables{
    modifiedProductsArray = [[NSMutableArray alloc] init];
    modifiedProductsArray = [self retrieveModifiedProducts];
    
}
-(NSArray*)retrieveModifiedProducts{
    
    //initialising the fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];

//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ == %@", Key(Product, productNeedsUpdating), [NSNumber numberWithBool:YES]];
    
    
//    [fetchRequest setPredicate:predicate];
    
    
    
    
    NSError *error;
    //Query on managedObjectContext with generated fetchRequest.
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"fetched records: %@", [fetchedRecords description]);
    
    //Returning fetched Records
    return fetchedRecords;
}
-(void)settingUpView{
    numberPad = [[PkNumberPadVC alloc] init];
    [numberPad setDelegate:self];
    
    CGRect tempFrame = numberPad.view.frame;
    CGRect screenRect = self.view.frame;
    
    tempFrame.origin.x = screenRect.size.width-tempFrame.size.width - 5;
    tempFrame.origin.y = screenRect.size.height-tempFrame.size.height - 5;
    numberPad.view.frame = tempFrame;
    [self.view addSubview:numberPad.view];
    
    displayPad = [[DisplayPadVC alloc] init];
    //    [displayPad setDelegate:self];
    tempFrame = displayPad.view.frame;
    
    tempFrame.origin.x = screenRect.size.width-tempFrame.size.width;
    
    //Stretch the displaypad down up until the number pad. It should only do that, if there is a gap between the number and display pad.
    if(numberPad.view.frame.origin.y > (displayPad.view.frame.origin.y+displayPad.view.frame.size.height)){
        tempFrame.size.height = numberPad.view.frame.origin.y;
    }
    displayPad.view.frame = tempFrame;
    [self.view addSubview:displayPad.view];

    
    
    
    NSLog(@"numberPad Frame: %@", NSStringFromCGRect(numberPad.view.frame));
    NSLog(@"tempFrame: %@", NSStringFromCGRect(tempFrame));
    
    tempFrame = _productsTable.frame;
    NSLog(@"productsTable: %@", NSStringFromCGRect(tempFrame));

    tempFrame.size.width -= numberPad.view.frame.size.width+10;
    _productsTable.frame = tempFrame;
    
    NSLog(@"productsTable: %@", NSStringFromCGRect(_productsTable.frame));
//    [_productsTable setBackgroundColor:[UIColor yellowColor]];
    
//    CGRect tempFrame2 = _productsTable.frame;
//    tempFrame2.size.width = 596;
//    _productsTable.frame = tempFrame2;

}
-(void)viewDidAppear:(BOOL)animated{
}
-(void)setNavigationItems{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Products";
    
    
    //    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit Order" style:UIBarButtonItemStylePlain target:self action:@selector(editOrder)];
    //    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
    
//    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editOrder)];//:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
//    
//    UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(sendEmail)];//:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
//    
//    UIBarButtonItem *emptyButton = [[UIBarButtonItem alloc] initWithTitle:@"  " style:UIBarButtonItemStylePlain target:self action:nil];
//    
//    [self.navigationItem setRightBarButtonItems:@[editButton, emptyButton, emptyButton, emailButton] animated:YES];
    
    
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
}
-(void)show:(id)sender{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UITableView DataSource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [modifiedProductsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
/*
 if (indexPath.row < orderLinesArray.count) {
        static NSString *CellIdentifier = @"OrderLineCell";
        cell = (OrderLineCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderLineCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        OrderLineDataModel *currentOLDM = [orderLinesArray objectAtIndex:indexPath.row];
        [((OrderLineCell*)cell) setOrderLineQuantityValue:[NSString stringWithFormat:@"%@", currentOLDM.orderLineQuantity]];
        [((OrderLineCell*)cell) setOrderLineCodeValue:currentOLDM.orderLineCode];
        
        NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
        [fmt setPositiveFormat:@"0.##"];
        [((OrderLineCell*)cell) setOrderLinePriceValue:[NSString stringWithFormat:@"£%@", currentOLDM.orderLinePrice]];
        //        NSLog(@"setting cell price to: %@",[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:currentOLDM.orderLineTotal]]);
        [((OrderLineCell*)cell) setOrderLineTotalValue:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:currentOLDM.orderLineTotal]]];
        
    } else {
        // If the row is outside the range, it's the row that was added to allow insertion (see tableView:numberOfRowsInSection:) so give it an appropriate label.
 */
    
    
    
    if([modifiedProductsArray count] > 0){
        static NSString *AddIngredientCellIdentifier = @"productcell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:AddIngredientCellIdentifier];
        if (cell == nil) {
            // Create a cell to display "Add Ingredient".
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:AddIngredientCellIdentifier];
            //            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        Product *currentProduct = [modifiedProductsArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [currentProduct productCode];
        cell.detailTextLabel.text = [[currentProduct productPrice] stringValue];
    }
    
    
    
  /*
   }
   */
    
//    SelectedBackgroundView *selectedBackgroundViewForCell = [SelectedBackgroundView new];
//    [selectedBackgroundViewForCell setBackgroundColor:[UIColor lightGrayColor]];
//    cell.selectedBackgroundView = selectedBackgroundViewForCell;
    return cell;
}



#pragma mark - mark DisplayPad delegate methods

-(void)numberPadView:(PkNumberPadVC *)pkNumberPadVC digitPressed:(NSNumber *)digitPressed{
    
}
-(void)numberPadViewPointButtonDidPress:(PkNumberPadVC *)numberPadView{
    
}

-(void)numberPadViewReturnButtonDidPress:(PkNumberPadVC *)numberPadView{
    
}
-(void)numberPadViewBackspaceButtonDidPress:(PkNumberPadVC *)numberPadView{
    
}
@end
