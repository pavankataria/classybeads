//
//  SectionContainers.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 06/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SectionContainer : NSObject
@property (retain, nonatomic) NSMutableArray *sectionDiscountOrderLines;
@property (retain, nonatomic) NSString *sectionDiscountSearchString;
@property (retain, nonatomic) NSString *sectionDiscountUniqueLinkingString;
@property (retain, nonatomic) NSString *sectionDiscountHeaderTitle;
@property (assign) int sectionDiscountInitialQuantityOffer;
@property (assign) int sectionDiscountFinalQuantityOffer;


@property (assign) int sectionDiscountQuantityTotal;
@property (assign) CGFloat sectionDiscountDiscountTotal;
@property (assign) CGFloat sectionDiscountFinalTotal;

-(id)initWithContainerLinkingString:(NSString*)string andSectionTitleHeader:(NSString*)sectionTitleHeader;

-(id)initWithUniqueLinkingStringAndWithSectionTitleHeader:(NSString*)headerTitle withInitial:(int)initial andFinal:(int)final;
-(void)setOrderLinesArray:(NSMutableArray *)sectionOrderLines;
-(NSString*)description;
-(void)sectionDiscountCalculateFooterDetailValues;
@end
