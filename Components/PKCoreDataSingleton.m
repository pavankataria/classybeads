//
//  PKCoreDataSingleton.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKCoreDataSingleton.h"

@implementation PKCoreDataSingleton
+ (PKCoreDataSingleton*)sharedCoreDataInstance{

    static PKCoreDataSingleton *sharedCoreDataInstance;
    
    @synchronized(self){
        if(!sharedCoreDataInstance){
            sharedCoreDataInstance = [[PKCoreDataSingleton alloc] init];
        }
    }
    
    return [sharedCoreDataInstance sharedInstance];
}

-(PKCoreDataSingleton*)sharedInstance{
    self.managedObjectContext = [AppDelegate getAppDelegateReference].managedObjectContext;
//    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//    //2
//    self.managedObjectContext = appDelegate.managedObjectContext;
//    NSLog(@"OLL");
    return self;
}

+(NSArray*)retrieveAllProductsLatestFirst{
    return [[self sharedCoreDataInstance] _retrieveAllProductsLatestFirst];
}
+(NSArray*)retrieveAllProducts{
    return [[self sharedCoreDataInstance] _retrieveAllProducts];
}

//Figure out how to use error NSError with method signature and pass it down to the next method
+(Product*)retrieveProductWithCode:(NSString*)productCode{
//    NSLog(@"Product code to receive: %@", productCode );
    return [[self sharedCoreDataInstance] _retrieveProductWithCode:productCode];
}
+(void)removeObjectFromStore:(Product*)product{
    [[self sharedCoreDataInstance] removeObjectFromStore:product];
}
+(void)removeObjectsFromStore:(NSArray*)productsToDelete{
    [[self sharedCoreDataInstance] removeObjectsFromStore:productsToDelete];

}









-(void)removeObjectsFromStore:(NSArray*)productsToDelete{
    for(int i = 0; i < [productsToDelete count]; i++){
        [self.managedObjectContext deleteObject:[productsToDelete objectAtIndex:i]];
        
        NSLog(@"deleting product FromCoreData: %@", [[productsToDelete objectAtIndex:i] description]);
        NSError*error;
        [self.managedObjectContext save:&error];

    }
}


-(void)removeObjectFromStore:(Product*)product{
    [self.managedObjectContext deleteObject:product];
    NSError*error;
    [self.managedObjectContext save:&error];
}
-(Product*)_retrieveProductWithCode:(NSString*)productCode{
    // initializing NSFetchRequest
//    NSLog(@"Product code to receive: %@", productCode );

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productCode == %@", productCode];
//    NSLog(@"predicate to use: %@", [predicate description]);
    [fetchRequest setFetchLimit:1];
    [fetchRequest setEntity:entityToBeFetched];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    if ([self.managedObjectContext countForFetchRequest:fetchRequest error:&error]){
        return [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] firstObject];
    }
    else{
        return nil;
    }
}
-(NSArray*)_retrieveAllProductsLatestFirst{
    return [[[self _retrieveAllProducts] reverseObjectEnumerator] allObjects];
}
-(NSArray*)_retrieveAllProducts{
    
    //initialising the fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ == %@", Key(Product, productNeedsUpdating), [NSNumber numberWithBool:YES]];
    
    //    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    //Query on managedObjectContext with generated fetchRequest.
    NSArray *fetchedRecords = [self.
                               managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"fetched records: %@", [fetchedRecords description]);
    
    //Returning fetched Records
    return fetchedRecords;
}
@end
