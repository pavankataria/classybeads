//
//  CustomerDetailsReceiptVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 05/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface CustomerDetailsReceiptVC : UIViewController<MFMailComposeViewControllerDelegate, NDHTMLtoPDFDelegate>{
    IBOutlet UITextField *name;
    IBOutlet UITextField *email;
}

@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;

-(id)initWithInvoiceDataArray:(NSMutableArray*)invoiceArray andReferenceNumber:(NSString*)ref;

@end
