//
//  PKCoreDataSingleton.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface PKCoreDataSingleton : NSObject{
    
}

//+ (PKCoreDataSingleton*)sharedCoreDataInstance;



@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

+(NSArray*)retrieveAllProducts;
+(NSArray*)retrieveAllProductsLatestFirst;

+(Product*)retrieveProductWithCode:(NSString*)productCode;
+(void)removeObjectFromStore:(Product*)product;
+(void)removeObjectsFromStore:(NSArray*)productsToDelete;

@end
