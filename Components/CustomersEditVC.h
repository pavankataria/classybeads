//
//  CustomersEditVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 07/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerDataModel.h"
#import "SBJson.h"
#import "ASIHTTPRequest.h"
#import "NoResultsCell.h"
#import "AddCustomerVC.h"

@interface CustomersEditVC : UIViewController<UITableViewDataSource, UITableViewDelegate, PKAddCustomerDelegate>{
    IBOutlet UITableView *_agentsTable;
    IBOutlet UISearchBar *searchBar;

}
//@property (weak, nonatomic) IBOutlet UITableView *customersTable;
@property (strong, nonatomic) UIRefreshControl *agentTableRefreshControl;

@property (retain, nonatomic) ASIHTTPRequest *serverRequest;

//-(void)remov
@end
