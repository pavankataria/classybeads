//
//  PKTokenTextField.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 23/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKTokenTextField.h"
@interface PKTokenFieldInternalDelegate ()
@property (nonatomic, weak) id <UITextFieldDelegate> delegate;
//@property (nonatomic, weak) TITokenField * tokenField;
@end

@implementation PKTokenTextField{
    PKTokenFieldInternalDelegate * _internalDelegate;

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self commonSetup];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)commonSetup{
    _internalDelegate = [[PKTokenFieldInternalDelegate alloc] init];

//	[_internalDelegate setTokenField:self];
	[super setDelegate:_internalDelegate];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"description field did become responder");
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"LOL OF COURSE");
    return YES;
}

@end


//==========================================================
#pragma mark - PKTokenFieldInternalDelegate -
//==========================================================
@implementation PKTokenFieldInternalDelegate
@synthesize delegate = _delegate;
//@synthesize tokenField = _tokenField;

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	if ([_delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]){
		return [_delegate textFieldShouldBeginEditing:textField];
	}
	
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	
	if ([_delegate respondsToSelector:@selector(textFieldDidBeginEditing:)]){
		[_delegate textFieldDidBeginEditing:textField];
	}
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
	
	if ([_delegate respondsToSelector:@selector(textFieldShouldEndEditing:)]){
		return [_delegate textFieldShouldEndEditing:textField];
	}
	
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	if ([_delegate respondsToSelector:@selector(textFieldDidEndEditing:)]){
		[_delegate textFieldDidEndEditing:textField];
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
//	if (_tokenField.tokens.count && [string isEqualToString:@""] && [_tokenField.text isEqualToString:kTextEmpty]){
//		[_tokenField selectToken:[_tokenField.tokens lastObject]];
//		return NO;
//	}
//	
//	if ([textField.text isEqualToString:kTextHidden]){
//		[_tokenField removeToken:_tokenField.selectedToken];
//		return (![string isEqualToString:@""]);
//	}
//	
//	if ([string rangeOfCharacterFromSet:_tokenField.tokenizingCharacters].location != NSNotFound && !_tokenField.forcePickSearchResult){
//		[_tokenField tokenizeText];
//		return NO;
//	}
//	
//	if ([_delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]){
//		return [_delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
//	}
//    
//    if (_tokenField.tokenLimit!=-1 &&
//        [_tokenField.tokens count] >= _tokenField.tokenLimit) {
//        return NO;
//    }
	
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
//	[_tokenField tokenizeText];
	
	if ([_delegate respondsToSelector:@selector(textFieldShouldReturn:)]){
		return [_delegate textFieldShouldReturn:textField];
	}
	
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
	
	if ([_delegate respondsToSelector:@selector(textFieldShouldClear:)]){
		return [_delegate textFieldShouldClear:textField];
	}
	
	return YES;
}

@end

