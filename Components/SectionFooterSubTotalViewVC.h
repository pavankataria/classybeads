//
//  SectionFooterSubTotalViewVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 06/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionFooterSubTotalViewVC : UIViewController{
    IBOutlet UILabel *totalDiscountLabel;
    IBOutlet UILabel *totalFinalLabel;
    IBOutlet UILabel *totalQuantityLabel;
    
}

@property (nonatomic, assign) CGFloat tCDTotalDiscountValue;
@property (nonatomic, assign) CGFloat tCDTotalFinalValue;
@property (nonatomic, assign) int tCDTotalQuantityValue;

@end
