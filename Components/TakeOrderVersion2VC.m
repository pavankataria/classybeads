//
//  TakeOrderVersion2VC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 04/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "TakeOrderVersion2VC.h"
#import "OrderLineDataModel.h"
#import "OrderLineCell.h"
@interface TakeOrderVersion2VC (){
    OrdersDataModel *editModeOrderObject;
    BOOL EDIT_MODE;
    
}
//@property (strong, nonatomic) OrderedDictionary *items;
@property (strong, nonatomic) NSMutableArray *sectionsAndItems;



@end

@implementation TakeOrderVersion2VC
@synthesize orderLineTable = _orderLineTable;
@synthesize customerDM = _customerDM;

-(id)initWithCustomerObject:(CustomerDataModel*)customerObject{
    self = [super init];
    if( !self ) return nil;
    _customerDM = customerObject;
    return self;
}
-(id)initInOrderEditModeWithOrderObject:(OrdersDataModel*)orderObject{
    self = [super init];
    if( !self ) return nil;
    
    NSLog(@"edit order mode view controller init method called");
    editModeOrderObject = orderObject;
    NSLog(@"EDIT MODE ORDERLINE DETAILS: %@", editModeOrderObject.orderLinesDataArray);
    EDIT_MODE = YES;
    return self;
}
-(void)settingUpView{
    numberPad = [[PkNumberPadVC alloc] init];
    [numberPad setDelegate:self];
    CGRect tempFrame = numberPad.view.frame;
    CGRect screenRect = self.view.frame;
    
    tempFrame.origin.x = screenRect.size.width-tempFrame.size.width - 5;
    tempFrame.origin.y = screenRect.size.height-tempFrame.size.height - 5;
    numberPad.view.frame = tempFrame;
    [self.view addSubview:numberPad.view];
    
    //#################
    //FOOTER VIEW
    //##    ###
    orderTotalFooterView = [[OrderTotalFooterVC alloc] init];
    tempFrame = orderTotalFooterView.view.frame;
    tempFrame.size.width = 596;
    tempFrame.size.height = ORDER_ITEM_ROW_HEIGHT;
    tempFrame.origin.y = self.view.frame.size.height-tempFrame.size.height;
    
    [orderTotalFooterView.view setFrame:tempFrame];
    
    [self.view addSubview:orderTotalFooterView.view];
    
    
    displayPad = [[DisplayPadVC alloc] init];
    //    [displayPad setDelegate:self];
    tempFrame = displayPad.view.frame;
    
    tempFrame.origin.x = screenRect.size.width-tempFrame.size.width;
    
    //Stretch the displaypad down up until the number pad. It should only do that, if there is a gap between the number and display pad.
    if(numberPad.view.frame.origin.y > (displayPad.view.frame.origin.y+displayPad.view.frame.size.height)){
        tempFrame.size.height = numberPad.view.frame.origin.y;
    }
    displayPad.view.frame = tempFrame;
    [self.view addSubview:displayPad.view];
    
    
    _orderLineTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    tempFrame = _orderLineTable.frame;
    tempFrame.size.height -= orderTotalFooterView.view.frame.size.height;
    
    //    originalTableFrame = _orderLineTable.frame = tempFrame;
    
    tempFrame = _orderLineTable.frame;
    tempFrame.size.width = 596;
    tempFrame.size.height -= orderTotalFooterView.view.frame.size.height;
    _orderLineTable.frame = tempFrame;
    
    
    crossDiscount = [[CrossDiscountControlVC alloc] init];
    [crossDiscount setDelegate:self];
//    [crossDiscount setDelegate:self];
    tempFrame = crossDiscount.view.frame;
    
    
    tempFrame.origin.x = [_orderLineTable frame].size.width;
    tempFrame.size.width = screenRect.size.width -[_orderLineTable frame].size.width;
    tempFrame.size.height = screenRect.size.height;
    
    crossDiscount.view.frame = tempFrame;
    [self setCrossDiscountViewHidden:YES];
    [self.view addSubview:crossDiscount.view];

    
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    [self settingUpView];
    [self settingUpTables];
    [self setupNavigationItems];
    
//    [_orderLineTable selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
}
-(void)setupNavigationItems{
    showReceiptButton = [[UIBarButtonItem alloc] initWithTitle:@"Receipt" style:UIBarButtonItemStylePlain target:self action:@selector(showReceipt:)];
    applyCrossDiscountButton = [[UIBarButtonItem alloc] initWithTitle:@"Cross Discount" style:UIBarButtonItemStylePlain target:self action:@selector(applyCrossDiscount:)];
    UIBarButtonItem *emptyButton = [[UIBarButtonItem alloc] initWithTitle:@"  " style:UIBarButtonItemStylePlain target:self action:@selector(emptyCall)];
    
    //    self.navigationItem.rightBarButtonItem = showReceiptButton;
    [self.navigationItem setRightBarButtonItems:@[showReceiptButton, emptyButton, applyCrossDiscountButton] animated:YES];

}
-(void)emptyCall{
    
}

-(void)showReceipt:(id)sender{
    /* operation change
     if([[self.items objectForKey:@"none"] count] > 1){
        OrderLineDataModel *oldm = [[self.items objectForKey:@"none"] firstObject];
        if([oldm isWholeObjectEmpty]){
            [self.items removeObjectFromArrayAtIndex:0 forKey:@"none"];
        }
    }
    */
    
    //@"none" tag
//    if([[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] count] > 1){
//        OrderLineDataModel *oldm = [[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] firstObject];
//        if([oldm isWholeObjectEmpty]){
//            [[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] removeObject:oldm];
//        }
//    }
    
    for(int i = 0; i < [self.sectionsAndItems count]; i++){
        
        for(int k = 0; k < [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count]; k++){
            OrderLineDataModel *oldm = [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] objectAtIndex:k];
            if([oldm isWholeObjectEmpty]){
                [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] removeObject:oldm];
                if([[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count] == 0){
//                    [self.sectionsAndItems removeObjectAtIndex:i];
                    break;
                }
            }
        }
    }
    
    NSLog(@"sectionsAndItems %@", self.sectionsAndItems);
    
    
    [_orderLineTable reloadData];
    
    
    
//    
//    //Dont press enter button if last cell is removed since enter is pressed only to ENTER the values into the last edited table row.
//    //so that changes take effect incase the user accidentally forgets to press enter on the price when clicking on the receipt button
//    BOOL hadToRemoveObject = NO;
//    if([oldm.orderLineCode isEqualToString:@"0"]){
//        [orderLinesArray removeLastObject];
//        [_orderLineTable reloadData];
//        hadToRemoveObject = YES;
//    }
//    if( ! hadToRemoveObject){
//        [self numberPadViewReturnButtonDidPress:nil];
//    }
    
/* operation change
 if([self.items count] > 0){
        NSError *error = nil;
        BOOL aOK = [self checkIfAllDataFieldsValidWithError:&error];
        
        if(aOK){
            [self showReceiptViewController];
            return;
        }
        else{
            NSString * message = [NSString stringWithFormat:@"Cant show receipt because %@", [error localizedDescription]];
            [SVProgressHUD showErrorWithStatus:message maskType:SVProgressHUDMaskTypeBlack];

        }
    }
    else{
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Add a product first"] maskType:SVProgressHUDMaskTypeBlack];
    }
 */
    int totalNumberOfOrderLines = 0;
    for(int i = 0; i < [self.sectionsAndItems count]; i++){
        
        totalNumberOfOrderLines += [[[self.sectionsAndItems objectAtIndexSafe:i] sectionDiscountOrderLines] count];
    }
            
    if(totalNumberOfOrderLines > 0){
        NSError *error = nil;
        BOOL aOK = [self checkIfAllDataFieldsValidWithError:&error];
        
        if(aOK){
            [self showReceiptViewController];
            return;
        }
        else{
            NSString * message = [NSString stringWithFormat:@"Cant show receipt because %@", [error localizedDescription]];
            [SVProgressHUD showErrorWithStatus:message maskType:SVProgressHUDMaskTypeBlack];
            
        }
    }
    else{
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Add a product first"] maskType:SVProgressHUDMaskTypeBlack];
    }

}

-(void)showReceiptViewController{
    NSMutableArray *receiptArray = [[NSMutableArray alloc] init];
    /* operation change
    for(NSString *key in self.items){
        for (OrderLineDataModel *oldm in [self.items objectForKey:key]) {
            [receiptArray addObject:oldm];
        }
    }
     */
    
//    for(NSString *key in self.items){
//        for (OrderLineDataModel *oldm in [self.items objectForKey:key]) {
//            [receiptArray addObject:oldm];
//        }
//    }
//
//    
//    
////    
    /*
    if(EDIT_MODE){
        NSLog(@"original count: %d edit object count: %d", [receiptArray count], [editModeOrderObject.orderLinesDataArray count]);
        
        editModeOrderObject.orderLinesDataArray = receiptArray;
        ReceiptVC *receiptVC = [[ReceiptVC alloc] initInEditModeWithOrderObject:editModeOrderObject];
        [self.navigationController pushViewController:receiptVC animated:YES];
        
    }
    else{
*/
        
        ReceiptVC *receiptVC = [[ReceiptVC alloc] initWithCustomerObject:_customerDM andInvoiceDataArray:receiptArray];
        //        [receiptVC setCustomerDetails:_customerDM];
        //        [receiptVC setInvoiceDataArray:orderLinesArray];
        [self.navigationController pushViewController:receiptVC animated:YES];
//    }
}
-(void)applyCrossDiscount:(id)sender{
    if(displayPad.view.isHidden){
        [self setCrossDiscountViewHidden:YES];
    }
    else{
        [self setCrossDiscountViewHidden:NO];
    }

}

-(void)setCrossDiscountViewHidden:(BOOL)hidden{
//    for (NSIndexPath *indexPath in _orderLineTable.indexPathsForSelectedRows) {
//        [_orderLineTable deselectRowAtIndexPath:indexPath animated:YES];
//        [selectedArray removeAllObjects];
//    }
    if(hidden){
        [crossDiscount.view setHidden:YES];
        [_orderLineTable setAllowsMultipleSelection:NO];
        [displayPad.view setHidden:NO];
        [numberPad.view setHidden:NO];
        [showReceiptButton setEnabled:YES];
        [showReceiptButton setTitle:@"Receipt"];
        [applyCrossDiscountButton setTitle:@"Cross Discount"];
    }
    else{
        [crossDiscount.view setHidden:NO];
        [_orderLineTable setAllowsMultipleSelection:YES];
        
        [displayPad.view setHidden:YES];
        [numberPad.view setHidden:YES];
        [showReceiptButton setEnabled:NO];
        [showReceiptButton setTitle:@""];
        [applyCrossDiscountButton setTitle:@"Finish Cross Discount"];
    }
    
}

-(void)settingUpTables{
    
//    self.items = [[OrderedDictionary alloc] init];
//    [self.items insertObjectToArray:[[OrderLineDataModel alloc] init] forKey:@"none"];
    
            self.sectionsAndItems = [[NSMutableArray alloc] init];

    SectionContainer *sectionContainer = [[SectionContainer alloc] initWithContainerLinkingString:@"none" andSectionTitleHeader:@"Normal Order Lines"];

    [self.sectionsAndItems addObject:sectionContainer];

    [[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] insertObject:[[OrderLineDataModel alloc] init] atIndex:0];

    
    
    [_orderLineTable setEditing:YES animated:YES];
    [_orderLineTable reloadData];

    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




//===============================================
#pragma mark - UITabeView DataSource Methods -
//===============================================

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
/* operation change
 if([[self.items allKeys] count] == 0){
        //This ensures there will always be atleast one section
        return 1;
    }
    else{
        return [[self.items allKeys] count];
    }
 */
    if([self.sectionsAndItems count] == 0) return 1;
    else return [self.sectionsAndItems count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSUInteger rows = [[[self.sectionsAndItems objectAtIndexSafe:section] sectionDiscountOrderLines] count];
    NSLog(@"ROWS: %d FOR SECTION: %d", rows, section);
    if(tableView.isEditing){
        if(section == 0){
            rows++;
        }
    }
    return rows;
    /* operation change
    NSUInteger rows = [[self.items objectForKey:[self.items keyAtIndex:section]] count];
    if(tableView.isEditing){

        if(section == 0){
            //For extra
            rows++;
        }
    }
    
    return rows;
     */
}

//-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//        return [[self.sectionsAndItems objectAtIndex:section] sectionDiscountHeaderTitle];
//}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView.isEditing){
        if (indexPath.section == 0 && indexPath.row == 0) {
            static NSString *addProductCellIdentifier = @"AddProductCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addProductCellIdentifier];
            if (cell == nil) {
                // Create a cell to display "Add Ingredient".
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addProductCellIdentifier];
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            cell.textLabel.text = @"Add Product";

            return cell;
        }

    }
    static NSString *CellIdentifier = @"OrderLineCell";
    OrderLineCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"OrderLineCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.showsReorderControl = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(OrderLineCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0){
        return;
    }
/* operation change
    int indexCorrection = tableView.isEditing ? -1 : 0;
    */
    
    int correctionDelta = 0;
    if(indexPath.section == 0) correctionDelta = -1;

    NSLog(@"indexPath row: %d section: %d", indexPath.row, indexPath.section);
    NSLog(@"items: %@ correction: %d", self.sectionsAndItems, correctionDelta);
    
    OrderLineDataModel *odm = [[[self.sectionsAndItems objectAtIndex:indexPath.section] sectionDiscountOrderLines] objectAtIndex:indexPath.row + correctionDelta];
    
    [cell setCellWithOrderLineObject:odm];
}



//===============================================
#pragma mark - UITabeView Editing Methods -
//===============================================
-(BOOL)checkIfAllDataFieldsValidWithError:(NSError**)error{
    NSError *innerError = nil;
    NSUInteger currentRow = 1; //The current row is used to shake the appropriate cell to present error to user
    NSUInteger section = 1;
    /* operation change
    for(NSString *key in self.items){
   
        
        NSLog(@"checking current array with key: %@", key);
        for(OrderLineDataModel * oldm in [self.items objectForKey:key]){
            NSLog(@"cc oldm: %@", oldm);
            
            if(![oldm isOrderLineDataModelValuesValidWithError:&innerError]){
                
                NSString *errorDescription = [innerError localizedDescription];
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setValue:[NSString stringWithFormat:@"Error: < %@ > on orderLine: %d", errorDescription, currentRow] forKey:NSLocalizedDescriptionKey];
                [dict setValue:[NSIndexPath indexPathForRow:currentRow inSection:section] forKey:@"offendingRow"];
                *error =  [NSError errorWithDomain:@"PavanKatariaDomain" code:8001 userInfo:dict];
                return NO;
            }
            currentRow++;
        }
        section++;
    }
     
     */
    
    
    for(int i = 0; i < [self.sectionsAndItems count]; i++){
        
        for(int k = 0; k < [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count]; k++){
            OrderLineDataModel *oldm = [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] objectAtIndex:k];
            
            if(![oldm isOrderLineDataModelValuesValidWithError:&innerError]){
                NSString *errorDescription = [innerError localizedDescription];
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setValue:[NSString stringWithFormat:@"Error: < %@ > on orderLine: %d in category: %d", errorDescription, currentRow, section] forKey:NSLocalizedDescriptionKey];
                [dict setValue:[NSIndexPath indexPathForRow:currentRow inSection:section] forKey:@"offendingRow"];
                *error =  [NSError errorWithDomain:@"PavanKatariaDomain" code:8001 userInfo:dict];
                return NO;
            }
            currentRow++;
        }
        section++;
    }
    
    *error = nil;
    return YES;
}

/*//Try getting this to work to shake a uitaleview cell when there is an error in its data
 -(void)shakeAnimation:(UIView*) cell
{
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
    [shake setDuration:0.1];
    [shake setRepeatCount:5];
    [shake setAutoreverses:YES];
    [shake setFromValue:[NSValue valueWithCGPoint:
                         CGPointMake(cell.center.x - 5,cell.center.y)]];
    [shake setToValue:[NSValue valueWithCGPoint:
                       CGPointMake(cell.center.x + 5, cell.center.y)]];
    [cell.layer addAnimation:shake forKey:@"position"];
}*/

-(void)addNewRowToOrderLineTable{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];

    [_orderLineTable.dataSource tableView:_orderLineTable commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
    
//    [_orderLineTable deselectRowAtIndexPath:indexPath animated:NO];
}

-(BOOL)canAddNewRow{
    NSError *error = nil;
    if( ! [self checkIfAllDataFieldsValidWithError:&error]){
        NSIndexPath *offendingIndexPath = [[error userInfo] objectForKey:@"offendingRow"];
        [_orderLineTable selectRowAtIndexPath:offendingIndexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
/*
 [_orderLineTable scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        
        
        OrderLineCell *cell = (OrderLineCell*)[_orderLineTable.dataSource tableView:_orderLineTable cellForRowAtIndexPath:scrollIndexPath];
 [self shakeAnimation:cell.contentView];
 */
        NSLog(@"Cannot add new row because of the following reason: %@", [error localizedDescription]);
        return NO;
    }
    return YES;
}


-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int correctionDelata = 0;
    if(indexPath.section == 0){
        correctionDelata = -1;
    }
    //Minus one because of the insert style row
    if(indexPath.section == 0 && indexPath.row == 0){
        [[_orderLineTable dataSource] tableView:_orderLineTable commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
        return nil;
    }
    else{
        /* operation change
        [displayPad setValuesWithOrderLine:[[self.items objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1]];
         */
        
        
        [displayPad setValuesWithOrderLine:[[[self.sectionsAndItems objectAtIndex:indexPath.section] sectionDiscountOrderLines] objectAtIndex:indexPath.row+ correctionDelata]];
        return indexPath;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.section == 0 && indexPath.row == 0){
//        if([self canAddNewRow]){
//            [self addNewRowToOrderLineTable];
//        }
//    }
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0 && indexPath.row == 0){
        
        return UITableViewCellEditingStyleInsert;
    }
    else{
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (editingStyle) {
        case UITableViewCellEditingStyleDelete:{
            /* operation change
            [self.items removeObjectFromArrayAtIndex:indexPath.row-1 forKey:[self.items keyAtIndex:indexPath.section]];
            */
            
            int correctionDelta = 0;
            if(indexPath.section == 0){
                correctionDelta = -1;
            }
            [[[self.sectionsAndItems objectAtIndex:indexPath.section] sectionDiscountOrderLines] removeObjectAtIndex:indexPath.row+correctionDelta];

            
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
            [tableView endUpdates];
            
            break;
        }
        case UITableViewCellEditingStyleInsert:
        {
            
            [self isTableView:tableView allowedToAddNewRowForIndexPath:indexPath];


            break;
    }
            if([tableView indexPathForSelectedRow] == nil){
                NSLog(@"no selection so clear display");
                [displayPad clearDisplays];
            };
            

        default:
            break;
    }
}
-(NSDictionary*)isDataInOrderLineTableValid{
    BOOL isDataValid = YES;
    NSIndexPath *errorAtIndexPath = nil;
    NSUInteger count = 0;
    NSString *error = @"";
    /* operation change
    for(NSString *key in self.items){
        for(OrderLineDataModel *currentOL in [self.items objectForKey:key]){
            if( ! ([currentOL.orderLineQuantity integerValue] > 0)){
                isDataValid = NO;
                errorAtRow = count;
                error = @"quantity";
                break;
            }
            if( ! (currentOL.orderLineCode.length > 3)){
                isDataValid = NO;
                errorAtRow = count;
                error = @"product code";
                break;
            }
            count++;
        }
    }
     */
    NSLog(@"sectionItems count: %d \nsections: %@", [self.sectionsAndItems count], self.sectionsAndItems);
    for(int i = 0; i < [self.sectionsAndItems count]; i++){
        NSLog(@"sectionContainer: %@ withItemCount: %d", [self.sectionsAndItems objectAtIndex:i], [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count]);
        
        for(int k = 0; k < [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count]; k++){
            int correctionDelta = k;
            if(i == 0){
                correctionDelta = k+1; // plus because we're going from index 0 to 1 - 1 because in the table domain for section 0, the index starts at 1 because of row 0 being an Insert button
            }
            NSLog(@"sectionItemsForSection: %d \n%@\n\n", i, [[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines]);

            OrderLineDataModel *currentOL = [[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] objectAtIndex:k];
            NSLog(@"currentOL: %@", currentOL);

            if( ! ([currentOL.orderLineQuantity integerValue] > 0)){
                isDataValid = NO;
                
                errorAtIndexPath = [NSIndexPath indexPathForRow:correctionDelta inSection:i];
                error = @"quantity";
                break;
            }
            if( ! (currentOL.orderLineCode.length > 3)){
                isDataValid = NO;
                errorAtIndexPath = [NSIndexPath indexPathForRow:correctionDelta inSection:i];
                error = @"product code";
                break;
            }
            count++;
        }
    }
    
    //We start the index from 1 instead of 0;
//    NSLog(@"ERROR REPORT: %@",  @{@"isDataValid":[NSNumber numberWithBool:isDataValid], @"errorMessage":error, @"errorIndexPath":errorAtIndexPath});
    if(errorAtIndexPath){
        return @{@"isDataValid":[NSNumber numberWithBool:isDataValid], @"errorMessage":error, @"errorIndexPath":errorAtIndexPath};
    }
    else{
        return @{@"isDataValid":[NSNumber numberWithBool:isDataValid]};
    }
    
}

-(void)isTableView:(UITableView*)tableView allowedToAddNewRowForIndexPath:(NSIndexPath*)indexPath{
    
    NSDictionary *dataValidityStatus = [self isDataInOrderLineTableValid];
    /*
     @"isDataValid"
     @"error"
     @"errorIndex"
     */
    
    if([[dataValidityStatus objectForKey:@"isDataValid"] boolValue]){
/* operation change
        [self.items insertObjectToArray:[[OrderLineDataModel alloc] init] forKey:[self.items keyAtIndex:0]];
        */
        if([[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] count] == 0){
            NSLog(@"going to add object");

            [[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] addObject:[[OrderLineDataModel alloc] init]];

        }
        else{
            NSLog(@"going to insert object");

            [[[self.sectionsAndItems objectAtIndex:0] sectionDiscountOrderLines] insertObject:[[OrderLineDataModel alloc] init] atIndex:0];
        }
        
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        
        [tableView endUpdates];
        NSIndexPath *newRow = [NSIndexPath indexPathForRow:1 inSection:0];
        [tableView selectRowAtIndexPath:newRow animated:YES scrollPosition:UITableViewScrollPositionTop];
        [displayPad clearDisplays];

        
        
        NSLog(@"index: %d", (int)indexPath.row);
        [displayPad moveSelectionBorderToBeginning];
        
    }
    else{
        NSIndexPath * errorAtIndexPath = [dataValidityStatus objectForKey:@"errorIndexPath"];

        int correctionDelta = 0;
        if(errorAtIndexPath.section == 0){
            correctionDelta = -1;
        }
        
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Complete the %@ for item: %d at section category: %d", [dataValidityStatus objectForKey:@"errorMessage"], (int)errorAtIndexPath.row+1, errorAtIndexPath.section+1] maskType:SVProgressHUDMaskTypeBlack];
        
        [tableView scrollToRowAtIndexPath:errorAtIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        [tableView selectRowAtIndexPath:errorAtIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        [displayPad setValuesWithOrderLine:[[[self.sectionsAndItems objectAtIndex:errorAtIndexPath.section] sectionDiscountOrderLines] objectAtIndex:errorAtIndexPath.row+correctionDelta]];

        //#warning Display alertview message for when a row cannot be added due to fields being empty
    }
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0 && indexPath.row == 0){
        return NO;
    }
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    if ([sourceIndexPath isEqual:destinationIndexPath]) {
        return;
    }
    NSIndexPath *fromIndexPath = sourceIndexPath;
    NSIndexPath *toIndexPath = destinationIndexPath;

    if(sourceIndexPath.section == 0){
        fromIndexPath = [NSIndexPath indexPathForRow:sourceIndexPath.row-1 inSection:sourceIndexPath.section];
    }
    if(destinationIndexPath.section == 0){
        toIndexPath = [NSIndexPath indexPathForRow:destinationIndexPath.row-1 inSection:destinationIndexPath.section];
    }
    
//    NSLog(@"Before change: %@", [[self.sectionsAndItems objectAtIndex:toIndexPath.section] sectionDiscountOrderLines] );
    
    OrderLineDataModel *temp = [[[self.sectionsAndItems objectAtIndex:fromIndexPath.section] sectionDiscountOrderLines] objectAtIndex:fromIndexPath.row];
    [[[self.sectionsAndItems objectAtIndex:fromIndexPath.section] sectionDiscountOrderLines] removeObject:temp];
    [[[self.sectionsAndItems objectAtIndex:toIndexPath.section] sectionDiscountOrderLines] insertObject:temp atIndex:toIndexPath.row];
    
    NSLog(@"After change: %@", [[self.sectionsAndItems objectAtIndex:toIndexPath.section] sectionDiscountOrderLines] );
    
    
    
}

-(NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
//    if(sourceIndexPath.section == 0 && sourceIndexPath.row == 0) return sourceIndexPath;
    if(proposedDestinationIndexPath.section == 0 && proposedDestinationIndexPath.row == 0) return sourceIndexPath;
    
//    if( sourceIndexPath.section != proposedDestinationIndexPath.section )
//    {
//        return sourceIndexPath;
//    }
//    else
//    {
//        return proposedDestinationIndexPath;
//    }
    return proposedDestinationIndexPath;

}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _orderLineTable.frame.size.width, [tableView.delegate tableView:tableView heightForHeaderInSection:section])];
    
    SectionHeaderSubTotalVC *sectionHeader = [[SectionHeaderSubTotalVC alloc] init];
    [view addSubview:sectionHeader.view];

    CGRect temp = sectionHeader.view.frame;
    temp.origin.y = view.frame.size.height-temp.size.height;
    temp.size.width = view.frame.size.width;
    sectionHeader.view.frame = temp;
    
    [sectionHeader setSectionHeaderTitle:[[self.sectionsAndItems objectAtIndex:section] sectionDiscountHeaderTitle]];
    [sectionHeader.view setBackgroundColor:APP_COLOR_MAIN];
    

    return view;

}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _orderLineTable.frame.size.width, [tableView.delegate tableView:tableView heightForHeaderInSection:section])];

    SectionFooterSubTotalViewVC *sectionFooter = [[SectionFooterSubTotalViewVC alloc] init];
    [view addSubview:sectionFooter.view];
    
    CGRect temp = sectionFooter.view.frame;
    temp.origin.y = view.frame.size.height-temp.size.height;
    temp.size.width = view.frame.size.width;
    sectionFooter.view.frame = temp;

    [sectionFooter.view setBackgroundColor:APP_COLOR_MAIN];
    [sectionFooter setTCDTotalQuantityValue:[[self.sectionsAndItems objectAtIndex:section] sectionDiscountQuantityTotal]];
    [sectionFooter setTCDTotalDiscountValue:[[self.sectionsAndItems objectAtIndex:section] sectionDiscountDiscountTotal]];
    [sectionFooter setTCDTotalFinalValue:[[self.sectionsAndItems objectAtIndex:section] sectionDiscountFinalTotal]];
    
    return view;

}
//===============================================
#pragma mark - PK NumberPad Delegate Methods
//===============================================

-(BOOL)orderLineSelectedForEdit{
    if([_orderLineTable indexPathForSelectedRow] == nil){
        [SVProgressHUD showErrorWithStatus:@"Select an item to edit" maskType:SVProgressHUDMaskTypeBlack];
        return NO;
    }
    return YES;
}
-(void)numberPadView:(PkNumberPadVC *)pkNumberPadVC digitPressed:(NSNumber *)digitPressed{
    if( ! [self orderLineSelectedForEdit]) return;
    
    [displayPad setDisplayPadSelectedLabelValue:[digitPressed stringValue]];

}
-(void)numberPadViewPointButtonDidPress:(PkNumberPadVC *)numberPadView{
    if( ! [self orderLineSelectedForEdit]) return;
    OrderTakingLabelType currentLabelType = [displayPad getSelectedLabelName];
    switch (currentLabelType) {
        case OrderTakingLabelType_Price:
            [self addPointSymbolToOrderLineCell];
            break;
        default:
            break;
    }
}
-(void)addPointSymbolToOrderLineCell{
    UILabel *currentLabel = [displayPad getSelectedLabel];
    NSMutableString *string = [currentLabel.text mutableCopy];
    if([string rangeOfString:@"."].location == NSNotFound){
        if([string hasPrefix:@"0"] == FALSE){ //&& [string length] > 0){
            [string appendString:@"."];
            [displayPad setDPPriceValue:string];
        }
        else{
            [string appendString:@"."];
            [displayPad setDPPriceValue:string];
        }
    }
}

-(void)numberPadViewReturnButtonDidPress:(PkNumberPadVC *)numberPadView{
    if( ! [self orderLineSelectedForEdit]) return;
    
    NSIndexPath *selectedRow = [_orderLineTable indexPathForSelectedRow];
    
    if(selectedRow){
        NSLog(@"editing orderline: %d", selectedRow.row);
        if([displayPad getSelectedLabel] == displayPad.quantityLabel){
            if([displayPad.quantityLabel.text isEqualToString:@"0"]){
                [[displayPad quantityLabel] setText:@"1"];
            }
        }
/* operation change
        OrderLineDataModel *oLDM = [[self.items objectForKey:[self.items keyAtIndex:selectedRow.section]] objectAtIndex:selectedRow.row-1];
 */
        
        int correctionDelta = 0;
        if(selectedRow.section == 0) correctionDelta = -1;
        
        OrderLineDataModel *oLDM = [[[self.sectionsAndItems objectAtIndex:selectedRow.section] sectionDiscountOrderLines] objectAtIndex:selectedRow.row+correctionDelta];

        [displayPad grabDisplayPadValuesAndSetOrderLineObject:oLDM];
        
        [_orderLineTable beginUpdates];
        [_orderLineTable reloadRowsAtIndexPaths:@[selectedRow] withRowAnimation:UITableViewRowAnimationFade];
        [_orderLineTable endUpdates];
        [_orderLineTable selectRowAtIndexPath:selectedRow animated:NO scrollPosition:UITableViewScrollPositionNone];

        [self calculateTotalWithItems];
    }
    [self shouldSelectionBoxCycleThroughDisplayPadOrAddNewOrderItemRow];
}
-(void)shouldSelectionBoxCycleThroughDisplayPadOrAddNewOrderItemRow{
    if(
       ([[displayPad.productLabel text] length] == 4) &&
       [[displayPad.quantityLabel text] integerValue] > 0 &&
       ([displayPad getSelectedLabel] == displayPad.priceLabel)){
        
     /* operation change
        NSLog(@"About to add: %@", [[self.items objectForKey:[self.items keyAtIndex:0]] objectAtIndex:[_orderLineTable indexPathForSelectedRow].row-1]);
       */
        int correctionDelta = 0;
        if([_orderLineTable indexPathForSelectedRow].section == 0) correctionDelta = -1;
        
        
        NSLog(@"About to add: %@", [[[self.sectionsAndItems objectAtIndex:[_orderLineTable indexPathForSelectedRow].section] sectionDiscountOrderLines] objectAtIndex:[_orderLineTable indexPathForSelectedRow].row+correctionDelta]);

        [displayPad canProcessProduct];
        [self addNewRowToOrderLineTable];
    }
    else{
        
        if([displayPad getSelectedLabel] == displayPad.productLabel){
            NSLog(@"analysing product label");
            if([displayPad.productLabel.text length] < 4){
                [displayPad.productLabel setBackgroundColor:COLOR_WITH_RGB(249, 198, 198, 255)];
                
                return;
            }
            else{
                [displayPad.productLabel setBackgroundColor:COLOR_WITH_RGB(255, 255, 255, 255)];
            }
        }
    }
    [displayPad moveSelectionToNextFrame];

    
}


-(void)numberPadViewBackspaceButtonDidPress:(PkNumberPadVC *)numberPadView{
    if( ! [self orderLineSelectedForEdit]) return;
    UILabel *currentLabel = [displayPad getSelectedLabel];
    NSMutableString *string = [currentLabel.text mutableCopy];
    if([string length] > 0){
        string = [[string substringWithRange:NSMakeRange(0, [string length] -1)] mutableCopy];
    }
    if ([string length] == 0) {
        //If the label is quantity then reset back to 1 since you have to buy a minimum of 1
        if(currentLabel.tag == 1){
            string = [NSMutableString stringWithFormat:@"0"];
        }
        else{
            string = [NSMutableString stringWithFormat:@"0"];
        }
    }
    [currentLabel setText:string];
    NSLog(@"backspace");

}







-(void)calculateTotalWithItems{
    /* operation change
    [AppConfig calculateTotalWithItemsDictionary:self.items withOrderTotalFooter:orderTotalFooterView];
     */
    [AppConfig calculateTotalWithSectionsArray:self.sectionsAndItems withOrderTotalFooter:orderTotalFooterView];

    
}

//===============================================
#pragma mark - CrossDiscount Delegate Methods -
//===============================================

-(void)crossDiscountDidClickInsertSectionButtonWithSectionHeaderTitle:(NSString*)headerTitle initial:(int)initial andFinal:(int)final{
    for(int i = 0; i < [self.sectionsAndItems count]; i++){
        
        if([[[self.sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count] == 0){
            [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"Can't add anymore until discount section: %d is dealt with",i+1]];
            return;
        }
    }

    SectionContainer *sectionContainer = [[SectionContainer alloc] initWithUniqueLinkingStringAndWithSectionTitleHeader:headerTitle
                                                                                                            withInitial:initial
                                                                                                               andFinal:final];
    
    
    [self.sectionsAndItems addObject:sectionContainer];

    
    [_orderLineTable beginUpdates];
    [_orderLineTable insertSections:[NSIndexSet indexSetWithIndex:[self.sectionsAndItems count]-1] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    // Update data model
    
    [_orderLineTable endUpdates];
    
}
@end