//
//  CustomersEditVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 07/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "CustomersEditVC.h"

@interface CustomersEditVC (){
    //Will store original data array where we can implement our predicates and searchers
    NSMutableArray *customersDataArray;
    NSMutableDictionary *customerTableIndices;
    
    //the output array after predicates and searches have been implemented on the original array
    NSMutableArray *currentArrayDisplayed;
    NSMutableArray *customersFilteredArray;
    BOOL loading;
//    id formViewController;
}

@end

@implementation CustomersEditVC
//@synthesize customersTable;


@synthesize serverRequest;
@synthesize agentTableRefreshControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavigationButtons];
    [self setupExtraVariables];
    [self retrieveMembers];
}

-(void)setNavigationButtons{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Members";
    
    
    UIBarButtonItem *addCustomerButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addCustomer:)];
    [self.navigationItem setRightBarButtonItem:addCustomerButton];
}


-(void)show:(id)sender{
    //    [[[AppDelegate getAppDelegateReference] splitViewController] toggleMasterView:sender];
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableAttributedString*)getRefreshControlAttributedStringDefaultWithString:(NSString *)s{
    NSMutableAttributedString *a = [[NSMutableAttributedString alloc] initWithString:s];
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: APP_COLOR_WHITE,
                                        };
    [a setAttributes:refreshAttributes range:NSMakeRange(0, a.length)];
    
    return a;
}
-(void)setRefreshControlToDefault{
    agentTableRefreshControl.attributedTitle = [self getRefreshControlAttributedStringDefaultWithString:@"Refresh Data"];
    
}

#pragma mark - ASIHTTPRequest methods
-(void)addCustomer:(id)sender{
//    UIView *modalView =
//    [[UIView alloc] initWithFrame:APP_FRAME_OF_WHOLE_SCREEN_LANDSCAPE];
//    modalView.opaque = NO;
//    modalView.backgroundColor =
//    [[UIColor blackColor] colorWithAlphaComponent:0.8f];
//        [self.navigationController.view addSubview:modalView];
//    
    AddCustomerVC *addCustomerVC = [[AddCustomerVC alloc] initWithNibName:@"AddCustomerVC" bundle:nil];
//    addCustomerVC.modalPresentationStyle = UIModalPresentationFormSheet;
    addCustomerVC.delegate = self;
//    formViewController = addCustomerVC;
    [((PKNavigationController*)[self.sideMenuViewController mainViewController]) pushViewController:addCustomerVC animated:YES];

//    [self retrieveWithRequestStringType:RequestCreateCustomer withDictionary:nil];
//    else if([typeOfRequest isEqualToString:RequestCreateCustomer]){
//        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestCreateCustomer forKey:@"RequestType"]];
//        
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//        [dict setObject:agentName.text forKey:@"name"];
//        [dict setObject:[NSNumber numberWithInteger:selectedStateId] forKey:@"stateid"];
//        [dict setObject:[NSNumber numberWithInteger:directionSelection.selectedSegmentIndex+1] forKey:@"directionid"];
//        [dict setObject:[agentBalance.text stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"balance"];
//        [dict setObject:agentPDiscount.text forKey:@"percentageDiscount"];
//        [dict setObject:[[agentComments.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0?agentComments.text:@"None" forKey:@"comment"];
//        
//        //    [dict setObject:[NSNumber numberWithInteger:6] forKey:@"listingorder"];
//        
//        NSString *jsonString = [dict JSONRepresentation];
//        [serverRequest appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
//        
//    }
}
-(void)retrieveMembers{
    [self retrieveWithRequestStringType:GetCustomersRequest withDictionary:nil];
}
-(void)retrieveWithRequestStringType:(NSString*)typeOfRequest withDictionary:(NSMutableDictionary*)inputdictionary{
    loading = TRUE;
    
    NSLog(@"loading = TRUE");
    NSLog(@"Retrieve %@ method called", typeOfRequest);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", typeOfRequest];
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    if([typeOfRequest isEqualToString:GetCustomersRequest]){
        [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:GetCustomersRequest forKey:@"RequestType"]];
    }
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
    
}

-(void)requestSucceeded:(ASIHTTPRequest*)request{
    loading = FALSE;
    
    //some weird table header view offset shit was happening and it happened when setting the refreshcontrol after the retrieve agents request was made.
    
    if(![agentTableRefreshControl.attributedTitle isEqualToAttributedString:[self getRefreshControlAttributedStringDefaultWithString:@"Refresh Data"]]){
        [self setRefreshControlToDefault];
    }
    NSLog(@"loading = false");
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSDictionary *JSONDictionary = [myString JSONValue];
    
    switch (statusCode) {
        case 400:
        case 401:
        {
            NSLog(@"display error message");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[request.responseString JSONValue] objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            break;
        }
        case 200:
            NSLog(@"status code = 200 so successful");
            
            //statesRetrieval succeeded
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:GetCustomersRequest]){
                NSLog(@"\n\nJSON RESULT: %@\n\n", JSONDictionary);
                [self setupCustomersTableWithDictionary:[JSONDictionary objectForKey:@"customers"]];
                [agentTableRefreshControl endRefreshing];
                
            }
            break;
        default:
            NSLog(@"went to none: %d or %@", statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]);
            break;
    }
}
-(void)requestFailed:(ASIHTTPRequest*)request{
    //statesRetrieval failed
    [self setRefreshControlToDefault];
    loading = FALSE;
    NSLog(@"loading = false");
    if([[[request userInfo] objectForKey:@"RquestType"] isEqualToString:GetCustomersRequest]){
        NSLog(@"retrieving states failed so trying again");
        [self retrieveMembers];
    }
}

#pragma mark -


-(void)setupCustomersTableWithDictionary:(NSDictionary*)inputJSONDictionary{
    [_agentsTable setBackgroundColor:[UIColor clearColor]];
    if (customersDataArray == nil) {
        customersDataArray = [[NSMutableArray alloc] init];
    }
    [customersDataArray removeAllObjects];
    NSMutableArray *unsortedAgentsDataArray = [[NSMutableArray alloc] init];
    
    for(id key in inputJSONDictionary){
        NSLog(@"Customer init in customer VC %@", key);
        CustomerDataModel *customerDataModel = [[CustomerDataModel alloc] initWithJSONData:key];
        [unsortedAgentsDataArray addObject:customerDataModel];
    }
    
    //##############################
    //SORT ARRAY
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"customerName" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    customersDataArray = [[unsortedAgentsDataArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    
    
    [self setTablePropertiesWithArray:customersDataArray];
}
-(void)setTablePropertiesWithArray:(NSMutableArray*)inputArray{
    currentArrayDisplayed = inputArray;
    NSLog(@"set table properties with array %@", inputArray);
    //##############################
    //CREATE INDICES
    // Loop through the books and create our keys
    if (customerTableIndices == nil) {
        customerTableIndices = [[NSMutableDictionary alloc] init];
    }
    [customerTableIndices removeAllObjects];
    
    BOOL found;
    for (CustomerDataModel *agent in currentArrayDisplayed){
        NSString *c = [agent.customerName substringToIndex:1];
        found = NO;
        
        for (NSString *str in [customerTableIndices allKeys]){
            if ([str isEqualToString:c]){
                found = YES;
                break;
            }
        }
        if (!found){
            NSLog(@"character: %@", c);
            [customerTableIndices setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    //##############################
    //POPULATE INDICES
    for (CustomerDataModel *customer in currentArrayDisplayed){
        [[customerTableIndices objectForKey:[customer.customerName substringToIndex:1]] addObject:customer];
    }
    
    //    NSLog(@"Agent index: %@", agentTableIndices);
    
    if([currentArrayDisplayed count] == 0){
//        [NoResultsView  noResultsView:noResultsContainer showNoDataImageFound:YES withText:@"No agents" forTable:_agentsTable];
    }
    else{
//        [NoResultsView hideNoResultsContainer:noResultsContainer withTableView:_agentsTable];
    }
    [_agentsTable reloadData];
}




#pragma mark - UITableView data source methods

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //    NSLog(@"a");
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if([customersFilteredArray count] > 0){
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if([currentArrayDisplayed count] > 0){
            return [customerTableIndices count];
        }
        //if no data loaded return 1 section
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [PreferencesConfig7 tableView:tableView heightForHeaderInSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
//    return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section];
    return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section withFontSize:APP_DEFAULT_HEADER_FONT_SIZE];

    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //    NSLog(@"b");
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    else{
        if([currentArrayDisplayed count] > 0){
            return [[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        }
        else{
            return 0;
        }
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"c");
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if(customersFilteredArray.count > 0){
            return [customersFilteredArray count];
        }
    }
    else{
        if([currentArrayDisplayed count] > 0){
            //return [agentsDataArray count];
            return [[customerTableIndices valueForKey:[[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
        }
        //if no data loaded display 3 cells so we can display loading cell near the middle of the table
    }
    return 3;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    else{
        if([currentArrayDisplayed count] > 0){
            //http://jomnius.blogspot.co.uk/2012/02/how-to-get-magnifying-glass-into.html
            NSMutableArray *indexList = [NSMutableArray arrayWithCapacity:[[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] count]+1];
            
            [indexList addObject:UITableViewIndexSearch];
            for (NSString *item in [[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]){
                
                [indexList addObject:[item substringToIndex:1]];
            }
            return indexList;
        }
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    if (index == 0)
    {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    return index-1; // due magnifying glass icon
}


#pragma mark - UITableView delegate methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if(customersFilteredArray.count == 0){
            //            NSLog(@"1");
            
//            //NoResultsCell *noResultsCell = [NoResultsCell alloc] init
            static NSString *simpleTableIdentifier = @"SimpleTableCell";
            
            NoResultsCell *cell = (NoResultsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NoResultsCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            if(indexPath.row == 2){
                ((UILabel *)cell.message).text = @"No Results"; // setText:@"Username"];
                
            }
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        else{
            //            NSLog(@"2");
            
            static NSString *cellID = @"SlideMenuCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
//                CustomArrowAccessoryView *accessory = [CustomArrowAccessoryView accessoryWithColor:APP_COLOR_MAIN];
//                cell.accessoryView =accessory;
                
            }
            cell.textLabel.text = [(CustomerDataModel*)[customersFilteredArray objectAtIndex:indexPath.row] customerName];
//            cell.detailTextLabel.text = [[(CustomerDataModel*)[customersFilteredArray objectAtIndex:indexPath.row] agentStateAbbreviation] uppercaseString];
            
            //            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
    }
    else {
        UITableViewCell *cell;
        UILabel *label;
        //No data yet
        //If original source does not contain any elements then display loading cell
        if([currentArrayDisplayed count] == 0){
            //            NSLog(@"3");
            
            static NSString *cellID = @"loadingcell";
            
            cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
                //[label setBackgroundColor:[UIColor redColor]];
                [cell.contentView addSubview:label];
            }
            if(indexPath.row == 2){
                if (loading == FALSE) {
                    NSLog(@"loading = false");
                    cell.textLabel.text = [NSString stringWithFormat:@"No agents"];
                }
                else{
                    NSLog(@"loading = true");
                    cell.textLabel.text = [NSString stringWithFormat:@"Loading Agents"];
                }
                //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
        }
        
        else if([currentArrayDisplayed count] > 0){
            //            NSLog(@"4");
            
            static NSString *cellID = @"productcell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                //label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
                //[label setBackgroundColor:[UIColor redColor]];
                //[cell.contentView addSubview:label];
            }
            CustomerDataModel *agentDM  = [[customerTableIndices valueForKey:[[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            
            if([PreferencesConfig7 isStringNumeric:[agentDM customerLastOrderDate]]){
                cell.textLabel.text = [NSString stringWithFormat:@"%@   (%@)", [agentDM customerName], [agentDM customerLastOrderDate]];
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@"%@", [agentDM customerName]];
            }
//            cell.detailTextLabel.text = [agentDM.agentStateAbbreviation uppercaseString];
            //            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//            CustomArrowAccessoryView *accessory = [CustomArrowAccessoryView accessoryWithColor:APP_COLOR_MAIN];
//            cell.accessoryView = accessory;
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
}
-(void)setupExtraVariables{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    agentTableRefreshControl = refreshControl;
    
    refreshControl.tintColor = APP_COLOR_WHITE;
    //    NSMutableAttributedString *a = [[NSMutableAttributedString alloc] initWithString:s];
    //    [a addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSRangeFromString(s)];
    //    refreshControl.attributedTitle = a;
    
    [self setRefreshControlToDefault];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    
    [refreshControl setBackgroundColor:APP_COLOR_MAIN];
    [_agentsTable addSubview:agentTableRefreshControl];
    
//    noResultsContainer = [[NoResultsView alloc] initWithPosition:_agentsTable.bounds withText:@"Loading" isHidden:NO];
//    [NoResultsView noResultsView:noResultsContainer showNoDataImageFound:YES withText:@"Loading" forTable:_agentsTable];
//    
//    [self.view addSubview:noResultsContainer];
    
}
-(void)handleRefresh:(id)sender{
    agentTableRefreshControl.attributedTitle = [self getRefreshControlAttributedStringDefaultWithString:@"Refreshing data"];
    
    NSLog(@"refreshing orders");
    //    noLoadingErrors = false;
    
    
    [self retrieveMembers];
    
}
#pragma mark - PKAddCustomerDelegate


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomerDataModel *customer = [[customerTableIndices valueForKey:[[[customerTableIndices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];//[customersDataArray objectAtIndex:indexPath.row];
    
    AddCustomerVC *addCustomerVC = [[AddCustomerVC alloc] initWithCustomerObject:customer];
//    addCustomerVC.modalPresentationStyle = UIModalPresentationFormSheet;
    addCustomerVC.delegate = self;
//    formViewController = addCustomerVC;
    [((PKNavigationController*)[self.sideMenuViewController mainViewController]) pushViewController:addCustomerVC animated:YES];//presentViewController:addCustomerVC animated:YES completion:nil];

}
-(void)addedCustomerVCWithCustomer:(CustomerDataModel *)customer{
    NSLog(@"Member: %@, successfully added.", customer.customerName);
//    [formViewController dismissViewControllerAnimated:NO completion:nil];
    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Member: %@, successfully added.", customer.customerName] maskType:SVProgressHUDMaskTypeBlack];
    
    [self retrieveMembers];
    
}
-(void)editedCustomerVCWithCustomer:(CustomerDataModel *)customer{
    NSLog(@"Member: %@, successfully edited.", customer.customerName);
//    [formViewController dismissViewControllerAnimated:NO completion:nil];
    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Member: %@, successfully edited.", customer.customerName] maskType:SVProgressHUDMaskTypeBlack];
    
    [self retrieveMembers];
    
}

-(void)deletedCustomer:(CustomerDataModel*)customer{
    NSLog(@"Member: %@, successfully deleted.", customer.customerName);
    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Member: %@, successfully deleted.", customer.customerName] maskType:SVProgressHUDMaskTypeBlack];
    
    [self retrieveMembers];

}




@end
