//
//  TakeOrderVersion2VC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 04/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PkNumberPadVC.h"
#import "DisplayPadVC.h"
#import "OrderTotalFooterVC.h"


#import "OrderedDictionary.h"

#import "CrossDiscountControlVC.h"
#import "ReceiptVC.h"
#import "SectionContainer.h"
#import "SectionFooterSubTotalViewVC.h"

#import "SectionHeaderSubTotalVC.h"

@interface TakeOrderVersion2VC : UIViewController
<UITableViewDataSource,
UITableViewDelegate,
PkNumberPadDelegate,
UIAlertViewDelegate,
UINavigationControllerDelegate,
UIAlertViewDelegate,
PkNumberPadDelegate,
UIPickerViewDelegate,
CrossDiscountControlDelegate>{
    
    PkNumberPadVC *numberPad;
    DisplayPadVC *displayPad;
    OrderTotalFooterVC *orderTotalFooterView;
    CrossDiscountControlVC *crossDiscount;

    
    UIBarButtonItem *showReceiptButton;
    UIBarButtonItem *applyCrossDiscountButton;
    UIBarButtonItem *reloadTableButton;

    
    
}
@property (weak, nonatomic) IBOutlet UITableView *orderLineTable;
@property (retain, nonatomic) ASIHTTPRequest *serverRequest;
@property (retain, nonatomic) CustomerDataModel *customerDM;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

-(id)initWithCustomerObject:(CustomerDataModel*)customerObject;

@end
