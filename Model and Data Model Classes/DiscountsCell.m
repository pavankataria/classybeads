//
//  DiscountsCell.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "DiscountsCell.h"

@implementation DiscountsCell

@synthesize discountsLineQuantityValue;
@synthesize discountsLineCodeValue;
@synthesize discountsLinePriceValue;
@synthesize discountsLineTotalValue;
@synthesize discountsLineDiscountTypeValue;
@synthesize discountsLineLineTotalValue;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDiscountsLineQuantityValue:(NSString *)quantityValue{
    [discountsLineQuantityLabel setText:quantityValue];
}
-(void)setDiscountsLineCodeValue:(NSString *)codeValue{
    [discountsLineCodeLabel setText:codeValue];
}
-(void)setDiscountsLinePriceValue:(NSString *)priceValue{
    [discountsLinePriceLabel setText:priceValue];
}
-(void)setDiscountsLineTotalValue:(NSString *)totalValue{
    [discountsLineTotalLabel setText:totalValue];
}



-(void)setDiscountsLineDiscountTypeValue:(NSString *)discountType{
    [discountsLineDiscountTypeLabel setText:discountType];
}
-(void)setDiscountsLineLineTotalValue:(NSString *)lineTotal{
    [discountsLineLineTotalValueLabel setText:lineTotal];
}
@end
