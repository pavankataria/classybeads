//
//  ProductEntryCVCell.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ProductViewingItemCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProductViewingItemCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
//        [self.layer setCornerRadius:10];
//        self.contentView.layer.cornerRadius = 10;
//        [self.contentView setBackgroundColor:[UIColor redColor]];
        NSLog(@"INIT WITH FRAME FOR CELL CALLEd");
    }
    return self;
}
-(void)setSelected:(BOOL)selected{

    
    if(selected){
        [self setBackgroundColor:APP_COLOR_MAIN];
        [self.productCodeLabel setTextColor:[UIColor whiteColor]];
        [self.productPriceLabel setTextColor:[UIColor whiteColor]];
        [self. productDescriptionLabel setTextColor:[UIColor whiteColor]];
        [self. productQuantityLabel setTextColor:[UIColor whiteColor]];

//        [productEntryVC setProductEntryFieldsWithProduct:[[finalProductsArray objectAtIndex:indexPath.row] product]];
        
//        NSLog(@"productCode: %@ indexPath: %ld state: %d", [currentCell productCodeLabel].text, (long)indexPath.row, [[finalProductsArray objectAtIndex:indexPath.row] stateSelected]);
        
    }
    else{
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self. productCodeLabel setTextColor:[UIColor blackColor]];
        [self. productPriceLabel setTextColor:COLOR_WITH_RGB(164, 43, 43, 255)];
        [self. productDescriptionLabel setTextColor:[UIColor blackColor]];
        [self. productQuantityLabel setTextColor:[UIColor blackColor]];
    }
    
    
//    self.backgroundColor = selected?[UIColor greenColor]:[UIColor grayColor];
    [super setSelected:selected];
}


/*-(void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}
*/
-(void) drawRect:(CGRect)rect {
    [super drawRect:rect];
//    
//    if (self.highlighted) {
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        CGContextSetRGBFillColor(context, 1, 0, 0, 1);
//        CGContextFillRect(context, self.bounds);
//    }
}
 
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
