//
//  ProductEntryFieldVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ProductEntryFieldVC.h"

@interface ProductEntryFieldVC (){
}

@end

@implementation ProductEntryFieldVC
//@synthesize currentlyEditingProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{

      self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
     return self;
}

- (void)viewDidLoad
{
      [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self commonSetup];
    
 }

-(void)commonSetup{
      [self.view setBackgroundColor:[UIColor clearColor]];

    self.managedObjectContext = [AppDelegate getAppDelegateReference].managedObjectContext;
    [self setEnabledAllProductEntryFields:NO];
    
    
//    _productCodeTextField.delegate = self;
//_productQuantityTextField.delegate = self;
//    _productDescriptionTextField.delegate = self;
//    _productPriceTextField.delegate = self;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:_productCodeTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:_productDescriptionTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:_productQuantityTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:_productPriceTextField];
    
    
    NSLog(@"product entry field has loaded");
    
 }
- (void)didReceiveMemoryWarning
{
      [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
 }


-(BOOL)isProductEntryFormValidWithError:(NSError**)error{
    
      if([[_productCodeTextField text] length] < 4){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Enter product Code" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"com.pavankataria.classybeads" code:1 userInfo:dict];
    }
    //    else if([[_productDescriptionTextField text] length] == 0){
    //        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    //        [dict setValue:@"Enter description" forKey:NSLocalizedDescriptionKey];
    //        *error = [NSError errorWithDomain:@"com.pavankataria.classybeads" code:2 userInfo:dict];
    //    }
    else if([[_productQuantityTextField text] length] == 0){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Enter quantity" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"com.pavankataria.classybeads" code:3 userInfo:dict];
    }
    else if( ! ([[_productPriceTextField text] floatValue] > 0) ){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Enter valid price" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"com.pavankataria.classybeads" code:4 userInfo:dict];
    }
     return (*error == nil ? YES : NO);
    
}
-(BOOL)isTheFormValid{
      NSError *error;
    BOOL isformValid = [self isProductEntryFormValidWithError:&error];
    
    if(error){
        NSLog(@"form is NOT valid because of error: %@", [error localizedDescription]);
    }
    else{
        NSLog(@"form is valid");
        
    }
     return isformValid ;
}

-(BOOL)doesInputString:(NSString*)inputString followCharacterSet:(NSCharacterSet*)characterSet{
      NSCharacterSet *myCharSet = characterSet;
    for (int i = 0; i < [inputString length]; i++) {
        unichar c = [inputString characterAtIndex:i];
        if (![myCharSet characterIsMember:c]) {
            NSLog(@"the character: %hu is not found in the charset: %@", c, myCharSet);
         
            return NO;
        }
    }
     return YES;
}



//==========================================================
#pragma mark - UITextField Delegate Methods -
//==========================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
  
    if(textField == _productCodeTextField){
        
        if([[textField text] length] == 4){
            [_productDescriptionTextField becomeFirstResponder];
        }
        else{
            [textField setBackgroundColor:COLOR_WITH_RGB(249, 198, 198, 255)];
        }
    }
    else if(textField == _productDescriptionTextField){
        [_productQuantityTextField becomeFirstResponder];
    }
    else if(textField == _productQuantityTextField){
        [_productPriceTextField becomeFirstResponder];
    }
    else if(textField == _productPriceTextField){
        if( ! [self isTheFormValid]){
            [_productCodeTextField becomeFirstResponder];
        }
        else{
            [self enterProduct];
        }
    }
 
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
  
    //    UITextField *a, *b, *c, *d;
    //    a = _productCodeTextField;
    //    b = _productDescriptionTextField;
    //    c = _productQuantityTextField;
    //    d = _productPriceTextField;
    //
    //    NSLog(@"%@", textField);
 
    
}

-(void)textFieldDidChange:(NSNotification *)notificationObject {
  
    // whatever you wanted to do
    NSLog(@"text field did change");
    UITextField *textField = (UITextField*)[notificationObject object];
    
    if(textField == _productCodeTextField){
        int maxChar = 4;
        int length = (int)[_productCodeTextField.text length] ;
        if(length != maxChar){
            //CLEAR
            [textField setBackgroundColor:COLOR_WITH_RGB(255, 255, 255, 255)];
            [self setEnabledAllProductEntryFields:NO];
            _currentlyEditingProduct = nil;
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(modifyingProduct)]){
                [self.delegate modifyingProduct];
            }
        }
        else{
            //COMPLETE - CHECK FOR PRODUCT now with CODE
            //product text field is legal - CHANGE GREEN
            [textField setBackgroundColor:COLOR_WITH_RGB(195, 255, 165, 255)];
            
            [self setEnabledAllProductEntryFields:YES];
            [self checkIfProductExistsWithCode:[[_productCodeTextField text] intValue]];
        }
    }
    /*
     [self enableProductEntryFieldsTillError];
     */
    
 
}

-(void)setProductEntryFieldsWithProduct:(Product*)inputProduct{
    
    if(inputProduct){
        _productCodeTextField.text = [inputProduct productCode];
        _productDescriptionTextField.text = [inputProduct productDescription];
        _productQuantityTextField.text = [[inputProduct productQuantity] stringValue];
        _productPriceTextField.text = [NSString stringWithFormat:@"%.2f", [[inputProduct productPrice] floatValue]];
        [self setEnabledAllProductEntryFields:YES];
    }
}
-(void)checkIfProductExistsWithCode:(NSInteger)productCode{
    _currentlyEditingProduct = [PKCoreDataSingleton retrieveProductWithCode:[_productCodeTextField text]];
    
    NSLog(@"productRetrieved: %@", [_currentlyEditingProduct description]);
    
    
    if(_currentlyEditingProduct){
        [self setProductEntryFieldsWithProduct:_currentlyEditingProduct];
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(didRetrieveProductAfterProductSearch:)]){
            [self.delegate didRetrieveProductAfterProductSearch:_currentlyEditingProduct];
        }

    }

    

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  
    // CODE
    if (textField == _productCodeTextField) {
        int maxChar = 4;
        //        [textField setBackgroundColor:COLOR_WITH_RGB(255, 255, 255, 255)];
        
        if( ! [self doesInputString:string followCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]]){
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > maxChar) ? NO : YES;
    }
    
    
    // DESCRIPTION
    else if(textField == _productDescriptionTextField){
        //        NSLog(@"description");
        if( [self doesInputString:string followCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,:/.-& "]]){
            return YES;
        }
    }
    
    // QUANTITY
    else if(textField == _productQuantityTextField){
        //        NSLog(@"quantity");
        //        NSLog(@"string: .%@. lol", string);
        if( ! [self doesInputString:string followCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]]){
            return NO;
        }
        int maxChar = 3;
        
        //Adding this below line somehow fixes the 'not being able to activate the backspace deletion' problem
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > maxChar) ? NO : YES;
    }
    
    // PRICE
    else if (textField == _productPriceTextField){
        
        if( ! [self doesInputString:string followCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789."]]){
            return NO;
        }
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        //Stops adding more than one decimal point
        NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
        
        //Makes sure that you cannot have more than 2 digits before the decimal point
        if([[arrayOfString firstObject] length] > 2) return NO;
        
        if ([arrayOfString count] > 2 ) return NO ;
        else{
            //Stops there being any more digits after the decimal point
            if([arrayOfString count] == 2){
                if([[arrayOfString lastObject] length] > 2){
                    return NO;
                }
            }
            return YES;
        }
    }
    
 
    return NO;
    
}


-(void)setEnabledAllProductEntryFields:(BOOL)enable{
  
    //    UITextField *b, *c, *d;
    ////    a = _productCodeTextField;
    //    b = _productDescriptionTextField;
    //    c = _productQuantityTextField;
    //    d = _productPriceTextField;
    //
    //    a.text = @"Aha";
    
    //    NSLog(@"a: %@ b: %@ c: %@ d: %@ ", a.text, b.text, c.text, d.text);
    //    _productCodeTextField;
    //Disable all fields
    
    [self setTextFieldEnabled:_productCodeTextField withBool:YES];
    
    [self setTextFieldEnabled:_productDescriptionTextField withBool:enable];
    [self setTextFieldEnabled:_productQuantityTextField withBool:enable];
    [self setTextFieldEnabled:_productPriceTextField withBool:enable];
    
    
    
    //    if([a.text length] != 4){
    //        a.enabled =  TRUE;
    //        return;
    //    }
    
 
    
}
-(void)setTextFieldEnabled:(UITextField*)textField withBool:(BOOL)makeActive {
  
    [textField setEnabled:makeActive];
    if(makeActive){
        [textField setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        [textField setBackgroundColor:COLOR_WITH_RGB(194, 190, 188, 255)];
        [textField setText:@""];
    }
 
}
-(void)enableProductEntryFieldsTillError{
  
    [self setEnabledAllProductEntryFields:NO];
    
    if([[_productCodeTextField text] length] < 4) return;
    
    [_productDescriptionTextField setEnabled:YES];
    [_productQuantityTextField setEnabled:YES];
    
    if([[_productQuantityTextField text] length] == 0) return;
    
    [_productPriceTextField setEnabled:YES];
    
    if( ! ([[_productPriceTextField text] floatValue] > 0) ){
        return;
    }
 
}

-(void)enterProduct{
  
    NSError *error;
    BOOL newProductAdded = false;
    if(_currentlyEditingProduct == nil){
        _currentlyEditingProduct = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Product"
                                    inManagedObjectContext:self.managedObjectContext];
        [_currentlyEditingProduct setProductCode:[_productCodeTextField text]];
        newProductAdded = true;
    }
    [_currentlyEditingProduct setProductDescription:[_productDescriptionTextField text]];
    [_currentlyEditingProduct setProductQuantity:[NSNumber numberWithInt:[[_productQuantityTextField text] intValue]]];
    [_currentlyEditingProduct setProductPrice:[NSNumber numberWithFloat:[_productPriceTextField.text floatValue]]];
    
    [_currentlyEditingProduct setProductNeedsUpdating:[NSNumber numberWithBool:YES]];
    
    
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Fail to save with error: \n%@\n%@", [error localizedDescription], [error userInfo] );
    }
    else{
        NSLog(@"save successful product: %@", [_currentlyEditingProduct description]);
        
        if(newProductAdded){
            if(self.delegate && [self.delegate respondsToSelector:@selector(didAddNewProduct:)]){
                [self.delegate didAddNewProduct:_currentlyEditingProduct];
            }
        }
        else{
            if(self.delegate && [self.delegate respondsToSelector:@selector(didModifyProduct:)]){
                [self.delegate didModifyProduct:_currentlyEditingProduct];
            }
        }
        
        [_productCodeTextField setText:@""];
        [_productQuantityTextField setText:@""];
        [_productDescriptionTextField setText:@""];
        [_productPriceTextField setText:@""];
        [_productCodeTextField becomeFirstResponder];
    }
 
}


@end
