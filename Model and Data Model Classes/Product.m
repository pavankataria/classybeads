//
//  Product.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "Product.h"


@implementation Product

@dynamic productDescription;
@dynamic productCode;
@dynamic productNeedsUpdating;
@dynamic productPrice;
@dynamic productQuantity;

@end
