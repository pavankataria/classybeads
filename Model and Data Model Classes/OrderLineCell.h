//
//  OrderLineCell.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderLineCell : UITableViewCell{
    IBOutlet UILabel * orderLineQuantityLabel;
    IBOutlet UILabel * orderLineCodeLabel;
    IBOutlet UILabel * orderLinePriceLabel;
    IBOutlet UILabel * orderLineTotalLabel;
    
    IBOutlet UILabel * orderLineDiscountTypeLabel;
    IBOutlet UILabel * orderLineLineTotalValueLabel;
    
}
@property (nonatomic, retain) NSString * orderLineQuantityValue;
@property (nonatomic, retain) NSString * orderLineCodeValue;
@property (nonatomic, retain) NSString * orderLinePriceValue;
@property (nonatomic, retain) NSString * orderLineTotalValue;



@property (nonatomic, retain) NSString * orderLineDiscountTypeValue;
@property (nonatomic, retain) NSString * orderLineLineTotalValue;



-(void)setOrderLineQuantityValue:(NSString *)quantityValue;
-(void)setOrderLineCodeValue:(NSString *)codeValue;
-(void)setOrderLinePriceValue:(NSString *)priceValue;
-(void)setOrderLineTotalValue:(NSString *)totalValue;
-(void)setOrderLineDiscountTypeValue:(NSString *)discountType;
-(void)setOrderLineLineTotalValue:(NSString *)lineTotal;


-(void)setCellWithOrderLineObject:(OrderLineDataModel*)oldm;
@end
