//
//  OrderLineDataModel.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrderLineDataModel.h"

@implementation OrderLineDataModel
@synthesize orderLineOrderId;
@synthesize orderLineQuantity;
@synthesize orderLineCode;
@synthesize orderLinePrice;
@synthesize orderLineOriginalTotal;

@synthesize orderLineDiscountDescription;
//@synthesize orderLineDiscountAmount;

//@synthesize orderLineDiscountedTotal;
@synthesize orderLineDiscount;
@synthesize orderLineFinalTotalPrice;

@synthesize orderLineDiscountTypeEnumStruct;
@synthesize orderLineLinkingString;
-(id)init{
    if (self = [super init]) {
        [self initialiseVariables];
    }
    return self;
}
-(BOOL)isWholeObjectEmpty{
    if([self.orderLineQuantity intValue] == 0 &&
       ( ! [self.orderLineCode length] >= 0) &&
       ( ! [self.orderLinePrice floatValue] > 0)){
        return YES;
    }
    else{
        return NO;
    }
}
-(BOOL)isOrderLineDataModelValuesValidWithError:(NSError**)error{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];

    if( ! ([self.orderLineQuantity intValue] > 0)){
        [dict setValue:@"Quantity not filled in" forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"PavanKataria" code:8001 userInfo:dict];
        return NO;
    }
    
    if( ! ([self.orderLineCode length] >= 4)){
        [dict setValue:@"Code not filled in" forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"PavanKataria" code:8002 userInfo:dict];
        return NO;
    }
    if( ! ([self.orderLinePrice floatValue] > 0)){
        [dict setValue:@"Price not set" forKey:NSLocalizedDescriptionKey];
        //Populate the error
        *error = [NSError errorWithDomain:@"PavanKataria" code:8003 userInfo:dict];
        return NO;
    }
    return YES;
}
-(id)initWithOrderLineDataObject:(OrderLineDataModel*)object{
    if (self = [super init]) {
        self.orderLineQuantity = object.orderLineQuantity;
        self.orderLineCode = object.orderLineCode;
        self.orderLinePrice = object.orderLinePrice;
        self.orderLineOriginalTotal = object.orderLineOriginalTotal;
        
        self.orderLineDiscount = object.orderLineDiscount;
        self.orderLineFinalTotalPrice = object.orderLineFinalTotalPrice;
        self.orderLineDiscountDescription = object.orderLineDiscountDescription;
        self.orderLineDiscountTypeEnumStruct = object.orderLineDiscountTypeEnumStruct;
        self.orderLineLinkingString = object.orderLineLinkingString;
        self.orderLineOrderId = object.orderLineOrderId;
        if(!self.orderLineLinkingString){
            self.orderLineLinkingString = @"none";
        }

        
    }
    return self;

}
-(void)initialiseVariables{
    self.orderLineQuantity = [NSNumber numberWithInt:0];
    self.orderLineCode = @"0";
    self.orderLinePrice = [NSNumber numberWithInt:0];
    self.orderLineDiscount = [NSNumber numberWithFloat:0.00];
    self.orderLineFinalTotalPrice = [NSNumber numberWithFloat:0.00];
    self.orderLineDiscountDescription = @"-";
    self.orderLineDiscountTypeEnumStruct = ProductDiscountNone;
    
    self.orderLineOriginalTotal = [NSNumber numberWithFloat:0.00];
    self.orderLineLinkingString = @"none";
}

-(id)initWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
        
        self.orderLineOrderId = [[data objectForKey:@"Order_Id"] integerValue];
        self.orderLineQuantity = [NSNumber numberWithInteger:[[data objectForKey:@"OrderLine_QuantitySoldATOP"] integerValue]];
        self.orderLineCode = [data objectForKey:@"OrderLine_ProductCodeATOP"];
        self.orderLinePrice = [NSNumber numberWithFloat:[[data objectForKey:@"OrderLine_PriceOfProductATOP"] floatValue]];
        self.orderLineOriginalTotal =[NSNumber numberWithFloat:[[data objectForKey:@"OrderLine_Total"] floatValue]];
        
        
        self.orderLineDiscountDescription = ([[data objectForKey:@"OrderLine_DiscountDescriptionATOP"] length] == 0 ? nil : [data objectForKey:@"OrderLine_DiscountDescriptionATOP"]);
//        self.orderLineDiscountTypeEnumStruct = ];
        self.orderLineDiscount = [NSNumber numberWithFloat:[[data objectForKey:@"OrderLine_Discount"] floatValue]];
        
        self.orderLineFinalTotalPrice = [NSNumber numberWithFloat:[[data objectForKey:@"OrderLine_FinalTotalPrice"] floatValue]];
        
        self.orderLineLinkingString = [data objectForKey:@"OrderLine_LinkingString"];
        if(self.orderLineLinkingString == [NSNull null]){
            self.orderLineLinkingString = @"none";
        }

        
        
        //That means the order is old so automatically set the order line final total to the original total
        if(!self.orderLineFinalTotalPrice){
            self.orderLineFinalTotalPrice = orderLineOriginalTotal;
        }
        if(!self.orderLineDiscount){
//            self.orderLineFinalTotalPrice = orderLineOriginalTotal;
            self.orderLineDiscount = [NSNumber numberWithFloat:0.00];
        }
        else{
            NSLog(@"coming in discount %.2f", [self.orderLineDiscount floatValue]);// self.orderLineDiscountDescription);
            self.orderLineDiscountTypeEnumStruct = [AppConfig productdDiscountOptionWithString:self.orderLineDiscountDescription];
        }
        
        if(!self.orderLineDiscountDescription){
            self.orderLineDiscountTypeEnumStruct = ProductDiscountNone;
            self.orderLineDiscountDescription = @"-";
        }
        else{
            NSLog(@"coming in description %@", self.orderLineDiscountDescription);// self.orderLineDiscountDescription);

        }
    }
    return self;
}

-(NSString*)orderLineConvertToTableDataRowIsEven:(BOOL)isEven{
    
    return [AppConfig getHTMLForRowWithOrderLineObject:self];
    
}

-(void)setDiscountwithDiscountType:(ProductDiscountOption)discountType{
//    self.orderLineDiscountTypeEnumStruct = discountType;
//
//    self.orderLineDiscountDescription = [AppConfig stringForProductDiscountOptionToString:discountType];
//    
//    self.orderLineDiscountedTotal = [AppConfig getOrderFinalDiscountAmountWithDiscountType:discountType withOrderLineQuantity:self.orderLineQuantity withOrderLinePrice:self.orderLinePrice withOrderLineOriginalTotal:self.orderLineOriginalTotal];
////
//    self.orderLineDiscountAmount = [NSNumber numberWithFloat:(([self.orderLineOriginalTotal floatValue] - [orderLineDiscountedTotal floatValue]) * [self.orderLineQuantity floatValue])]
//    ;
    
    DLog(@"disount type: %u", discountType);
    self.orderLineDiscountTypeEnumStruct = discountType;
    self.orderLineDiscountDescription = [AppConfig stringForProductDiscountOptionToString:discountType];
    self.orderLineDiscount = [AppConfig getDiscountAmountWithDiscountType:discountType withOrderLineQuantity:self.orderLineQuantity withOrderLinePrice:self.orderLinePrice withOrderLineOriginalTotal:self.orderLineOriginalTotal];
    
    self.orderLineFinalTotalPrice = [NSNumber numberWithFloat:([self.orderLinePrice floatValue] * [self.orderLineQuantity intValue])-[self.orderLineDiscount floatValue]];
    

    
    //    self.orderLineDiscountBeingReceived =
//    NSLog(@"Discounted Total For Product Code: %@ is %.2f", self.orderLineCode, [self.orderLineDiscountedTotal floatValue]);
}

-(NSString*)description{
    return [NSString stringWithFormat:@"\n<\n Qty: %@  \n Code: %@ \n Price:%@ \n Total:%@ \n -%% %@ \n discount: %.2f \n structEnum: %d \n Did conversion work?: %@ \n>",
            self.orderLineQuantity,
            self.orderLineCode,
            self.orderLinePrice,
            self.orderLineOriginalTotal,
            self.orderLineDiscountDescription,
            [self.orderLineDiscount floatValue],
            (int)self.orderLineDiscountTypeEnumStruct,
            [AppConfig stringForProductDiscountOptionToString:self.orderLineDiscountTypeEnumStruct]];
}

-(void)clearAnyPreviousDiscount{
    self.orderLineDiscountDescription = @"";
    self.orderLineDiscountTypeEnumStruct = ProductDiscountNone;
    self.orderLineDiscount = [NSNumber numberWithInt:0];
}
@end
