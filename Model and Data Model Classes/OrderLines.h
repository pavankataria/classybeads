//
//  OrderLines.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Orders;

@interface OrderLines : NSManagedObject

@property (nonatomic, retain) NSString * orderLineProductNameATOP;
@property (nonatomic, retain) NSNumber * orderLinePriceATOP;
@property (nonatomic, retain) NSNumber * orderLinePercentageDiscountATOP;
@property (nonatomic, retain) NSNumber * orderLineQuantitySoldATOP;
@property (nonatomic, retain) Orders *order;

@end
