//
//  V1toV2ProductMigrationPolicy.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 17/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "V1toV2ProductMigrationPolicy.h"
//#import "ProductV2.h"

@implementation V1toV2ProductMigrationPolicy


- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance
                                      entityMapping:(NSEntityMapping *)mapping
                                            manager:(NSMigrationManager *)manager
                                              error:(NSError **)error {
    
    // Create the product managed object
    NSManagedObject *newObject = [NSEntityDescription insertNewObjectForEntityForName:[mapping destinationEntityName]
                                                               inManagedObjectContext:[manager destinationContext]];
    
    [newObject setValue:[sInstance valueForKey:@"productCode"] forKey:@"productCode"];
    [newObject setValue:[sInstance valueForKey:@"productPrice"] forKey:@"productPrice"];
    [newObject setValue:[NSNumber numberWithInt:0] forKey:@"needsUpdating"];
    
    [manager associateSourceInstance:sInstance withDestinationInstance:newObject forEntityMapping:mapping];
    NSLog(@"percentage complete: %f", [manager migrationProgress]);
    
    return newObject != nil;
}


@end
