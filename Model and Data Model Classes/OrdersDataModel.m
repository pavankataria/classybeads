//
//  OrdersDataModel.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrdersDataModel.h"

@implementation OrdersDataModel
@synthesize orderId;
@synthesize orderReferenceNumber;
@synthesize orderCreatedDate;
@synthesize orderTotalAmount;
@synthesize orderRegionStateId;
@synthesize orderCustomerName;
@synthesize orderLinesDataArray;
@synthesize orderCustomerOrderCount;

//@synthesize orderDispatchedDate;
//@synthesize orderFinalisedDate;
//@synthesize orderAgentId;
//@synthesize orderStateNumber;
//@synthesize orderLevelBalance;
/*
 {"Order_Id":"1",
 "Order_ReferenceNumber":"Kotagiri\/1602\/001",
 "Order_CreatedDate":"2013-02-16 21:07:41",
 "Agent_Id":"87",
 "Order_State":"1"},
 */

-(id)initWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
        self.orderId = [[data objectForKey:@"Order_Id"] integerValue];
        self.orderReferenceNumber = [data objectForKey:@"Order_ReferenceNumber"];
        self.orderCreatedDate = [data objectForKey:@"Order_CreatedDate"];
        
        self.orderCustomerId = [[data objectForKey:@"Customer_Id"] integerValue];
        self.orderTotalAmount = [data objectForKey:@"Order_TotalAmount"];
        self.orderCustomerName = [data objectForKey:@"Customer_Name"];
        self.orderLinesDataArray = [[NSMutableArray alloc] init];
        self.orderCustomerOrderCount = [[data objectForKey:@"Customer_OrderCount"] integerValue];
        
        self.orderCustomerEmail = [data objectForKey:@"Customer_Email"];
        
        //        self.orderRegionStateId = [[data objectForKey:@"State_Id"] integerValue];
        //        self.orderFinalisedDate = [data objectForKey:@"Order_FinalisedDate"];
        //        self.orderStateNumber = [[data objectForKey:@"Order_State"] integerValue];
        //        self.orderLevelBalance = [[data objectForKey:@"Order_LevelBalance"] integerValue];
        
//        self.orderDispatchedDate = [data objectForKey:@"Order_DispatchedDate"];
        self.stateSelected = false;
        
        
        
//        if(self.orderFinalisedDate != nil){
//            
//            //NSLog(@"starting payment now");
//            NSDateFormatter *inputDateFormat = [[NSDateFormatter alloc] init];
//            [inputDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            NSDate *myInputDate = [inputDateFormat dateFromString:self.orderFinalisedDate];
//            
//            
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"dd/MM/yy"];
//            self.orderFinalisedDate  = [dateFormatter stringFromDate:myInputDate];
//        }
        
//        NSLog(@"customer id %d", (int)self.orderCustomerId );
    }
    return self;
}

-(NSString*)getOrderCreatedDateWithDateFormatString:(NSString*)dateFormat{
    
    NSDateFormatter *inputDateFormat = [[NSDateFormatter alloc] init];
    [inputDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *myInputDate = [inputDateFormat dateFromString:self.orderCreatedDate];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter stringFromDate:myInputDate];
}


@end
