//
//  OrderLineDataModel.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderLineDataModel : NSObject

@property (assign) NSInteger orderLineOrderId;
@property (nonatomic, retain) NSNumber * orderLineQuantity;
@property (nonatomic, retain) NSString * orderLineCode;
@property (nonatomic, retain) NSNumber * orderLinePrice;
@property (nonatomic, retain) NSNumber * orderLineOriginalTotal;

@property (nonatomic, retain) NSString * orderLineDiscountDescription;

@property (nonatomic, retain) NSNumber * orderLineFinalTotalPrice;
@property (nonatomic, retain) NSNumber * orderLineDiscount;


//This property will determine how products are linked together
//If this value is populated that means that there is another product also linked to it benefiting from the same discount

@property (nonatomic, retain) NSString * orderLineLinkingString;







//@property (nonatomic, retain) NSNumber * orderLineDiscountAmount;
//@property (nonatomic, retain) NSNumber * orderLineDiscountedTotal;


@property (assign) ProductDiscountOption orderLineDiscountTypeEnumStruct;

-(id)init;
-(id)initWithJSONData:(NSDictionary*)data;
-(id)initWithOrderLineDataObject:(OrderLineDataModel*)object;

-(NSString*)orderLineConvertToTableDataRowIsEven:(BOOL)isEven;
-(void)clearAnyPreviousDiscount;

-(void)setDiscountwithDiscountType:(ProductDiscountOption)discountType;

-(BOOL)isOrderLineDataModelValuesValidWithError:(NSError**)error;

-(BOOL)isWholeObjectEmpty;
@end
