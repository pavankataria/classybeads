//
//  ProductInstanceCVTemplate.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 25/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ProductInstanceCVTemplate.h"

@implementation ProductInstanceCVTemplate

-(id)initWithProduct:(Product*)inputProduct withState:(BOOL)state{
    self = [super init];
    if(self){
        
        self.product = inputProduct;
        self.deleteSelected = state;
        
        NSLog(@"productInstance description: %@", [self description]);
    }
    return self;
}

-(NSString*)productCode{
    return self.product.productCode;
}
-(NSString*)productDescription{
    return self.product.productDescription;

}
-(NSNumber*)productQuantity{
    return self.product.productQuantity;

}
-(NSNumber*)productPrice{
    return self.product.productPrice;

}
@end
