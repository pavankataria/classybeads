//
//  ProductEntryCVCell.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductViewingItemCell : UICollectionViewCell{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *productThumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *productCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *productQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;

@end
