//
//  DiscountsCell.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscountsCell : UITableViewCell{
    IBOutlet UILabel * discountsLineQuantityLabel;
    IBOutlet UILabel * discountsLineCodeLabel;
    IBOutlet UILabel * discountsLinePriceLabel;
    IBOutlet UILabel * discountsLineTotalLabel;
    
    
    IBOutlet UILabel * discountsLineDiscountTypeLabel;
    IBOutlet UILabel * discountsLineLineTotalValueLabel;
}
@property (nonatomic, retain) NSString * discountsLineQuantityValue;
@property (nonatomic, retain) NSString * discountsLineCodeValue;
@property (nonatomic, retain) NSString * discountsLinePriceValue;
@property (nonatomic, retain) NSString * discountsLineTotalValue;

@property (nonatomic, retain) NSString * discountsLineDiscountTypeValue;
@property (nonatomic, retain) NSString * discountsLineLineTotalValue;



@end
