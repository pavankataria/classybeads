//
//  totalCrossDiscountDataModel.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "TotalCrossDiscountDataModel.h"

@implementation TotalCrossDiscountDataModel
@synthesize tCDTotalDiscountValue;
@synthesize tCDTotalFinalValue;
@synthesize tCDTotalQuantityValue;

-(id)initWithTotalDiscountValue:(CGFloat)dv withTotalFinalValue:(CGFloat)fv withTotalQuantityValue:(int)qv{
    self = [super init];
    if(!self) return nil;
    self.tCDTotalDiscountValue = dv;
    self.tCDTotalFinalValue = fv;
    self.tCDTotalQuantityValue = qv;
    return self;

}
@end
