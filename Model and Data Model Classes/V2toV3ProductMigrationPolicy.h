//
//  V2toV3ProductMigrationPolicy.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 19/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ProductV3.h"

@interface V2toV3ProductMigrationPolicy : NSEntityMigrationPolicy
- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error;
@end
