//
//  V2toV3ProductMigrationPolicy.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 19/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "V2toV3ProductMigrationPolicy.h"

@implementation V2toV3ProductMigrationPolicy

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error {
    
    // Create the product managed object
    
    NSString *productCode = [sInstance valueForKey:Key(Product, productCode)];
    NSNumber *productNeedsUpdating = [sInstance valueForKey:@"needsUpdating"];
    NSNumber *productPrice = [sInstance valueForKey:Key(Product, productPrice)];

    //New members
    NSString *productDescription = @"";
    NSNumber *productQuantity = [NSNumber numberWithInt:0];
    
    
    
    Product *newProductInstance = [NSEntityDescription insertNewObjectForEntityForName:[mapping destinationEntityName]
                                                                inManagedObjectContext:[manager destinationContext]];
    
    

    [newProductInstance setProductCode: productCode];
    [newProductInstance setProductDescription: productDescription];
    [newProductInstance setProductQuantity:productQuantity];
    [newProductInstance setProductPrice:productPrice];
    [newProductInstance setProductNeedsUpdating:productNeedsUpdating];

    /**
     The previous old product entries didnt have anything for the last attribute,
     where as the new instances of product entity should have a detault value of YES.
     */
//    [newProductInstance setLol:[NSNumber numberWithInteger:8314]];
    
    
    // Set up the association between the old Product and the new Product for the migration manager
    
    [manager associateSourceInstance:sInstance
             withDestinationInstance:newProductInstance
                    forEntityMapping:mapping];
    return YES;
}


@end
