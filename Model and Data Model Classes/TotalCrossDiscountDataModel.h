//
//  totalCrossDiscountDataModel.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TotalCrossDiscountDataModel : NSObject
@property (nonatomic, assign) CGFloat tCDTotalDiscountValue;
@property (nonatomic, assign) CGFloat tCDTotalFinalValue;
@property (nonatomic, assign) int tCDTotalQuantityValue;

-(id)initWithTotalDiscountValue:(CGFloat)dv withTotalFinalValue:(CGFloat)fv withTotalQuantityValue:(int)qv;

@end
