//
//  OrdersDataModel.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 08/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrdersDataModel : NSObject

-(id)initWithJSONData:(NSDictionary*)data;


@property (assign) NSInteger orderId;
@property (assign) NSInteger orderRegionStateId;
@property (strong) NSString *orderReferenceNumber;
@property (strong) NSString *orderCreatedDate;
@property (assign) NSInteger orderCustomerId;
@property (strong) NSString* orderCustomerName;
@property (assign) NSInteger orderCustomerOrderCount;
@property (strong) NSString *orderTotalAmount;
@property (strong) NSString *orderCustomerEmail;

@property (strong) NSMutableArray *orderLinesDataArray;
//
//@property (strong) NSString *orderFinalisedDate;
//@property (strong) NSString *orderDispatchedDate;
//
//
//@property (assign) NSInteger orderStateNumber;
//@property (assign) NSInteger orderLevelBalance;


@property (assign) BOOL stateSelected;

-(NSString*)getOrderCreatedDateWithDateFormatString:(NSString*)dateFormat;

@end
