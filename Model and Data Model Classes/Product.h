//
//  Product.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Product : NSManagedObject

@property (nonatomic, retain) NSString * productDescription;
@property (nonatomic, retain) NSString * productCode;
@property (nonatomic, retain) NSNumber * productNeedsUpdating;
@property (nonatomic, retain) NSNumber * productPrice;
@property (nonatomic, retain) NSNumber * productQuantity;

@end
