//
//  totalCrossDiscountCell.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "TotalCrossDiscountCell.h"

@implementation TotalCrossDiscountCell
@synthesize tCDTotalDiscountValue;
@synthesize tCDTotalFinalValue;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setTCDTotalDiscountValue:(CGFloat)dv{
    
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];

    [totalDiscountLabel setText:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:[NSNumber numberWithFloat:dv]]]];
}
-(void)setTCDTotalFinalValue:(CGFloat)fv{
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    [totalFinalLabel setText:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:[NSNumber numberWithFloat:fv]]]];
}

-(void)setTCDTotalQuantityValue:(int)qv{
    [totalQuantityLabel setText:[NSString stringWithFormat:@"%d", qv]];
}
@end
