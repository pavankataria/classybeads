//
//  AgentDisplayCell.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 05/02/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface AgentDisplayCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *agentNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *agentCardinalDirectionLabel;
@property (nonatomic, strong) IBOutlet UILabel *agentStateLabel;
@property (nonatomic, strong) IBOutlet UILabel *agentBalanceLabel;

@end
