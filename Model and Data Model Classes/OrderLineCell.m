//
//  OrderLineCell.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "OrderLineCell.h"

@implementation OrderLineCell
//@synthesize orderLineQuantity;
//@synthesize orderLineCode;
//@synthesize orderLinePrice;
//@synthesize orderLineTotal;

@synthesize orderLineQuantityValue;
@synthesize orderLineCodeValue;
@synthesize orderLinePriceValue;
@synthesize orderLineTotalValue;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



-(void)setOrderLineQuantityValue:(NSString *)quantityValue{
    [orderLineQuantityLabel setText:quantityValue];
}
-(void)setOrderLineCodeValue:(NSString *)codeValue{
    [orderLineCodeLabel setText:codeValue];
}
-(void)setOrderLinePriceValue:(NSString *)priceValue{
    [orderLinePriceLabel setText:priceValue];
}
-(void)setOrderLineTotalValue:(NSString *)totalValue{
    [orderLineTotalLabel setText:totalValue];
}


-(void)setOrderLineDiscountTypeValue:(NSString *)discountType{
    [orderLineDiscountTypeLabel setText:discountType];
}
-(void)setOrderLineLineTotalValue:(NSString *)lineTotal{
    [orderLineLineTotalValueLabel setText:lineTotal];
}
-(void)setCellWithOrderLineObject:(OrderLineDataModel*)oldm{
//    [orderLineQuantityLabel setText:[oldm.orderLineQuantity stringValue]];
//    [orderLineCodeLabel setText:oldm.orderLineCode];
//    [orderLinePriceLabel setText:[oldm.orderLineFinalTotalPrice stringValue]];
//    [orderLineTotalLabel setText:[oldm.orderLineOriginalTotal stringValue]];
//    [orderLineDiscountTypeLabel setText:oldm.orderLineDiscountDescription];
//    [orderLineLineTotalValueLabel setText:[oldm.orderLineFinalTotalPrice stringValue]];
//    
    [self setOrderLineQuantityValue:[NSString stringWithFormat:@"%@", oldm.orderLineQuantity]];
    [self setOrderLineCodeValue:oldm.orderLineCode];
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    [self setOrderLinePriceValue:[NSString stringWithFormat:@"£%@", oldm.orderLinePrice]];
    [self setOrderLineTotalValue:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:oldm.orderLineOriginalTotal]]];
    [self setOrderLineDiscountTypeValue:oldm.orderLineDiscountDescription];
    [self setOrderLineLineTotalValue:[NSString stringWithFormat:@"%.2f", [oldm.orderLineFinalTotalPrice floatValue]]];

}

//APPLE BUG REPORT FILED
//-(void)willTransitionToState:(UITableViewCellStateMask)state{
//    [super willTransitionToState:state];
//
//    if(state & UITableViewCellStateShowingDeleteConfirmationMask){
//            NSLog(@"cell is about to be deleted");
//    }
//    else if(state & UITableViewCellStateShowingEditControlMask){
//            NSLog(@"Cell is back to normal state");
//    }
//    else{
//        NSLog(@"default LOLOL");
//    }
//}
//
//-(void)didTransitionToState:(UITableViewCellStateMask)state{
//    [super didTransitionToState:state];
//    if(state & UITableViewCellStateShowingDeleteConfirmationMask){
//        NSLog(@"cell deleted");
//    }
//    else if(state & UITableViewCellStateShowingEditControlMask){
//        NSLog(@"Cell did normal state");
//    }
//    else{
//        NSLog(@"default LOLOL");
//    }
//}


@end
