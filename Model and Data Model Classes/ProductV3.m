//
//  ProductV3.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ProductV3.h"

@implementation ProductV3

@dynamic productDescription;
@dynamic productCode;
@dynamic productNeedsUpdating;
@dynamic productPrice;
@dynamic productQuantity;

@end
