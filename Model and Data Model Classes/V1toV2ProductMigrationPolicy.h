//
//  V1toV2ProductMigrationPolicy.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 17/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <CoreData/CoreData.h>
@interface V1toV2ProductMigrationPolicy : NSEntityMigrationPolicy


- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error;

@end
