//
//  customerDataModel.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 04/02/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerDataModel : NSObject

@property (assign) NSInteger customerId;
@property (strong) NSString *customerName;
@property (assign) NSInteger customerOrderCount;
@property (strong) NSString *customerLastOrderDate;
@property (strong) NSString *customerPhone;
@property (strong) NSString *customerEmail;

@property (assign) BOOL stateSelected;

-(id)initWithJSONData:(NSDictionary*)data;
-(id)initCustomerAccountModelWithJSONData:(NSDictionary*)data;

@end
