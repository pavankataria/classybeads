//
//  ProductInstanceCVTemplate.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 25/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface ProductInstanceCVTemplate : NSObject
@property (strong) Product *product;
@property (assign) BOOL deleteSelected;


-(id)initWithProduct:(Product*)inputProduct withState:(BOOL)state;

-(NSString*)productCode;
-(NSString*)productDescription;
-(NSNumber*)productQuantity;
-(NSNumber*)productPrice;





@end
