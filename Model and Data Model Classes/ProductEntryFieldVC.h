//
//  ProductEntryFieldVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 24/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ProductEntryFieldVC;
@protocol ProductEntryFieldDelegate <NSObject>
@required
-(void)didAddNewProduct:(Product*)product;
-(void)didModifyProduct:(Product*)product;
-(void)didRetrieveProductAfterProductSearch:(Product*)product;
-(void)modifyingProduct;

@end



@interface ProductEntryFieldVC : UIViewController<UITextFieldDelegate>


-(void)setProductEntryFieldsWithProduct:(Product*)inputProduct;


@property (weak, nonatomic) IBOutlet UITextField *productCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *productDescriptionTextField;
@property (weak, nonatomic) IBOutlet UITextField *productQuantityTextField;
@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

//CurrentlyEditingProduct
@property (nonatomic, retain) Product *currentlyEditingProduct;


@property (nonatomic, weak) id<ProductEntryFieldDelegate> delegate;

@end
