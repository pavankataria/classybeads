//
//  TakeOrderVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 26/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PkNumberPadVC.h"
#import "DisplayPadVC.h"
#import "OrderTotalFooterVC.h"
#import "ReceiptVC.h"
#import "CustomerDataModel.h"
#import "CrossDiscountVC.h"

#import "OrdersDataModel.h"
//Core data stuff
#import "Product.h"
#import "AppDelegate.h"
#import "AppConfig.h"
#import "CrossDiscountVC.h"


#import "DiscountsCell.h"
#import "TotalCrossDiscountDataModel.h"
#import "TotalCrossDiscountCell.h"

@interface TakeOrderVC : UIViewController<UITableViewDataSource, UITableViewDelegate, PkNumberPadDelegate, UIAlertViewDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, CrossDiscountDelegate>{
    IBOutlet UILabel *rowOne;
    IBOutlet UILabel *rowTwo;
    IBOutlet UILabel *rowThree;
//    IBOutlet UILabel *rowFour;
    UILabel *selectionBorder;
//    IBOutletCollection(UIButton)NSMutableArray *numberPadButtons;
    PkNumberPadVC *numberPad;
    DisplayPadVC *displayPad;
    CrossDiscountVC *crossDiscount;
    OrderTotalFooterVC *orderTotalFooterView;

    UIBarButtonItem *showReceiptButton;
    UIBarButtonItem *applyCrossDiscountButton;
    UIBarButtonItem *reloadTableButton;
}
-(id)initInOrderEditModeWithOrderObject:(OrdersDataModel*)orderObject;
-(id)initWithCustomerObject:(CustomerDataModel*)customerObject;

@property (weak, nonatomic) IBOutlet UITableView *orderLineTable;
@property (retain, nonatomic) ASIHTTPRequest *serverRequest;
@property (retain, nonatomic) CustomerDataModel *customerDM;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

//-(void)numberPadKeyPressed:(id)sender;

@end
