//
//  MenuVC7.h
//  Rajam Oorvasi
//
//  Created by Pavan Kataria on 28/12/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MenuVC7 : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>{
    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;

    UITableView *menuListTable;
    NSIndexPath *lastSelectedIndex;
    
    NSArray *menuTitlesDataSourceArray;
}
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@property (retain, nonatomic) UITableView *menuTable;
@property (nonatomic, retain) UIImageView *backgroundImageView;
-(void)openOrderScreen;
@end



