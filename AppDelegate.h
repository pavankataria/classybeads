//
//  AppDelegate.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 25/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainVC7.h"
#import "MenuVC7.h"
#import "TWTSideMenuViewController.h"
#import "PKNavigationController.h"

#import "NDHTMLtoPDF.h"

typedef NS_ENUM(NSUInteger, CoreDataManagerState)
{
    CoreDataManagerStateUndefined,
    CoreDataManagerStateInitializing,
    CoreDataManagerStateMigrating,
    CoreDataManagerStateOperational,
};




@interface AppDelegate : UIResponder <UIApplicationDelegate/*, NDHTMLtoPDFDelegate*/ >{
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;


@property (retain, nonatomic) NSString *migrationStepDescription;

@property (assign) CGFloat migrationStepProgress;

@property (assign) CoreDataManagerState state;


@property (retain, nonatomic) TWTSideMenuViewController *sideMenuViewController;
@property (nonatomic, strong) MainVC7 *mainViewController;
@property (nonatomic, strong) MenuVC7 *menuViewController;



@property (strong, nonatomic) UIWindow *window;





- (NSString *)applicationDocumentsDirectory;
- (BOOL)progressivelyMigrateURL:(NSURL*)sourceStoreURL
                         ofType:(NSString*)type
                        toModel:(NSManagedObjectModel*)finalModel
                          error:(NSError**)error;


+(AppDelegate *)getAppDelegateReference;
@end
