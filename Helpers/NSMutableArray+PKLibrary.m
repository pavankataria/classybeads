//
//  NSObject+PKLibrary.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 04/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "NSMutableArray+PKLibrary.h"

@implementation NSMutableArray (PKLibrary)

-(id)objectAtIndexSafe:(NSUInteger)index {
    if (index >= [self count])
        return nil;
    return [self objectAtIndex:index];
}

@end


@implementation NSArray (PKLibrary)

-(id)objectAtIndexSafe:(NSUInteger)index {
    if (index >= [self count])
        return nil;
    return [self objectAtIndex:index];
}

@end
