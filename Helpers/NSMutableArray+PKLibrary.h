//
//  NSObject+PKLibrary.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 04/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (PKLibrary)

-(id)objectAtIndexSafe:(NSUInteger)index;
@end


@interface NSArray (PKLibrary)
-(id)objectAtIndexSafe:(NSUInteger)index;

@end