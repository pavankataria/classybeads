//
//  NoResultsCell.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 30/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "NoResultsCell.h"

@implementation NoResultsCell

@synthesize message;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
