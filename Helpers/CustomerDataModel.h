//
//  customerDataModel.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 04/02/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerDataModel : NSObject{
    
    
}

-(id)initWithJSONData:(NSDictionary*)data;
-(id)initCustomerAccountModelWithJSONData:(NSDictionary*)data;


@property (assign) NSInteger customerId;
@property (strong) NSString *customerName;
//@property (strong) NSString *customerBalance;
//@property (strong) NSString *customerPercentageDiscount;
@property (assign) NSInteger customerOrderCount;
//@property (strong) NSString *customerComments;

//@property (strong) NSString *customerStateId;
//@property (strong) NSString *customerStateName;
//@property (strong) NSString *customerStateAbbreviation;
//
//@property (assign) NSInteger customerDirectionId;
//@property (strong) NSString *customerDirectionName;
@property (strong) NSString *customerLastOrderDate;

@property (assign) BOOL stateSelected;


@end
