//
//  PreferencesConfig7.h
//  Rajam Oorvasi
//
//  Created by Pavan Kataria on 26/12/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>


#define LOGIN_FORM_TEXTFIELD_SPACING 30
#define COLOR_WITH_RGB(r,g,b,a)  [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:(a/255.0)]
#define COLOR_WITH_RGB_WITH_TINT(r,g,b,a)  [UIColor colorWithRed:(r+((255-r)*1/4)) green:(g+((255-g)*1/2)) blue:(b+((255-b)*3/4)) alpha:(a/255.0)]


//#define APP_COLOR_MAIN COLOR_WITH_RGB(50, 156, 210, 255)
//#define APP_COLOR_MAIN_TINT COLOR_WITH_RGB(50*1/4, 156*1/2, 210*3/4, 255)

#define APP_COLOR_R 255
#define APP_COLOR_G 83
#define APP_COLOR_B 13
#define APP_MULTIPLIER 1.1

#define APP_COLOR_MAIN APP_COLOR_MAIN_WITH_MULTIPLIER(APP_MULTIPLIER, 255)

#define APP_COLOR_MAIN_WITH_MULTIPLIER(multiplierOneToTwo, alpha) [PreferencesConfig7 redistributeRGBwithR:APP_COLOR_R*multiplierOneToTwo g:APP_COLOR_G*multiplierOneToTwo b:APP_COLOR_B*multiplierOneToTwo a:alpha]


//COLOR_WITH_RGB_WITH_TINT(236, 70, 0, 255)
#define APP_COLOR_MAIN_TINT COLOR_WITH_RGB(236*1/4, 70*1/2, 0*3/4, 255)

#define APP_COLOR_WHITE COLOR_WITH_RGB(255, 245, 233, 255)

#define defaultAnimationDuration 0.6
#define APP_FRAME_OF_WHOLE_SCREEN_LANDSCAPE CGRectMake(0, 0, CGRectGetHeight([UIScreen mainScreen].bounds), CGRectGetWidth([UIScreen mainScreen].bounds))

#define APP_DEFAULT_HEADER_FONT_SIZE 25
#define APP_DEFAULT_HEADER_MARGIN_HEIGHT 5
#define APP_DEFAULT_HEADER_HEIGHT 70
@interface PreferencesConfig7 : NSObject{
    
}


+ (UIColor*) getMainBackgroundImage;
+ (UIImage *) imageWithColor:(UIColor *)color;
+ (UIImage *) getMainBGImageWithBlur:(BOOL)addBlur;
+ (UIImage *) getMenuBGImageWithBlur;
+(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
+(UIView *)tableView:(UITableView *)tableView editOrderViewForHeaderInSection:(NSInteger)section;
+ (UIView*) getDefaultSelectedBackgroundView;

+ (BOOL) isStringNumeric:(NSString*)string;
+(UIColor*)redistributeRGBwithR:(CGFloat)r g:(CGFloat)g b:(CGFloat)b a:(CGFloat)a;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section withFontSize:(int)fontSize;
+(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;


+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
@end
