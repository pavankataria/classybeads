//
//  CrossDiscountControlVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 05/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CrossDiscountControlDelegate <NSObject>

@required

-(void)crossDiscountDidClickInsertSectionButtonWithSectionHeaderTitle:(NSString*)headerTitle initial:(int)initial andFinal:(int)final;

@end

@interface CrossDiscountControlVC : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>{
    __weak id <CrossDiscountControlDelegate> delegate;
    
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIButton *addSectionButon;
}
- (IBAction)insertSection:(id)sender;

@property (weak) id <CrossDiscountControlDelegate> delegate;

@end
