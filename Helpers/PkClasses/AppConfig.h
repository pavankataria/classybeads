//
//  AppConfig.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 21/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <Foundation/Foundation.h>
#import "OrderTotalFooterVC.h"
#import "SectionContainer.h"
//#import "Structs.h"


#define PK_TESTING NO
#define ALPHA                   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC                 @"1234567890"
#define ALPHA_NUMERIC           ALPHA NUMERIC
#define ALPHA_NUMERIC_WITHSPACE           ALPHA NUMERIC @" @"


//#define DEMO_URL2 @"http://ec2-54-251-74-199.ap-southeast-1.compute.amazonaws.com/DEMO"
//#define LIVE_URL2 @"http://ec2-54-251-74-199.ap-southeast-1.compute.amazonaws.com"
//#define WEB_SERVIVES_ACTIVE_URL2 @"none"//DEMO_URL


#define APP_NAME @"appName_OORVASI"
#define COLOR(r,g,b,a)  [UIColor colorWithRed:r green:g blue:b alpha:a]
#define COLOR_WITH_RGB(r,g,b,a)  [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:(a/255.0)]
#define TESTING_URL @"http://riabp.com/LIVE/"

#define GetCustomersRequest @"Get/Customers"
#define GetCustomersRequest2 @"Get/Customers"
#define GetOrdersRequest @"Get/Orders"

#define ORDER_ITEM_ROW_HEIGHT 80
#define ORDERLINE_CELL_ROW_HEIGHT 50
#define APP_ORDER_ITEM_HEADER_HEIGHT 40
#define HEIGHT_OF_PRODUCT_ENTRY_CELL 70


#ifdef DEBUG
#define Key(class, key)              ([(class *)nil key] ? @#key : @#key)
#define ProtocolKey(protocol, key)   ([(id <protocol>)nil key] ? @#key : @#key)
#else
#define Key(class, key)              @#key
#define ProtocolKey(class, key)      @#key
#endif



enum{
    CurrentDeviceTypeIPAD = 0,
    CurrentDeviceTypeIPHONE4 = 1,
    CurrentDeviceTypeIPHONE5 = 2
}CurrentDeviceType;

typedef enum {
    OrderTakingLabelType_Quantity  = 0,
    OrderTakingLabelType_ProductCode = 1,
    OrderTakingLabelType_Price = 2
} OrderTakingLabelType;


typedef NS_ENUM(NSUInteger, ProductDiscountOption)
{
    ProductDiscountNone = 0,
    ProductDiscount5Percent,
    ProductDiscount10Percent,
    ProductDiscount15Percent,
    ProductDiscount20Percent,
    ProductDiscountBuy1Get1FreePercent,
    ProductDiscount6For4,
};

#import "OrderLineDataModel.h"

@interface AppConfig : NSObject{
    
}

+(NSInteger) currentIOSDeviceType;
+(BOOL) validateEmail:(NSString *) email;
+(NSString *) addSuffixToNumber:(int)number;


+(NSString*)stringForProductDiscountOptionToString:(ProductDiscountOption)option;
//+(NSNumber*)getOrderFinalDiscountAmountWithDiscountType:(ProductDiscountOption)discountType andOrderLineOriginalTotal:(NSNumber*)originalTotal withOrderLineQuantity:(NSNumber*)orderLineQuantity;

+(NSNumber*)getOrderFinalDiscountAmountWithDiscountType:(ProductDiscountOption)discountType withOrderLineQuantity:(NSNumber*)orderLineQuantity withOrderLinePrice:(NSNumber*)orderLinePrice withOrderLineOriginalTotal:(NSNumber*)originalTotal __deprecated_msg("This method is deprecated. Use getDiscountAmountWithDiscountType:withOrderLineQuantity:withOrderLinePrice:withOrderLineOriginalTotal instead");

+(ProductDiscountOption)productdDiscountOptionWithString:(NSString*)string;

+(NSNumber*)getDiscountAmountWithDiscountType:(ProductDiscountOption)discountType withOrderLineQuantity:(NSNumber*)orderLineQuantity withOrderLinePrice:(NSNumber*)orderLinePrice withOrderLineOriginalTotal:(NSNumber*)originalTotal;


//FORMATTING FOR HTML IN EDIT ORDERS PAGE
+(NSString*)getHTMLForTableWithHeadings:(NSArray*)headings;

+(NSString*)getHTMLForRowWithOrderLineObject:(OrderLineDataModel*)oLObject;
+(NSArray*)setupReceiptArrayWithArray:_invoiceDataArray;
+(NSArray*)getDiscountsArrayFromFinalArray:(NSArray*)finalArray;
+(void)calculateTotalWithItemsWithArray:(NSMutableArray*)orderLinesArray withAllDiscountsArray:(NSMutableArray*)allDiscountsTotalArray withOrderTotalFooter:(OrderTotalFooterVC*)orderTotalFooterView;

+(void)calculateTotalWithItemsDictionary:(NSMutableDictionary*)orderLines withOrderTotalFooter:(OrderTotalFooterVC*)orderTotalFooterView;
+(void)calculateTotalWithSectionsArray:(NSMutableArray*)sectionsAndItems withOrderTotalFooter:(OrderTotalFooterVC*)orderTotalFooterView;


+(CGFloat)aOf:(int)numberOfItems bOf:(int)totalOfReductionItems cOf:(NSNumber*)orderLineQuantity dOf:(NSNumber*)orderLinePrice;
@end
