//
//  AppConfig.m
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 21/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "AppConfig.h"

#import "TotalCrossDiscountDataModel.h"
#import "OrderTotalFooterVC.h"

@implementation AppConfig: NSObject {
    
}

+(BOOL)validateEmail:(NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}
+(NSInteger)currentIOSDeviceType{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if ([[UIScreen mainScreen] bounds].size.height > 480) {
            return CurrentDeviceTypeIPHONE5;
        }else{
            return CurrentDeviceTypeIPHONE4;
        }
    }
    return CurrentDeviceTypeIPAD;
}



+(NSString *) addSuffixToNumber:(int) number
{
    NSString *suffix;
    int ones = number % 10;
    int temp = floor(number/10.0);
    int tens = temp%10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    NSString *completeAsString = [NSString stringWithFormat:@"%d%@",number,suffix];
    return completeAsString;
}



+(NSString*)stringForProductDiscountOptionToString:(ProductDiscountOption)option{
    NSDictionary *optionStrings =
    @{
      @(ProductDiscountNone) : @"-",
      @(ProductDiscount5Percent) : @"5%",
      @(ProductDiscount10Percent) : @"10%",
      @(ProductDiscount15Percent) : @"15%",
      @(ProductDiscount20Percent) : @"20%",
      @(ProductDiscountBuy1Get1FreePercent) : @"Buy1 G1Free",
      @(ProductDiscount6For4) : @"6 for 4",
    };
    return [optionStrings objectForKey:@(option)];
}
+(ProductDiscountOption)productdDiscountOptionWithString:(NSString*)string{
    NSDictionary *optionObjects =
    @{
      @"-" :    @(ProductDiscountNone),
      @"5%" :   @(ProductDiscount5Percent) ,
      @"10%" :  @(ProductDiscount10Percent),
      @"15%" :  @(ProductDiscount15Percent),
      @"20%" :  @(ProductDiscount20Percent),
      @"Buy1 G1Free" : @(ProductDiscountBuy1Get1FreePercent),
      @"6 for 4" : @(ProductDiscount6For4),
      };
    return (ProductDiscountOption)[optionObjects objectForKey:string];
}

+(NSNumber*)getDiscountAmountWithDiscountType:(ProductDiscountOption)discountType withOrderLineQuantity:(NSNumber*)orderLineQuantity withOrderLinePrice:(NSNumber*)orderLinePrice withOrderLineOriginalTotal:(NSNumber*)originalTotal {
    
    switch (discountType) {
        case ProductDiscountNone:
            return [NSNumber numberWithInteger:0];
            break;
        case ProductDiscount5Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.05)] floatValue]];
            break;
        case ProductDiscount10Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.10)] floatValue]];
            break;
        case ProductDiscount15Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.15)] floatValue]];
            break;
        case ProductDiscount20Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.20)] floatValue]];
            break;
        case ProductDiscountBuy1Get1FreePercent:
        {
            return [self calculateProductOfferOf:2 productFor:1 withOrderLineQuantity:orderLineQuantity withOrderLinePrice:orderLinePrice];
            break;
            
        }
        case ProductDiscount6For4:
            return [self calculateProductOfferOf:6 productFor:4 withOrderLineQuantity:orderLineQuantity withOrderLinePrice:orderLinePrice];
            break;
        default:
            break;
    }


}
+(NSNumber*)getOrderFinalDiscountAmountWithDiscountType:(ProductDiscountOption)discountType withOrderLineQuantity:(NSNumber*)orderLineQuantity withOrderLinePrice:(NSNumber*)orderLinePrice withOrderLineOriginalTotal:(NSNumber*)originalTotal {
    
    switch (discountType) {
        case ProductDiscountNone:
            return originalTotal;
            break;
        case ProductDiscount5Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.95)] floatValue]];
            break;
        case ProductDiscount10Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.90)] floatValue]];
            break;
        case ProductDiscount15Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.85)] floatValue]];
            break;
        case ProductDiscount20Percent:
            return [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",([originalTotal floatValue]*0.80)] floatValue]];
            break;
        case ProductDiscountBuy1Get1FreePercent:
        {
            return [self calculateProductOfferOf:2 productFor:1 withOrderLineQuantity:orderLineQuantity withOrderLinePrice:orderLinePrice];
            break;
            
        }
        case ProductDiscount6For4:
            return [self calculateProductOfferOf:6 productFor:4 withOrderLineQuantity:orderLineQuantity withOrderLinePrice:orderLinePrice];
            break;
        default:
            break;
    }
}
+(CGFloat)aOf:(int)numberOfItems bOf:(int)totalOfReductionItems cOf:(NSNumber*)orderLineQuantity dOf:(NSNumber*)orderLinePrice{
    int i = numberOfItems;     //n 3 - products total
    int f = totalOfReductionItems;              //r 2 - for the price of
    CGFloat p = [orderLinePrice floatValue];    //p £4
    int q = [orderLineQuantity intValue];       //q 4
    
    
    
    NSLog(@"i: %d f: %d pr: %.2f qty: %d", i, f, p, q);
    if(i == 0){
        return 0;
    }
    return ((((CGFloat)q)*p)-(((CGFloat)((int)(q/i)) * (CGFloat)(p*f)) + (CGFloat)((q%i)*p)));

}
+(NSNumber*)calculateProductOfferOf:(int)numberOfItems productFor:(int)totalOfReductionItems withOrderLineQuantity:(NSNumber*)orderLineQuantity withOrderLinePrice:(NSNumber*)orderLinePrice{
    int i = numberOfItems;     //n 3 - products total
    int f = totalOfReductionItems;              //r 2 - for the price of
    CGFloat p = [orderLinePrice floatValue];    //p £4
    int q = [orderLineQuantity intValue];       //q 4
    
   
    
    NSLog(@"i: %d f: %d pr: %.2f qty: %d", i, f, p, q);
    return [NSNumber numberWithFloat:(((CGFloat)q)*p)-(((CGFloat)((int)(q/i)) * (CGFloat)(p*f)) + (CGFloat)((q%i)*p))];
}

+(NSString*)getHTMLForTableWithHeadings:(NSArray*)headings{
    NSMutableString *tableHeader = [[NSMutableString alloc] init];
    
    [tableHeader appendString:[NSString stringWithFormat:@"<tr><td colspan='%d'><center>Order</center></td></tr>", [headings count]]];
    
    NSString *startOfString = @"<tr>";
    NSString *endOfString = @"</tr>";

    [tableHeader appendString:startOfString];
    for(int i = 0; i < [headings count]; i++){
        [tableHeader appendString:[NSString stringWithFormat:@"<th>%@</th>", [headings objectAtIndex:i]]];
    }
    
    [tableHeader appendString:endOfString];
    
    return tableHeader;
}

+(NSString*)getHTMLForRowWithOrderLineObject:(OrderLineDataModel*)oLObject{
    
    
    NSMutableString *tableRow = [[NSMutableString alloc] initWithString:@"<tr>"];
    
    [tableRow appendString:[NSString stringWithFormat:@"<td>%@</td>", oLObject.orderLineQuantity]];
    [tableRow appendString:[NSString stringWithFormat:@"<td>%@</td>", oLObject.orderLineCode]];
    [tableRow appendString:[NSString stringWithFormat:@"<td>%@</td>", oLObject.orderLinePrice]];
    [tableRow appendString:[NSString stringWithFormat:@"<td>%@</td>", oLObject.orderLineOriginalTotal]];
    [tableRow appendString:[NSString stringWithFormat:@"<td>%@</td>", oLObject.orderLineDiscount]];
    [tableRow appendString:[NSString stringWithFormat:@"<td>%@</td>", oLObject.orderLineFinalTotalPrice]];
    
    
    [tableRow appendString:@"</tr>"];
    return tableRow;
}
+(NSArray*)getDiscountsArrayFromFinalArray:(NSArray*)finalArray{
    //We start at one since we want to ignore the NONE array as that will not have a sub footer for total discounts
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for(int i = 1; i < [finalArray count]; i++){
        int quantity = 0;
        CGFloat discount = 0;
        CGFloat finalValue = 0;
        
        for(int k = 0; k < [[finalArray objectAtIndex:i] count]; k++){
            OrderLineDataModel *codm = [[finalArray objectAtIndex:i] objectAtIndex:k];
            quantity += [codm.orderLineQuantity intValue];
            discount += [codm.orderLineDiscount floatValue];
            finalValue += [codm.orderLineFinalTotalPrice floatValue];
        }
        TotalCrossDiscountDataModel *tcdc = [[TotalCrossDiscountDataModel alloc] initWithTotalDiscountValue:discount withTotalFinalValue:finalValue-discount withTotalQuantityValue:quantity];
        
        [array addObject:tcdc];
    }
    return array;
}
+(NSArray*)setupReceiptArrayWithArray:_invoiceDataArray{
    int i = 0;
    
    NSMutableDictionary *linkedOrderLines = [[NSMutableDictionary alloc] init];
    
    for(OrderLineDataModel * oldm in _invoiceDataArray){
        NSLog(@"code: %@ linkingString: %@", oldm.orderLineCode, oldm.orderLineLinkingString);
        if([oldm.orderLineLinkingString length] == 0) oldm.orderLineLinkingString = @"none";
        
        if(![linkedOrderLines objectForKey:oldm.orderLineLinkingString]){
            [linkedOrderLines setObject:[[NSMutableArray alloc] init] forKey:oldm.orderLineLinkingString];
        }
        [[linkedOrderLines objectForKey:oldm.orderLineLinkingString] addObject:oldm];
        
        NSLog(@"%d - %@ %@", i, oldm.orderLineCode, oldm.orderLineLinkingString);
    }
    NSLog(@"dictionary: %@", linkedOrderLines);
    
    
    int quantity = 0;
    CGFloat discount = 0;
    CGFloat final = 0;
    for(NSString *key in linkedOrderLines){
        NSArray *array = [linkedOrderLines objectForKey:key];
        
        for(OrderLineDataModel *odm in array){
            quantity += [odm.orderLineQuantity intValue];
            discount += [odm.orderLineDiscount floatValue];
            final += [odm.orderLineFinalTotalPrice floatValue];
        }
        
    }
    
    NSLog(@"qua: %d dis: %.2f fin: %.2f", quantity, discount, final);
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if(![linkedOrderLines objectForKey:@"none"]){
        [array addObject:[[NSArray alloc] init]];

    }
    for(NSString *key in linkedOrderLines){
        if([key isEqualToString:@"none"]){
            [array insertObject:[linkedOrderLines objectForKey:key] atIndex:0];
        }
        else{
            [array addObject:[linkedOrderLines objectForKey:key]];
        }
    }
    NSLog(@"");
    
    return array;
}
+(void)calculateTotalWithItemsDictionary:(NSMutableDictionary*)orderLines withOrderTotalFooter:(OrderTotalFooterVC*)orderTotalFooterView{
    NSUInteger totalItems = 0;
    CGFloat subTotal = 0.0f, discountTotal = 0.0f, finalTotal = 0.0f;
    
    for(NSString *key in orderLines){
        for(OrderLineDataModel *currentOL in [orderLines objectForKey:key]){
            if([currentOL.orderLineOriginalTotal floatValue] > 0){
                totalItems += [currentOL.orderLineQuantity integerValue];
                subTotal += [currentOL.orderLineOriginalTotal floatValue];
                discountTotal += [currentOL.orderLineDiscount floatValue];
            }
        }
    }
    
    finalTotal = subTotal - discountTotal;
 
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setSubTotalCost:subTotal];
    [orderTotalFooterView setDiscountTotal:discountTotal];
    [orderTotalFooterView setTotalCost:finalTotal];
}


+(void)calculateTotalWithSectionsArray:(NSMutableArray*)sectionsAndItems withOrderTotalFooter:(OrderTotalFooterVC*)orderTotalFooterView{

    NSUInteger totalItems = 0;
    CGFloat subTotal = 0.0f, discountTotal = 0.0f, finalTotal = 0.0f;
    
    for(int i = 0; i < [sectionsAndItems count]; i++){
        
        for(int k = 0; k < [[[sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] count]; k++){
            OrderLineDataModel *currentOL = [[[sectionsAndItems objectAtIndex:i] sectionDiscountOrderLines] objectAtIndex:k];
            if([currentOL.orderLineOriginalTotal floatValue] > 0){
                totalItems += [currentOL.orderLineQuantity integerValue];
                subTotal += [currentOL.orderLineOriginalTotal floatValue];
                discountTotal += [currentOL.orderLineDiscount floatValue];
            }
        }
    }
    
    finalTotal = subTotal - discountTotal;
    
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setSubTotalCost:subTotal];
    [orderTotalFooterView setDiscountTotal:discountTotal];
    [orderTotalFooterView setTotalCost:finalTotal];

}


+(void)calculateTotalWithItemsWithArray:(NSMutableArray*)orderLinesArray withAllDiscountsArray:(NSMutableArray*)allDiscountsTotalArray withOrderTotalFooter:(OrderTotalFooterVC*)orderTotalFooterView{
    NSUInteger totalItems = 0;
    CGFloat subTotal = 0.0f, discountTotal = 0.0f, finalTotal = 0.0f;
    
    for(NSArray *carray in orderLinesArray){
        for(OrderLineDataModel *currentOL in carray){
            if([currentOL.orderLineOriginalTotal floatValue] > 0){
                totalItems += [currentOL.orderLineQuantity integerValue];
                subTotal += [currentOL.orderLineOriginalTotal floatValue];
                discountTotal += [currentOL.orderLineDiscount floatValue];
            }
        }
    }
    finalTotal = subTotal - discountTotal;
    
//    for(TotalCrossDiscountDataModel *tCD in allDiscountsTotalArray){
//        totalItems += tCD.tCDTotalQuantityValue;
//        
//        discountTotal += tCD.tCDTotalDiscountValue;
//        finalTotal += tCD.tCDTotalFinalValue;
//        subTotal += tCD.tCDTotalFinalValue+tCD.tCDTotalDiscountValue;
//    }
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setSubTotalCost:subTotal];
    [orderTotalFooterView setDiscountTotal:discountTotal];
    [orderTotalFooterView setTotalCost:finalTotal];
}
@end
