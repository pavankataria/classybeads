//
//  PkNumberPadVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 11/02/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
//Step 1: linking to viewcontroller
@class PkNumberPadVC;

//Step 2: create protocol
@protocol PkNumberPadDelegate <NSObject>
@required
-(void)numberPadView:(PkNumberPadVC *)pkNumberPadVC digitPressed:(NSNumber*)digitPressed;
-(void)numberPadViewBackspaceButtonDidPress:(PkNumberPadVC *)numberPadView;
-(void)numberPadViewReturnButtonDidPress:(PkNumberPadVC *)numberPadView;
-(void)numberPadViewPointButtonDidPress:(PkNumberPadVC *)numberPadView;
@end



@interface PkNumberPadVC : UIViewController{
    IBOutletCollection(UIButton)NSMutableArray *numberPadButtons;
    //Step 3: create weak reference to delegate
    __weak id <PkNumberPadDelegate> delegate;
}

@property (weak) id<PkNumberPadDelegate> delegate;
-(id)init;
@end
