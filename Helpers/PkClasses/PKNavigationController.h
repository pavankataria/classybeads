//
//  PKNavigationController.h
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 28/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKNavigationController : UINavigationController


-(void)setTitle:(NSString *)title;
-(void)setNavigationBarTintColor:(UIColor*)color;
-(void)showLeftSideMenuButton;
@end
