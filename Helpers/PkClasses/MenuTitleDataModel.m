//
//  MenuTitleDataModel.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 25/01/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "MenuTitleDataModel.h"

@implementation MenuTitleDataModel
@synthesize mTName = _mTName, mTClassName = _mTClassName;


-(id)initWithMenuTitleName:(NSString*)_menuTitle andClassName:(NSString*)_className{
    self = [super init];
    if(!self) return nil;
    
    _mTName = _menuTitle;
    _mTClassName = _className;
    return self;
}
@end
