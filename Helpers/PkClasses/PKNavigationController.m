
//
//  PKNavigationController.m
//  RajamFMCGApp
//
//  Created by Pavan Kataria on 28/01/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import "PKNavigationController.h"
#import "AppConfig.h"
#import "AppDelegate.h"
@interface PKNavigationController ()

@end

@implementation PKNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setupNavigationBar{
    [self.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                 COLOR_WITH_RGB(255, 255, 255, 255), NSForegroundColorAttributeName,
                                                 [UIFont fontWithName:@"CartoGothicStd-Book" size:34.0], NSFontAttributeName, nil]];
    [self.navigationBar setTranslucent:NO];
    self.navigationBar.barTintColor = APP_COLOR_MAIN;//COLOR_WITH_RGB(50, 156, 210, 255);
    [[UISearchBar appearance] setBackgroundImage:[PreferencesConfig7 imageWithColor:APP_COLOR_MAIN]];
    self.view.tintColor = APP_COLOR_MAIN;//[UIColor colorWithRed:140/255.0f green:70/255.0f blue:35/255.0f alpha:1.0f];

    [[UIBarButtonItem appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           COLOR_WITH_RGB(255, 255, 255, 255), NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"CartoGothicStd-Book" size:24], NSFontAttributeName, nil]
                                                forState:UIControlStateNormal];

                self.navigationBar.tintColor = [UIColor whiteColor];
}
-(void)setNavigationBarTintColor:(UIColor*)color{
    self.navigationBar.tintColor = color;
};

-(void)showLeftSideMenuButton{
    UIBarButtonItem *showButton = [[UIBarButtonItem alloc] initWithTitle:@"MENU" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    self.navigationItem.leftBarButtonItem = showButton;
}
-(void)show:(id)sender{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];

}
-(void)setTitle:(NSString *)title{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        self.navigationItem.titleView = titleView;
    }
    titleView.text = title;
    [titleView sizeToFit];
}   

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
