//
//  DisplayPadVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 12/02/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "DisplayPadVC.h"

#import <QuartzCore/QuartzCore.h>

#define boris_random(smallNumber, bigNumber) ((((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * (bigNumber - smallNumber)) + smallNumber)
@interface DisplayPadVC (){
    UILabel *selectionBorder;
    
    NSMutableArray *productCodes;
    UITableView *autocompleteTableView;
    NSMutableArray *autocompleteProductCodes;
//    Product * product;
}

@end

@implementation DisplayPadVC

@synthesize pickerItems = _pickerItems;

@synthesize selectedLabel = _selectedLabel;


@synthesize quantityLabel;
@synthesize productLabel;
@synthesize priceLabel;

@synthesize dPQuantityValue;
@synthesize dPProductValue;
@synthesize dPPriceValue;
@synthesize shouldSaveCurrentTypedProductToDatabase;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    autocompleteTableView = [[UITableView alloc] initWithFrame:
                             CGRectMake(0, productLabel.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-productLabel.frame.origin.y) style:UITableViewStylePlain];
    [autocompleteTableView setBackgroundColor:[UIColor redColor]];
    autocompleteProductCodes = [[NSMutableArray alloc] init];
    productCodes = [[NSMutableArray alloc] init];
    
//    autocompleteTableView.delegate = self;
//    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [self.view addSubview:autocompleteTableView];
    
    
    // Do any additional setup after loading the view from its nib.
    for(int i = 0; i < 3; i++){
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        switch (i) {
            case 0:
                [quantityLabel addGestureRecognizer:singleTap];
                quantityLabel.tag = 1;
                quantityLabel.userInteractionEnabled = YES;
                break;
            case 1:
                [productLabel addGestureRecognizer:singleTap];
                productLabel.tag = 2;
                productLabel.userInteractionEnabled = YES;
                break;
            case 2:
                [priceLabel addGestureRecognizer:singleTap];
                priceLabel.tag = 3;
                priceLabel.userInteractionEnabled = YES;
                break;
            default:
                break;
        }
    }
    selectionBorder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, quantityLabel.frame.size.height)];
    [[selectionBorder layer] setBorderWidth:6];
    [[selectionBorder layer] setBorderColor:APP_COLOR_MAIN.CGColor];
    [[selectionBorder layer] setCornerRadius:5];
    [selectionBorder setBackgroundColor:[UIColor clearColor]];
    _selectedLabel = quantityLabel;
    [self.view addSubview:selectionBorder];
    
    
    [productLabel addObserver:self
                   forKeyPath:@"text"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:NULL];
    
    
    
    //1
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    //2
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    UIPickerView *discountsPicker = [[UIPickerView alloc] init];
    [discountsPicker setDelegate:self];
    [discountsPicker reloadAllComponents];
    discountsPicker.autoresizingMask = UIViewAutoresizingFlexibleHeight; // This is the code that will make sure the view does not get resized to the keyboards frame.
    
    

    [guineaPigTextField setInputView:discountsPicker];
    self.pickerItems = [[NSMutableDictionary alloc] init];
    
    /*
    NSMutableArray * numArray = [[NSMutableArray alloc] init];
    for(int i = 2; i < 11; i++){
        [numArray addObject:[NSNumber numberWithInt:i]];
    }
  
    [self.pickerItems setObject:[[NSMutableArray alloc] initWithArray:numArray] forKey:@"initial"];
    
    
    NSMutableArray * numArrayTwo = [[NSMutableArray alloc] init];
    
    for(int i = 1; i < 10; i++){
        [numArrayTwo addObject:[NSNumber numberWithInt:i]];
    }
    
    [numArrayTwo insertObject:@" " atIndex:0];
    [self.pickerItems setObject:[[NSMutableArray alloc] initWithArray:numArrayTwo] forKey:@"final"];
*/
    
    
    NSMutableArray *percentages = [[NSMutableArray alloc] initWithArray:@[@"5%", @"10%", @"15%", @"20%"]];
    NSMutableArray *initial = [[NSMutableArray alloc] init];
    NSMutableArray *final = [[NSMutableArray alloc] init];
    for(int i = 1; i < 11; i++){
        if(i != 1){
            [initial addObject:[[NSNumber numberWithInteger:i] stringValue]];
        }
        if(i != 10){
            [final addObject:[[NSNumber numberWithInteger:i] stringValue]];
        }
    }
    [percentages insertObject:@" " atIndex:0];
    [initial insertObject:@" " atIndex:0];
    [final insertObject:@" " atIndex:0];
    
    [self.pickerItems setObject:percentages forKey:@"percentages"];
    [self.pickerItems setObject:initial forKey:@"initial"];
    [self.pickerItems setObject:final forKey:@"final"];
}

//================================================
#pragma mark - UIPickerView methods
//================================================



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
//    return 3;
    return 4;
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
//
//    NSArray *array;
//    if(component == 0) array = [self.pickerItems objectForKey:@"initial"];
//    else if(component == 2) array = [self.pickerItems objectForKey:@"final"];
//    
//    UILabel *thisLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
//
//    thisLabel.text = [[array objectAtIndex:row] stringValue];
//    
//    return thisLabel;
//}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSLog(@"row: %d, component; %d", row, component);

    
    /*if(component == 0){
        if([[[self.pickerItems objectForKey:@"initial"] objectAtIndex:row] isKindOfClass:[NSString class]]){
            return [[self.pickerItems objectForKey:@"initial"] objectAtIndex:row];
        }
        else{
            return [[[self.pickerItems objectForKey:@"initial"] objectAtIndex:row] stringValue];
        }
    }
    else if(component == 1){
        if(row == 0){
            return @" ";
        }
        else{
            return @"For the price of";
        }
    }
    else if(component == 2){
        if([[[self.pickerItems objectForKey:@"final"] objectAtIndex:row] isKindOfClass:[NSString class]]){
            return [[self.pickerItems objectForKey:@"final"] objectAtIndex:row];
        }
        else{
            return [[[self.pickerItems objectForKey:@"final"] objectAtIndex:row] stringValue];
        }
    }
    return nil;
     */
    switch (component) {
        case 0:
                return [[self.pickerItems objectForKey:@"percentages"] objectAtIndex:row];
            break;
            
        case 1:
                return [[self.pickerItems objectForKey:@"initial"] objectAtIndex:row];
            break;
        case 2:
            if(row == 0){
                return @" ";
            }
            else{
                return @"For the price of";
            }
            break;
            
        case 3:
                return [[self.pickerItems objectForKey:@"final"] objectAtIndex:row];
            break;
        default:
            break;
    }
    return nil;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(component == 0){
        return [[self.pickerItems objectForKey:@"percentages"] count];
    }
    else if(component == 1){
        return [[self.pickerItems objectForKey:@"initial"] count];
    }
    else if (component == 2){
        return 2;
    }
    else if(component == 3){
        return [[self.pickerItems objectForKey:@"final"] count];
    }
    return 0;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 80;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 1) return 70;
    else if(component == 2) return 300;
    else if(component == 3) return 70;
    return 70;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"Did select row: %d at component: %d", row, component);
    int indexPercent = [pickerView selectedRowInComponent:0];//[[[self.pickerItems objectForKey:@"initial"] objectAtIndex:row] intValue];
    int indexLabel = [pickerView selectedRowInComponent:1];//[[[self.pickerItems objectForKey:@"initial"] objectAtIndex:row] intValue];
    
    int indexInitial = [pickerView selectedRowInComponent:2];//[[[self.pickerItems objectForKey:@"final"] objectAtIndex:row] intValue];

    int indexFinal = [pickerView selectedRowInComponent:3];//[[[self.pickerItems objectForKey:@"final"] objectAtIndex:row] intValue];

//    switch (component) {
//        case 0:
//            if(indexPercent > 4){
//
//            break;
//        case 1:
//            <#statements#>
//            break;
//        case 2:
//            <#statements#>
//            break;
//        case 3:
//            <#statements#>
//            break;
//            
//        default:
//            break;
//    }
    
    /*
    switch (component) {
        case 0:
            if(indexLeftRow < 4){
                if(indexMiddleRow > 0){
                    [pickerView selectRow:0 inComponent:1 animated:YES];
                }
                if(indexRightRow > 0){
                    [pickerView selectRow:0 inComponent:2 animated:YES];
                }
            }
            else{
                if(indexMiddleRow == 0){
                    [pickerView selectRow:1 inComponent:1 animated:YES];
                }
                if(indexRightRow == 0){
                    [pickerView selectRow:1 inComponent:2 animated:YES];
                }

            }
            break;
            
        default:
            break;
        case 1:
            if(indexMiddleRow == 0){
                if(indexLeftRow >= 4){
                    [pickerView selectRow:3 inComponent:0 animated:YES];
                }
                if(indexRightRow >= 1){
                    [pickerView selectRow:0 inComponent:2 animated:YES];
                }
            }
            else{
                if(indexLeftRow < 4){
                    [pickerView selectRow:4 inComponent:0 animated:YES];
                }
                if(indexRightRow == 0){
                    [pickerView selectRow:1 inComponent:2 animated:YES];
                }
            }
            break;
        case 2:
            if(indexRightRow ==  0){
                
                if(indexLeftRow >= 4){
                    [pickerView selectRow:3 inComponent:0 animated:YES];
                }
                if(indexMiddleRow >= 1){
                    [pickerView selectRow:0 inComponent:1 animated:YES];
                }
            }
            else{
                if(indexLeftRow < 4){
                    [pickerView selectRow:4 inComponent:0 animated:YES];
                }
                if(indexMiddleRow == 0){
                    [pickerView selectRow:1 inComponent:1 animated:YES];
                }
                
            }
    }
     */
    
    
//    int valueForRowOne = [[[self.pickerItems objectForKey:@"initial"] objectAtIndex:indexLeftRow] intValue];
//    int valueForRowTwo = [[[self.pickerItems objectForKey:@"final"] objectAtIndex:indexRightRow] intValue];
//    
//    NSLog(@"(%d) for (%d)", valueForRowOne, valueForRowTwo);
//    
//    if( valueForRowOne <= valueForRowTwo){
//        indexRightRow = indexLeftRow;
//        
//        [pickerView selectRow:indexLeftRow inComponent:0 animated:YES];
//        [pickerView selectRow:indexRightRow inComponent:2 animated:YES];
//    }
   
}
-(void)segmentedControlChangedValue:(id)sender{
    
}
-(void)handleSingleTap:(UIGestureRecognizer*)gestureRecognizer{
    UILabel *tappedLabel = (UILabel*)gestureRecognizer.view;
    if(tappedLabel == quantityLabel){
        NSLog(@"quantityLabel");
    }
    else if(tappedLabel == productLabel){
        NSLog(@"productLabel");
    }
    else if(tappedLabel == priceLabel){
        NSLog(@"priceLabel");
    }
    [self moveSelectionBorderToFrame:tappedLabel.frame];
    _selectedLabel = tappedLabel;
}

-(void)moveSelectionBorderToFrame:(CGRect)labelFrame{
    CGRect frame = selectionBorder.frame;
    frame.origin.y = labelFrame.origin.y;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         selectionBorder.frame = frame;
                     }
                     completion:nil];
}

-(void)moveSelectionToNextFrame{
    if(_selectedLabel == quantityLabel){
        NSLog(@"currently product label selected");

        _selectedLabel = productLabel;
    }
    else if(_selectedLabel == productLabel){
        _selectedLabel = priceLabel;

    }
    else if(_selectedLabel == priceLabel){
        _selectedLabel = quantityLabel;
    }
    [self moveSelectionBorderToFrame:_selectedLabel.frame];
    
    
}
- (IBAction)setDiscount:(id)sender {
    [guineaPigTextField becomeFirstResponder];
}

-(void)moveSelectionBorderToBeginning{
    CGRect frame = selectionBorder.frame;
    _selectedLabel = quantityLabel;

    frame.origin.y = _selectedLabel.frame.origin.y;

    [UIView animateWithDuration:0.2
                     animations:^{
                         selectionBorder.frame = frame;
                     }
                     completion:nil];

}


-(void)setDisplayPadSelectedLabelValue:(NSString*)stringValue{
//    [[self getSelectedLabel] setText:stringValue];
    NSMutableString *string = [[_selectedLabel text] mutableCopy];
//    NSLog(@"selectedLabel's text = %@", string);
    if ([string hasPrefix:@"0."]) {
//        string = [[string substringFromIndex:1] mutableCopy];
    }
    else if ([string hasPrefix:@"0"]) {
        string = [[string substringFromIndex:1] mutableCopy];
    }
    
    //This is for the cool 1 replace feature.. if the quantity has a 1 prefix and a digit is pressed, omit 1 and then append digit
    //otherwise append to digit 1
//    if(_selectedLabel == quantityLabel){
//        if([string hasPrefix:@"1"] && [string length] == 1){
//            //If the digit 1 is not pressed then delete 1 and start with that number instead.
//            if( ! [stringValue isEqualToString:@"1"]){
//                string = [[string substringFromIndex:1] mutableCopy];
//            }
//        }
//    }
    [string appendString:stringValue];
//    NSLog(@"string new value text = %@", string);

    for(int i = 0; i < 3; i++){
        switch (i) {
            case 0:
                if(_selectedLabel == quantityLabel){
                    [self setDPQuantityValue:string];
                }
                break;
            case 1:
                if(_selectedLabel == productLabel){
                    NSLog(@"code Value %@", string);
                    [self setDPProductValue:string];
                }
                break;
            case 2:
                if(_selectedLabel == priceLabel){
                    [self setDPPriceValue:string];
                }
                break;
            default:
                break;
        }
    }
    
}
-(OrderTakingLabelType)getSelectedLabelName{
    for(int i = 0; i < 3; i++){
        switch (i) {
            case 0:
                if(_selectedLabel == quantityLabel){
                    return OrderTakingLabelType_Quantity;
                }
                break;
            case 1:
                if(_selectedLabel == productLabel){
                    return OrderTakingLabelType_ProductCode;
                }
                break;
            case 2:
                if(_selectedLabel == priceLabel){
                    return OrderTakingLabelType_Price;
                }
                break;
            default:
                break;
        }
    }
    @throw [[NSException alloc] initWithName:@"PKException" reason:@"No label Name is selected/reached" userInfo:nil];

}
-(UILabel*)getSelectedLabel{
    for(int i = 0; i < 3; i++){
        switch (i) {
            case 0:
                if(_selectedLabel == quantityLabel){
                    return quantityLabel;
                }
                break;
            case 1:
                if(_selectedLabel == productLabel){
                    return productLabel;
                }
                break;
            case 2:
                if(_selectedLabel == priceLabel){
                    return priceLabel;
                }
                break;
            default:
                break;
        }
    }
    @throw [[NSException alloc] initWithName:@"PKException" reason:@"No label is selected" userInfo:nil];
}
-(void)setDPQuantityValue:(NSString *)quantityValue{
//    NSLog(@"custom quantity setter method called");
    if([quantityValue length] > 2){
        return;
    }
    [quantityLabel setText:quantityValue];
//    NSLog(@"quantity text: %@", [quantityLabel text]);
}
-(void)setDPProductValue:(NSString *)productValue{
//    NSLog(@"custom product setter method called");
    if([productValue length] > 4){
        return;
    }
    [productLabel setText:productValue];
//    NSLog(@"product text: %@", [productLabel text]);
}
-(void)setDPPriceValue:(NSString *)priceValue{
//    NSLog(@"custom price setter method called");
    
    if([priceValue rangeOfString:@"."].location != NSNotFound){
        NSString *component = [[priceValue componentsSeparatedByString: @"."] lastObject];
//        NSLog(@"lastcomponent: %@", component);
        int length = [[[priceValue componentsSeparatedByString: @"."] lastObject] length];
        if(length > 2){
            return;
        }
    }
    [priceLabel setText:priceValue];
//    NSLog(@"price text: %@", [priceLabel text]);
}
-(void)setDPDiscountSegment:(int)segment{
    [_dPDiscountSegmentControl setSelectedSegmentIndex:segment];
}
-(void)clearDisplays{
    [self setDPQuantityValue:@"0"];
    [self setDPProductValue:@"0"];
    [self setDPPriceValue:@"0"];
    [self setDPDiscountSegment:0];
    
    
//    product = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Autocomplete
/*
 - (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    if(textField == productLabel){
        autocompleteTableView.hidden = NO;
        
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        [self searchAutocompleteEntriesWithSubstring:substring];
        return YES;
    }
}
 */

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if ([keyPath isEqualToString:@"text"]) {
        /* etc. */

//        NSLog(@"label keypath: %@ object: %@", keyPath, object);
        NSString * pCode = [((UILabel*)object) text];
        
        if([pCode length] == 4){
            Product *product = [PKCoreDataSingleton retrieveProductWithCode:pCode];//[self retrieveDetailsForProduct:pCode];
            if(product){
//            NSLog(@"product found price: %0.2f for product: %@", [[product productPrice] floatValue], [product productCode]);
                NSString *price = [NSString stringWithFormat:@"%0.2f", [[product productPrice] floatValue]];
                
                [self setDPPriceValue:price];
                [[self priceLabel] setText:[NSString stringWithFormat:@"%@", price]];
            }
            else{
                NSLog(@"Product not found");
            }
        }
        else{
            [self setDPPriceValue:@"0"];
        }
    }
}

-(Product*)retrieveDetailsForProduct:(NSString*)pCode{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productCode == %@",  pCode];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setEntity:entityToBeFetched];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    
    if ([self.managedObjectContext countForFetchRequest:fetchRequest error:&error]){
        return [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] firstObject];
    }
    else{
        return nil;
    }
    
    
    /**
     Doing it this way returns the following error:
     2014-08-16 18:51:49.540 ClassyBeads2[4273:60b] -[Product subentitiesByName]: unrecognized selector sent to instance 0xb988ee0
     
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *dbExistingProducts = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"count of dbExistingProducts: %d ", (int)[dbExistingProducts count]);
    return ([dbExistingProducts count] == 0 ? nil : [dbExistingProducts firstObject]);
     */
}
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [autocompleteProductCodes removeAllObjects];
    
    for(NSString *curString in productCodes){
        NSRange substringRange = [curString rangeOfString:substring];
        
        if (substringRange.location == 0) {
            [autocompleteProductCodes addObject:curString];
        }

    }
    [autocompleteTableView reloadData];
}



-(void)canProcessProduct{
    NSLog(@"CAN PROCESS PRODUCT");
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productCode == %@", [productLabel text]];
    [fetchRequest setFetchLimit:1];
    [fetchRequest setEntity:entityToBeFetched];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    Product *product;
    if ([self.managedObjectContext countForFetchRequest:fetchRequest error:&error]){
        product = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] firstObject];
        
        NSLog(@"product price from database: %@", [[product productPrice] stringValue]);
        
        NSString *dbProductPrice = [[product productPrice] stringValue];
        if( [dbProductPrice isEqualToString:[priceLabel text]]){
            NSLog(@"No changes made to product price so no point saving the product");
            return;
        }
        else{
            NSLog(@"Save: There is a change in price. From dbPrice: %@ to newPrice: %@", dbProductPrice, [priceLabel text]);
        }
    }
    else{
        product = [NSEntityDescription
                   insertNewObjectForEntityForName:@"Product"
                   inManagedObjectContext:self.managedObjectContext];
        
        [product setProductCode:[productLabel text]];
        
    }
    [product setProductNeedsUpdating:[NSNumber numberWithBool:YES]];

    product.productPrice = [NSDecimalNumber decimalNumberWithString:[priceLabel text]];
    
    
//    NSLog(@"product description: %@", [product description]);
//[NSNumber numberWithFloat:[[priceLabel text] floatValue]];
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Fail to save with error: \n%@\n%@", [error localizedDescription], [error userInfo] );
    }
    else{
        NSLog(@"save successful product: %@ and updatedPrice: %@", [product productCode], [product productPrice]);
    }
}

-(void)createInstances{
    
    for(int i = 1; i <= 6; i++){
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        //Setting Entity to be Queried
        NSEntityDescription * entityToBeFetched = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
        

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productCode == %@", [NSString stringWithFormat:@"%d",(i*1111)]];
        [fetchRequest setFetchLimit:1];
        [fetchRequest setEntity:entityToBeFetched];
        [fetchRequest setPredicate:predicate];
        
        
        NSError* error;
        if (![self.managedObjectContext countForFetchRequest:fetchRequest error:&error]){
            Product *product = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Product"
                                inManagedObjectContext:self.managedObjectContext];
            
            [product setProductCode:[NSString stringWithFormat:@"%d",(i*1111)]];
            [product setProductPrice:[NSNumber numberWithFloat:boris_random(0.5, 8.35)]];
            
            [product setProductNeedsUpdating:[NSNumber numberWithBool:YES]];

            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Fail to save with error: \n%@\n%@", [error localizedDescription], [error userInfo] );
            }
            else{
                NSLog(@"save successful product: %@ and updatedPrice: %@", [product productCode], [product productPrice]);
            }
        }
    }
    //    [product setProductNeedsUpdating:[NSNumber numberWithBool:YES]];
    
//    product.productPrice = [NSDecimalNumber decimalNumberWithString:[priceLabel text]];
    
    //[NSNumber numberWithFloat:[[priceLabel text] floatValue]];
 

}


- (ProductDiscountOption)getSelectedDiscountOption{
    switch (self.dPDiscountSegmentControl.selectedSegmentIndex) {
        case 0:
            NSLog(@"No discount selected");
            return ProductDiscountNone;
            break;
        case 1:
            NSLog(@"5%%");
            return ProductDiscount5Percent;
            break;
        case 2:
            NSLog(@"10%%");
            return ProductDiscount10Percent;
            break;
        case 3:
            NSLog(@"15%%");
            return ProductDiscount15Percent;
            break;
        case 4:
            NSLog(@"20%%");
            return ProductDiscount20Percent;
            break;
        case 5:
            NSLog(@"B1:1F");
            return ProductDiscountBuy1Get1FreePercent;
            break;
        case 6:
            NSLog(@"6 for 4");
            return ProductDiscount6For4;
            break;
        default:
            NSLog(@"None selected so default: NO DISCOUNT");
            return ProductDiscountNone;
            break;
    }
}



-(void)setValuesWithOrderLine:(OrderLineDataModel*)oldm{
    [self setDPQuantityValue:[oldm.orderLineQuantity stringValue]];
    [self setDPProductValue:oldm.orderLineCode];
    [self setDPPriceValue:[oldm.orderLinePrice stringValue]];
    [self setDPDiscountSegment:oldm.orderLineDiscountTypeEnumStruct];
}

-(void)grabDisplayPadValuesAndSetOrderLineObject:(OrderLineDataModel*)oldm{
    oldm.orderLineQuantity = [NSNumber numberWithInteger:[[quantityLabel text] intValue]];
    oldm.orderLineCode = [productLabel text];
    oldm.orderLinePrice = [NSNumber numberWithFloat:[[NSString stringWithFormat:@"%.2f",[[priceLabel text] floatValue]] floatValue]];
    oldm.orderLineOriginalTotal = [NSNumber numberWithFloat:((float)[oldm.orderLineQuantity integerValue] * [oldm.orderLinePrice floatValue])] ;
    
    [oldm setDiscountwithDiscountType:[self getSelectedDiscountOption]];
    
}


@end
