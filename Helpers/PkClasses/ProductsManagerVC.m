//
//  ProductsManagerVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 23/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ProductsManagerVC.h"
//#import "ProductManagementCollectionView.h"

@interface ProductsManagerVC (){
    NSMutableArray *productsArray;
    NSMutableArray *preProductsArray;

    UIBarButtonItem *selectButton;

   UIBarButtonItem *showButton;
    UIBarButtonItem *deleteButton;
    UIActionSheet *deleteActionSheet;

}

@end

@implementation ProductsManagerVC
@synthesize productEntryFieldVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Jee Huaa Whataaaah Jeezzaaa
    
    [self commonSetup];
}
-(void)commonSetup{
    [self setupNativationItems];

    [self setupTable];
    [self setupVariables];
}

-(void)setupVariables{
//    [_productEntryCollectionView registerClass:[ProductEntryCVCell class]
//            forCellWithReuseIdentifier:ProductCellIdentifier];

    UINib *cellNib = [UINib nibWithNibName:@"ProductViewingItemCell" bundle:nil];
    [_productEntryCollectionView registerNib:cellNib forCellWithReuseIdentifier:ProductCellIdentifier];
    
//    [_productEntryCollectionView registerNib:[UINib nibWithNibName:@"ProductEntryHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ProductEntryCellIdentifier];
    
//    _productEntryCollectionView.collectionViewLayout = [[ProductManagementCollectionView alloc] init];
    
    productEntryFieldVC = [[ProductEntryFieldVC alloc] init];
    productEntryFieldVC.delegate = self;
    NSLog(@"Product Entry View frame: %@", NSStringFromCGRect(productEntryFieldVC.view.frame));
    [self.view addSubview:productEntryFieldVC.view];
    
    CGRect tableFrame = _productEntryCollectionView.frame;
    
    CGRect screenRect = self.view.frame;
    
    tableFrame.origin.y = productEntryFieldVC.view.frame.size.height;
    tableFrame.size.height = screenRect.size.height - productEntryFieldVC.view.frame.size.height;
    _productEntryCollectionView.frame = tableFrame;
    
    
    CGRect peCell = productEntryFieldVC.view.frame;
    peCell.size.width = screenRect.size.width;
    productEntryFieldVC.view.frame = peCell;
    
//    [productEntryVC.view setBackgroundColor:[UIColor redColor]];
    
    
    NSLog(@"final collection View %@", NSStringFromCGRect(_productEntryCollectionView.frame));
    
    
    /*
    // attach long press gesture to collectionView
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = .5; //seconds
    lpgr.delaysTouchesBegan = YES;
    lpgr.delegate = self;
    [_productEntryCollectionView addGestureRecognizer:lpgr];
    */
    
}
-(void)setupTable{
//    [_productEntryTable setDelegate:self];
//    [_productEntryTable setDataSource:self];
    [self fetchLatestProductsAndPopulateTable];
    
    
    
}



-(void)selectItems{
    int totalCountOfSelectedItems = [[_productEntryCollectionView indexPathsForSelectedItems] count];
    NSLog(@"total selected Items: %d", totalCountOfSelectedItems);
    
    NSArray *itemsNeedDeselecting = [[NSArray alloc] initWithArray:[_productEntryCollectionView indexPathsForSelectedItems]];
    for(int i = 0; i < totalCountOfSelectedItems; i++){
        NSIndexPath *currentIndexPath = [itemsNeedDeselecting objectAtIndex:i];
        NSLog(@"deselecting row: %d", currentIndexPath.row);
        [_productEntryCollectionView deselectItemAtIndexPath:currentIndexPath animated:NO];
    }
    
    if([[selectButton title] isEqualToString:@"Select"]){
        [selectButton setTitle:@"Cancel"];
        _deleteEditingMode = TRUE;
        
        deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(didSelectTrashButtonAfterSelectedItemsToDelete)];
        
        [deleteButton setEnabled:FALSE];
        [self.navigationItem setLeftBarButtonItem:deleteButton animated:YES];

        
        [_productEntryCollectionView setAllowsMultipleSelection:YES];
    }
    else{
        

        [self resetSelectButtonBackToNormal];
    }
}

-(void)resetSelectButtonBackToNormal{
    
    [self setupNativationItems];
    _deleteEditingMode = FALSE;
    [_productEntryCollectionView setAllowsMultipleSelection:NO];
}

-(void)didSelectTrashButtonAfterSelectedItemsToDelete{
    //Items To Delete
    
    
    if(deleteActionSheet == nil){
        NSArray *iTD = [[NSArray alloc] initWithArray:[_productEntryCollectionView indexPathsForSelectedItems]];
        
        BOOL deletePlural = ([iTD count] <= 1 ? NO : YES);
        NSString *titleMessage = [NSString stringWithFormat:@"%@ will also be deleted from your database and server.", (!deletePlural ? @"This product" : @"These products")];
        
        NSString *destructiveMessage = [NSString stringWithFormat:@"Delete %@", (!deletePlural) ? @"Photo" : [NSString stringWithFormat:@"%d products", (int)[iTD count]]];
        
        
        deleteActionSheet = [[UIActionSheet alloc] initWithTitle:titleMessage
                                                        delegate:self
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:destructiveMessage
                                               otherButtonTitles:nil, nil];
        [deleteActionSheet addButtonWithTitle: @""];
        [deleteActionSheet setCancelButtonIndex: deleteActionSheet.numberOfButtons - 1];
        [deleteActionSheet showFromBarButtonItem:deleteButton animated:YES];
    }
    
}
-(void)finallyDeleteAllItemsSelected{
    NSArray *iTD = [[NSArray alloc] initWithArray:[_productEntryCollectionView indexPathsForSelectedItems]];

    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (NSIndexPath *itemPath  in iTD) {
        [indexSet addIndex:itemPath.row];
    }
    NSArray *productsToDeleteFromCoreData = [productsArray objectsAtIndexes:indexSet];
    
    [PKCoreDataSingleton removeObjectsFromStore:productsToDeleteFromCoreData];
    
    [productsArray removeObjectsAtIndexes:indexSet];
    
    
    [_productEntryCollectionView deleteItemsAtIndexPaths:iTD];
    [self resetSelectButtonBackToNormal];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
        
        CGPoint p = [gestureRecognizer locationInView:_productEntryCollectionView];
        
        NSIndexPath *indexPath = [_productEntryCollectionView indexPathForItemAtPoint:p];
        if (indexPath == nil){
            NSLog(@"couldn't find index path");
        } else {
            // get the cell at indexPath (the one you long pressed)
//            ProductViewingItemCell * cell = (ProductViewingItemCell*)[_productEntryCollectionView cellForItemAtIndexPath:indexPath];
            NSLog(@"long press");
            
            [self didSelectCellItemForDeletionAtIndexPath:indexPath];
            
        }
    }
}
-(void)didSelectCellItemForDeletionAtIndexPath:(NSIndexPath*)indexPath{
//    [cell.contentView setBackgroundColor:[UIColor redColor]];
    [[productsArray objectAtIndex:indexPath.row] setDeleteSelected:YES];
    
}
*/

-(void)didRetrieveProductAfterProductSearch:(Product *)product{
    if(product){
        for(int i = 0; i < [productsArray count]; i++){
            if([[[productsArray objectAtIndex:i] productCode] isEqualToString:product.productCode]){
//                NSLog(@"Product found at index: %d", i);
                [_productEntryCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
            }
        }
    }
    else{
        
    }
    
    
}

//-(void)setNavigationButtons{
//
//    
////    UIBarButtonItem *addCustomerButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addCustomer:)];
////    [self.navigationItem setRightBarButtonItem:addCustomerButton];
//}

-(void)setupNativationItems{
    showButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(show:)];
    [self.navigationItem setLeftBarButtonItem:showButton animated:YES];
    self.title = @"Product Management";
    
    
    selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStylePlain target:self action:@selector(selectItems)];
    [self.navigationItem setRightBarButtonItems:@[selectButton] animated:NO];
}






-(void)show:(id)sender{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
    
}




/*
#pragma mark - UITableView DataSource methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT_OF_PRODUCT_ENTRY_CELL;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [productsArray count] + 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath == 0){
        static NSString *cellID = @"productEntryCell";
        ProductEntryCell *cell = (ProductEntryCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductEntryCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
        }
        
        // Configure Cell
        //    [cell.product setText:[NSString stringWithFormat:@"Row %i in Section %i", [indexPath row], [indexPath section]]];
        
        return cell;
    }
    else{
        static NSString *cellID = @"productCell";
        ProductEntryCell *cell = (ProductEntryCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductEntryCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
        }
        
        // Configure Cell
        //    [cell.product setText:[NSString stringWithFormat:@"Row %i in Section %i", [indexPath row], [indexPath section]]];
        
        return cell;

    }
}
 */











-(void)fetchLatestProductsAndPopulateTable{
    if(productsArray == nil){
        productsArray = [[NSMutableArray alloc] init];
    }
    [productsArray removeAllObjects];
    
//    if(preProductsArray == nil){
//        preProductsArray = [[NSMutableArray alloc] init];
//    }
//    [preProductsArray removeAllObjects];

    
    
    
//    preProductsArray = [[PKCoreDataSingleton retrieveAllProductsLatestFirst] mutableCopy];
//    
//    for(int i = 0; i < [preProductsArray count]; i++) {
//        NSLog(@"index: %d",i);
//        ProductInstanceCVTemplate *pICV = [[ProductInstanceCVTemplate alloc] initWithProduct:[preProductsArray objectAtIndex:i] withState:false];
//        [productsArray addObject:pICV];
//    }
    productsArray = [[PKCoreDataSingleton retrieveAllProductsLatestFirst] mutableCopy];
    
    [_productEntryCollectionView reloadData];
//    
//    for(Product *product in productsArray){
//        ProductInstanceCVTemplate *couple = [[ProductInstanceCVTemplate alloc] initWithProduct:product withState:FALSE];
//        [finalProductsArray addObject:couple];
//    }
}
//==========================================================
#pragma mark - ProductEntryDelegate methods -
//==========================================================

-(void)modifyingProduct{
    for (NSIndexPath *indexPath in [_productEntryCollectionView indexPathsForSelectedItems]) {
        [_productEntryCollectionView deselectItemAtIndexPath:indexPath animated:NO];
    }
    [productEntryFieldVC setCurrentlyEditingProduct:nil];
}

-(void)didAddNewProduct:(Product *)product{
    [self addNewItemWithProduct:product];

}
-(void)didModifyProduct:(Product *)product{
    NSLog(@"modified: %@\nso have to reload table view data", [product description]);
    
    int indexFoundAt = 0;
    for(int i = 0; i < [productsArray count]; i++){
        if([[[productsArray objectAtIndex:i] productCode] isEqualToString:product.productCode]){
//            NSLog(@"Product found at index: %d", i);
            indexFoundAt = i;
        }
    }
    [_productEntryCollectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:indexFoundAt inSection:0]]];
}


- (void)addNewItemWithProduct:(Product*)product{
    [productsArray insertObject:product atIndex:0];

    [_productEntryCollectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
}

//- (void)removeItemAtIndexPath:(NSIndexPath*)indexPath andDeleteProduct:(Product*)product{
//    //    Product *cn = [ColorName randomColorName];
//    NSLog(@"entering %s ", __PRETTY_FUNCTION__ );
//    [productsArray removeObjectAtIndex:indexPath.row];
//    [PKCoreDataSingleton removeObjectFromStore:product];
//    [_productEntryCollectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
//}

//==========================================================
#pragma mark - UICollectionViewDataSource -
//==========================================================


//==========================================================
#pragma mark - UICollectionViewDelegate -
//==========================================================

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//        NSLog(@"items selected: %@", [[[collectionView indexPathsForSelectedItems] firstObject] description]);
//    NSLog(@"selected item at index: %ld", (long)indexPath.row);

    if(_deleteEditingMode){
        ([[_productEntryCollectionView indexPathsForSelectedItems] count] > 0 ? [deleteButton setEnabled:YES] : [deleteButton setEnabled:NO]);
    }
    else{
        [productEntryFieldVC setCurrentlyEditingProduct:[productsArray objectAtIndex:indexPath.row]];
        [productEntryFieldVC setProductEntryFieldsWithProduct:[productsArray objectAtIndex:indexPath.row]];
    }
//    }
    
//    if([collectionView indexPathsForSelectedItems] != nil && [[[collectionView indexPathsForSelectedItems] firstObject] row] == indexPath.row) return;

//    [self collectionView:collectionView setHighlight:YES indexPath:indexPath];


}
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(_deleteEditingMode){
        ([[_productEntryCollectionView indexPathsForSelectedItems] count] > 0 ? [deleteButton setEnabled:YES] : [deleteButton setEnabled:NO]);
    }
}

/*
 - (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductEntryCVCell* cell = (ProductEntryCVCell*)[colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductEntryCVCell* cell = (ProductEntryCVCell*)[colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}
 */
//==========================================================
#pragma mark - UICollectionViewDataSource -
//==========================================================

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    return [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:ProductEntryCellIdentifier forIndexPath:indexPath];
//}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    NSLog(@"productsArray count: %d", [productsArray count]);
    return [productsArray count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductViewingItemCell *itemCell = [collectionView dequeueReusableCellWithReuseIdentifier:ProductCellIdentifier forIndexPath:indexPath];
    
//    itemCell.contentView.layer.cornerRadius = 20;
//    itemCell.contentView.layer.borderWidth = 1.0f;
//    itemCell.contentView.layer.borderColor = [UIColor blueColor].CGColor;
//    [itemCell.layer setBackgroundColor:[UIColor greenColor].CGColor]
//    ;
//    itemCell.contentView.clipsToBounds = YES;
    
    
//    itemCell.layer.shadowColor = [UIColor redColor].CGColor;
//    itemCell.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    itemCell.layer.shadowRadius = 2.0f;
//    itemCell.layer.shadowOpacity = 1.0f;
//    itemCell.layer.masksToBounds = NO;
//    itemCell.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:itemCell.bounds cornerRadius:itemCell.contentView.layer.cornerRadius].CGPath;

    
    //Current Product
    Product *cP = [productsArray objectAtIndex:indexPath.row];

    [itemCell.productCodeLabel setText:cP.productCode];
    [itemCell.productDescriptionLabel setText:cP.productDescription];
    [itemCell.productQuantityLabel setText:[NSString stringWithFormat:@"%@", [cP.productQuantity stringValue]]];
    [itemCell.productPriceLabel setText:[NSString stringWithFormat:@"£%.2f", [cP.productPrice floatValue]]];
    return itemCell;
}


//==========================================================
#pragma mark - UIActionSheetViewDelegate methods -
//==========================================================
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == actionSheet.destructiveButtonIndex)
    {
        
        [self finallyDeleteAllItemsSelected];
        deleteActionSheet = nil;
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    deleteActionSheet = nil;

}
@end
