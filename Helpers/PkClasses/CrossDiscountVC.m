//
//  CrossDiscountVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "CrossDiscountVC.h"

@interface CrossDiscountVC (){
    ProductDiscountOption discountOption;
    NSMutableArray *_selectedArray;
}

@end

@implementation CrossDiscountVC
@synthesize delegate;
@synthesize odmDiscountsValue;
@synthesize odmTotalValue;
@synthesize odmTotalQuantityValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    discountOption = ProductDiscountNone;
    _selectedArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setCurrentValue:(CGFloat)price{
    [productPriceLabel setText:[NSString stringWithFormat:@"%.2f", price]];
}



-(IBAction)none{
    if([delegate respondsToSelector:@selector(crossDiscountDidSelectDiscountOption:)]){
        [delegate crossDiscountDidSelectDiscountOption:ProductDiscountNone];
    }
    discountOption = ProductDiscountNone;
    [self recalculateValues];
}
-(IBAction)bOneGOneFree{
    if([delegate respondsToSelector:@selector(crossDiscountDidSelectDiscountOption:)]){
        [delegate crossDiscountDidSelectDiscountOption:ProductDiscountBuy1Get1FreePercent];
    }
    discountOption = ProductDiscountBuy1Get1FreePercent;
    [self recalculateValues];

}
-(IBAction)SixforFour{
    if([delegate respondsToSelector:@selector(crossDiscountDidSelectDiscountOption:)]){
        [delegate crossDiscountDidSelectDiscountOption:ProductDiscount6For4];
    }
        discountOption = ProductDiscount6For4;
        [self recalculateValues];
    
}
-(IBAction)applyCalculations{
    
    if([_selectedArray count] <= 1){
        [SVProgressHUD showErrorWithStatus:@"Select more than one product for cross discount" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    if(!(discountOption != ProductDiscountNone)){
        [SVProgressHUD showErrorWithStatus:@"Select a discount type" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }


    if([delegate respondsToSelector:@selector(crossDiscountApplyCalculationsWithDiscountType:)]){
        [delegate crossDiscountApplyCalculationsWithDiscountType:discountOption];
    }
}



-(void)setQuantity:(int)quantity setDiscounts:(CGFloat)discount setTotal:(CGFloat)total{
    [odmQuantity setText:[NSString stringWithFormat:@"%d", quantity]];
    [odmDiscounts setText:[NSString stringWithFormat:@"%.2f", discount]];
    [odmTotal setText:[NSString stringWithFormat:@"%.2f", total]];
    
    odmDiscountsValue = discount;
    odmTotalValue = total;
    odmTotalQuantityValue = quantity;
}

-(void)calculateValuesWithSelectedArray:(NSMutableArray*)selectedArray{
    
    _selectedArray = selectedArray;
    
    [self recalculateValues];
}
-(void)recalculateValues{
    [odmDescription setText:[AppConfig stringForProductDiscountOptionToString:discountOption]];

    int quantity = 0;
    CGFloat totalPrice = 0;
    
    for(OrderLineDataModel *odm in _selectedArray){
        quantity += [odm.orderLineQuantity intValue];
        totalPrice += [odm.orderLineOriginalTotal floatValue];
    }
    
    
    
    
//    NSLog(@"Total Price %.2f", totalPrice);
    CGFloat discountAmount = [[AppConfig getDiscountAmountWithDiscountType:discountOption withOrderLineQuantity:[NSNumber numberWithInt:quantity] withOrderLinePrice:[[_selectedArray firstObject] orderLinePrice] withOrderLineOriginalTotal:[NSNumber numberWithFloat:totalPrice]] floatValue];
    
    [self setQuantity:quantity setDiscounts:discountAmount setTotal:totalPrice-discountAmount];
    
    CGFloat unitDiscount = discountAmount/quantity;
    NSLog(@"amount: %.2f / quantity: %d = unit discount: %.2f", discountAmount, quantity, unitDiscount);
    
    CGFloat unitTotal = totalPrice/quantity;
    NSLog(@"amount: %.2f / quantity: %d = unit total: %.2f", totalPrice, quantity, unitTotal);
    
    
    for(OrderLineDataModel *odm in _selectedArray){
        [odm setOrderLineDiscount:[NSNumber numberWithFloat:(unitDiscount*[odm.orderLineQuantity intValue])]];
        [odm setOrderLineFinalTotalPrice:[NSNumber numberWithFloat:(unitTotal*[odm.orderLineQuantity intValue])]];
        
        
    }
}


@end
