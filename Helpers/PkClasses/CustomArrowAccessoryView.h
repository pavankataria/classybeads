//
//  CustomArrowAccessoryView.h
//  Rajam Oorvasi
//
//  Created by Pavan Kataria on 30/12/2013.
//  Copyright (c) 2013 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomArrowAccessoryView : UIControl
{
	UIColor *_accessoryColor;
	UIColor *_highlightedColor;
}

@property (nonatomic, retain) UIColor *accessoryColor;
@property (nonatomic, retain) UIColor *highlightedColor;

+ (CustomArrowAccessoryView *)accessoryWithColor:(UIColor *)color;

@end