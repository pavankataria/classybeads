//
//  CrossDiscountControlVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 05/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "CrossDiscountControlVC.h"
@interface CrossDiscountControlVC (){
//    int initialQuantity, finalQuantity;
}

@property (strong) NSMutableDictionary *pickerItems;
@end

@implementation CrossDiscountControlVC
@synthesize delegate;

@synthesize pickerItems;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.pickerItems = [[NSMutableDictionary alloc] init];
//    self.pickerItems = [@[@"a", @"b", @"c", @"d"] mutableCopy];
//
//    
//    
    NSMutableArray *initial = [[NSMutableArray alloc] init];
    NSMutableArray *final = [[NSMutableArray alloc] init];
    for(int i = 1; i < 11; i++){
        if(i != 1){
            [initial addObject:[[NSNumber numberWithInteger:i] stringValue]];
        }
        if(i != 10){
            [final addObject:[[NSNumber numberWithInteger:i] stringValue]];
        }
    }
    [initial insertObject:@" " atIndex:0];
    [final insertObject:@" " atIndex:0];
    
    [self.pickerItems setObject:initial forKey:@"initial"];
    [self.pickerItems setObject:final forKey:@"final"];
    [self updateFloorLabel];
    [pickerView reloadAllComponents];
//    [self enableAddSectionButton:NO];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)insertSection:(id)sender {

    
    int initialQuantity = [pickerView selectedRowInComponent:0];
    int finalQuantity = [pickerView selectedRowInComponent:1];
    
    if(initialQuantity == 0 || finalQuantity == 0){
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:@"Choose an appropriate discount offer" maskType:SVProgressHUDMaskTypeBlack];
        
    }
    else{
     
        int imes = [[[self.pickerItems objectForKey:@"initial"] objectAtIndex:initialQuantity] intValue];
        int fmes = [[[self.pickerItems objectForKey:@"final"] objectAtIndex:finalQuantity] intValue];
        
        NSString *title = [NSString stringWithFormat:@"\"%d for %d\" discount category", imes, fmes];
        [self.delegate crossDiscountDidClickInsertSectionButtonWithSectionHeaderTitle:title initial:initialQuantity andFinal:finalQuantity];
    }
}



-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSLog(@"a %d %d", row, component);
//    [self enableAddSectionButton:NO];
    switch (component) {

        case 0:
//            initialQuantity = [[[self.pickerItems objectForKey:@"initial"] objectAtIndex:row] intValue];

            return [[self.pickerItems objectForKey:@"initial"] objectAtIndex:row];
            break;

        case 1:
//            finalQuantity = [[[self.pickerItems objectForKey:@"final"] objectAtIndex:row] intValue];
            return [[self.pickerItems objectForKey:@"final"] objectAtIndex:row];
            break;
        default:
            break;
    }
    return nil;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (component) {
        case 0:
            return [[self.pickerItems objectForKey:@"initial"] count];
            break;
        case 1:
            return [[self.pickerItems objectForKey:@"final"] count];
            break;
        default:
            break;
    }
    return 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return [[self.pickerItems allKeys] count];
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return 200;
}
-(void)pickerView:(UIPickerView *)_pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"Did select row: %d at component: %d", row, component);
    NSLog(@"Did select row: %d at component: %d", row, component);
    int indexLeftRow = [pickerView selectedRowInComponent:0];//[[[self.pickerItems objectForKey:@"initial"] objectAtIndex:row] intValue];
    
    int indexRightRow = [pickerView selectedRowInComponent:1];//[[[self.pickerItems objectForKey:@"final"] objectAtIndex:row] intValue];
    
    
    int valueForRowOne = [[[self.pickerItems objectForKey:@"initial"] objectAtIndex:indexLeftRow] intValue];
    int valueForRowTwo = [[[self.pickerItems objectForKey:@"final"] objectAtIndex:indexRightRow] intValue];

    NSLog(@"(%d) for (%d)", valueForRowOne, valueForRowTwo);

    if( valueForRowOne <= valueForRowTwo){
        indexRightRow = indexLeftRow;

        [pickerView selectRow:indexLeftRow inComponent:0 animated:YES];
        [pickerView selectRow:indexRightRow inComponent:1 animated:YES];
        
//        initialQuantity = [[[self.pickerItems objectForKey:@"initial"] objectAtIndex:indexLeftRow] intValue];
//        finalQuantity = [[[self.pickerItems objectForKey:@"final"] objectAtIndex:indexRightRow] intValue];

//        [self enableAddSectionButton:YES];
    }
    
    [self changeAddDiscountButtonWithInitialQuantity:indexLeftRow andWithFinalQuantity:indexRightRow];

}
//-(void)enableAddSectionButton:(BOOL)enable{
//    if(enable){
//        [addSectionButon setBackgroundColor:APP_COLOR_MAIN];
//        [addSectionButon setEnabled:YES];
//    }
//    else{
//        [addSectionButon setBackgroundColor:[UIColor lightGrayColor]];
//        [addSectionButon setEnabled:NO];
//    }
//}
-(void)changeAddDiscountButtonWithInitialQuantity:(int)indexLeftRow andWithFinalQuantity:(int)indexRightRow{
    
    if((indexLeftRow == 0) || (indexRightRow == 0)){
//        [self enableAddSectionButton:NO];
    }
    else{

//        [self enableAddSectionButton:YES];

    }
}
- (void)updateFloorLabel {
//    NSInteger floor = [pickerView numberOfRowsInComponent:0] - [pickerView selectedRowInComponent:0];
//    NSString *suffix = @"th";
//    if (((floor % 100) / 10) != 1) {
//        switch (floor % 10) {
//            case 1:  suffix = @"st"; break;
//            case 2:  suffix = @"nd"; break;
//            case 3:  suffix = @"rd"; break;
//        }
//    }
    [pickerView addLabel:[NSString stringWithFormat:@"for the price of"]
                       ofSize:21
                  toComponent:0
                leftAlignedAt:130
baselineAlignedWithFontOfSize:25];
}

@end
