//
//  ProductEntryVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 23/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ProductEntryVC.h"

@interface ProductEntryVC ()

@end

@implementation ProductEntryVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
