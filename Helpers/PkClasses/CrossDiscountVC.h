//
//  CrossDiscountVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 03/09/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderLineDataModel.h"

@protocol CrossDiscountDelegate <NSObject>
@required
-(void)crossDiscountDidSelectDiscountOption:(ProductDiscountOption)discountOption;
-(void)crossDiscountApplyCalculationsWithDiscountType:(ProductDiscountOption)discountOption;

@end

@interface CrossDiscountVC : UIViewController{
    
    __weak id <CrossDiscountDelegate> delegate;
    IBOutlet UILabel *productPriceLabel;
    
    IBOutlet UILabel *odmQuantity;
    IBOutlet UILabel *odmDiscounts;
    IBOutlet UILabel *odmTotal;
    IBOutlet UILabel *odmDescription;

}
@property (weak) id <CrossDiscountDelegate> delegate;
@property (assign) CGFloat odmDiscountsValue;
@property (assign) CGFloat odmTotalValue;
@property (assign) int odmTotalQuantityValue;

-(void)setCurrentValue:(CGFloat)price;
-(void)setQuantity:(int)quantity setDiscounts:(CGFloat)discount setTotal:(CGFloat)amount;
-(void)calculateValuesWithSelectedArray:(NSMutableArray*)selectedArray;



@end
