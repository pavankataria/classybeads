//
//  DisplayPadVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 12/02/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
@interface DisplayPadVC : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate>{
    IBOutlet UITextField *guineaPigTextField;
}
@property (nonatomic, retain) UILabel *selectedLabel;


@property (nonatomic, retain) IBOutlet UILabel *quantityLabel;
@property (nonatomic, retain) IBOutlet UILabel *productLabel;
@property (nonatomic, retain) IBOutlet UILabel *priceLabel;



@property (nonatomic, retain) NSString *dPQuantityValue;
@property (nonatomic, retain) NSString *dPProductValue;
@property (nonatomic, retain) NSString *dPPriceValue;
@property (nonatomic, assign) BOOL shouldSaveCurrentTypedProductToDatabase;

@property (strong, nonatomic) IBOutlet UISegmentedControl * dPDiscountSegmentControl;
@property (strong, nonatomic) NSMutableDictionary *pickerItems;



-(void)setDisplayPadSelectedLabelValue:(NSString*)stringValue;
-(UILabel*)getSelectedLabel;
-(OrderTakingLabelType)getSelectedLabelName;
-(void)clearDisplays;


- (IBAction)setDiscount:(id)sender;


-(void)moveSelectionBorderToBeginning;
-(void)moveSelectionToNextFrame;
-(void)canProcessProduct;
-(void)createInstances;


-(void)setValuesWithOrderLine:(OrderLineDataModel*)oldm;
-(void)grabDisplayPadValuesAndSetOrderLineObject:(OrderLineDataModel*)oldm;

- (ProductDiscountOption)getSelectedDiscountOption;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
