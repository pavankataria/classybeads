//
//  ProductsManagerVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 23/08/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductViewingItemCell.h"



#import "ProductEntryFieldVC.h"
#import "ProductInstanceCVTemplate.h"

static NSString * const ProductCellIdentifier = @"ProductCellIdentifier";

@interface ProductsManagerVC : UIViewController<UICollectionViewDelegate, ProductEntryFieldDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate>
{
//    IBOutlet UITextField *rowThree;

}

@property (weak, nonatomic) IBOutlet UICollectionView *productEntryCollectionView;
@property (retain, nonatomic) ProductEntryFieldVC *productEntryFieldVC;
@property (nonatomic, assign) BOOL deleteEditingMode;
@end
