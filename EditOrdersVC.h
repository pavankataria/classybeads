//
//  EditOrdersVC.h
//  ClassyBeads2
//
//  Created by Pavan Kataria on 14/04/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrdersDataModel.h"
#import "OrderLineDataModel.h"
#import "OrderLineCell.h"
#import "CustomersEditVC.h"
#import "TakeOrderVC.h"
#import <MessageUI/MessageUI.h>
#import "AgentDisplayCell.h"

//#import "NDHTMLtoPDF.h"

@interface EditOrdersVC : UIViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, NDHTMLtoPDFDelegate>{
    NSMutableArray *orderDataArray;
    NSMutableArray *detailOrderDataArray;

}
@property (nonatomic, weak) IBOutlet UITableView *orderTable;
@property (nonatomic, weak) IBOutlet UITableView *detailOrderTable;

@property (retain, nonatomic) ASIHTTPRequest *serverRequest;

@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;


@end
