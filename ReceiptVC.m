//
//  ReceiptVC.m
//  ClassyBeads2
//
//  Created by Pavan Kataria on 09/03/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "ReceiptVC.h"
#import "CustomerDetailsReceiptVC.h"

@interface ReceiptVC (){
    BOOL EDIT_MODE;
    
    NSArray *finalArray;
    NSMutableArray *allDiscountsTotalArray;
    NSString *referenceNumberFromServer;
}

@end

@implementation ReceiptVC
@synthesize invoiceTable = _invoiceTable;
@synthesize invoiceDataArray = _invoiceDataArray;
@synthesize serverRequest;
@synthesize customerDetails = _customerDetails;
@synthesize editModeOrderObject = _editModeOrderObject;
typedef enum{
    kTagCreateOrder = 3434,
    //    kRetrieveProducts = 5434,
    //    kAddProduct = 2324,
    //    kRetrieveAllProductsPrices = 3467,
    //    kUpdateProduct = 8753
}RequestTypeTag;


NSString *const RequestForCreateOrder = @"Create/Order";
NSString *const RequestForEditOrder = @"Update/Order";


-(id) initInEditModeWithOrderObject:(OrdersDataModel*)orderObject{
    self = [super init];
    if (!self) return nil;
    _editModeOrderObject = orderObject;
    
    _invoiceDataArray = [[NSMutableArray alloc] init];
    _invoiceDataArray = _editModeOrderObject.orderLinesDataArray;
//    NSLog(@"Original Total Order amount: %@", _editModeOrderObject.orderTotalAmount);
    
    EDIT_MODE = YES;
    return self;
    
};

-(id) initWithCustomerObject:(CustomerDataModel*)customerObject andInvoiceDataArray:(NSMutableArray*)invoiceDataArray{
    self = [super init];
    if(!self) return nil;
    _customerDetails = customerObject;
    _invoiceDataArray = invoiceDataArray;
    EDIT_MODE = NO;
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    CGRect tempFrame;
    orderTotalFooterView = [[OrderTotalFooterVC alloc] init];
    tempFrame = orderTotalFooterView.view.frame;
    tempFrame.size.width = _invoiceTable.frame.size.width;
    tempFrame.origin.x = _invoiceTable.frame.origin.x;
    tempFrame.size.height = ORDER_ITEM_ROW_HEIGHT;
    tempFrame.origin.y = self.view.frame.size.height-tempFrame.size.height;
    
    [orderTotalFooterView.view setFrame:tempFrame];
    [self.view addSubview:orderTotalFooterView.view];
    
    
    
    
    //    NSUInteger totalItems = 0;
    //    CGFloat totalCost = 0.0f;
    //
    //    for(OrderLineDataModel *currentOL in _invoiceDataArray){
    //        if([currentOL.orderLineOriginalTotal floatValue] > 0){
    //            totalItems += [currentOL.orderLineQuantity integerValue];
    //            totalCost += [currentOL.orderLineOriginalTotal floatValue];
    //        }
    //    }
    //    [orderTotalFooterView setTotalItems:totalItems];
    //    [orderTotalFooterView setTotalCost:totalCost];
    
    
    
//    NSUInteger totalItems = 0;
//    CGFloat subTotal = 0.0f, discountTotal = 0.0f;
//    
//    for(OrderLineDataModel *currentOL in _invoiceDataArray){
//        if([currentOL.orderLineOriginalTotal floatValue] > 0){
//            totalItems += [currentOL.orderLineQuantity integerValue];
//            subTotal += [currentOL.orderLineOriginalTotal floatValue];
//            discountTotal += [currentOL.orderLineDiscount floatValue];
//        }
//    }
//    
//    CGFloat totalCost = subTotal-discountTotal;
//    [orderTotalFooterView setTotalItems:totalItems];
//    [orderTotalFooterView setSubTotalCost:subTotal];
//    [orderTotalFooterView setDiscountTotal:discountTotal];
//    [orderTotalFooterView setTotalCost:totalCost];
//    
//    
//    
    
    
    
    
    
    
//    NSLog(@"totalCost : %f, totalCost: %f", [orderTotalFooterView getTotalCost], totalCost);
    [self setNavigationRightButton];
    
    
    tempFrame = _invoiceTable.frame;
    tempFrame.size.height -= orderTotalFooterView.view.frame.size.height;
    _invoiceTable.frame = tempFrame;
    
    
    finalArray = [[NSArray alloc] initWithArray:[AppConfig setupReceiptArrayWithArray:_invoiceDataArray]];
    allDiscountsTotalArray = [[NSMutableArray alloc] initWithArray:[AppConfig getDiscountsArrayFromFinalArray:finalArray]];
    
    
    _invoiceTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //tempFrame = _invoiceTable.frame;
//    tempFrame.size.height -= orderTotalFooterView.view.frame.size.height;
    
//    _invoiceTable.frame = tempFrame;

    
    [self calculateTotalWithItems];
}
-(void)calculateTotalWithItems{
   /* NSUInteger totalItems = 0;
    CGFloat subTotal = 0.0f, discountTotal = 0.0f, finalTotal = 0.0f;
    
    for(OrderLineDataModel *currentOL in [finalArray objectAtIndex:0]){
        if([currentOL.orderLineOriginalTotal floatValue] > 0){
            totalItems += [currentOL.orderLineQuantity integerValue];
            subTotal += [currentOL.orderLineOriginalTotal floatValue];
            discountTotal += [currentOL.orderLineDiscount floatValue];
        }
    }
    finalTotal = subTotal - discountTotal;
    
    NSLog(@"totItems: %d sub: %.2f disTot: %.2f ", totalItems, subTotal, discountTotal);
    for(TotalCrossDiscountDataModel *tCD in allDiscountsTotalArray){
        totalItems += tCD.tCDTotalQuantityValue;
        
        discountTotal += tCD.tCDTotalDiscountValue;
        finalTotal += tCD.tCDTotalFinalValue;
        subTotal += tCD.tCDTotalFinalValue;
    }
        NSLog(@"totItems: %d sub: %.2f disTot: %.2f final: %.2f", totalItems, subTotal, discountTotal, finalTotal);
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setSubTotalCost:subTotal];
    [orderTotalFooterView setDiscountTotal:discountTotal];
    [orderTotalFooterView setTotalCost:finalTotal];
    
    NSLog(@"totalCost : %f, totalCost: %f", [orderTotalFooterView getTotalCost], finalTotal);
*/
    
    
    
    
    
    NSUInteger totalItems = 0;
    CGFloat subTotal = 0.0f, discountTotal = 0.0f, finalTotal = 0.0f;
    
    for(OrderLineDataModel *currentOL in [finalArray objectAtIndex:0]){
        if([currentOL.orderLineOriginalTotal floatValue] > 0){
            totalItems += [currentOL.orderLineQuantity integerValue];
            subTotal += [currentOL.orderLineOriginalTotal floatValue];
            discountTotal += [currentOL.orderLineDiscount floatValue];
        }
    }
    finalTotal = subTotal - discountTotal;
    
    for(TotalCrossDiscountDataModel *tCD in allDiscountsTotalArray){
        totalItems += tCD.tCDTotalQuantityValue;
        
        discountTotal += tCD.tCDTotalDiscountValue;
        finalTotal += tCD.tCDTotalFinalValue;
        subTotal += tCD.tCDTotalFinalValue+tCD.tCDTotalDiscountValue;
    }
    [orderTotalFooterView setTotalItems:totalItems];
    [orderTotalFooterView setSubTotalCost:subTotal];
    [orderTotalFooterView setDiscountTotal:discountTotal];
    [orderTotalFooterView setTotalCost:finalTotal];

}


-(void)setNavigationRightButton{
    if (EDIT_MODE) {
        buttonProcessOrder = [[UIBarButtonItem alloc] initWithTitle:@"Update Order" style:UIBarButtonItemStylePlain target:self action:@selector(updateOrder)];
    }
    else{
        buttonProcessOrder = [[UIBarButtonItem alloc] initWithTitle:@"Process Order" style:UIBarButtonItemStylePlain target:self action:@selector(processOrder)];
    }
    
    self.navigationItem.rightBarButtonItem = buttonProcessOrder;
}

-(void)updateOrder{
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Updating Order: %@", _editModeOrderObject.orderReferenceNumber] maskType:SVProgressHUDMaskTypeBlack];
    NSLog(@"edit update order method called");
    //    [SVProgressHUD showWithStatus:@"Updating Order" maskType:SVProgressHUDMaskTypeBlack];
    
    NSLog(@"Retrieve %@ method called", RequestForEditOrder);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", RequestForEditOrder];
    NSLog(@"url: %@", urlString);
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"    "];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    NSMutableArray *arrayToSend = [[NSMutableArray alloc] init];
    NSMutableDictionary *completeDataArray = [[NSMutableDictionary alloc] init];
    
    for(OrderLineDataModel *o in _invoiceDataArray){
        NSMutableDictionary *dictObject = [[NSMutableDictionary alloc] init]; //could use serialisation
        [dictObject setObject:o.orderLineCode forKey:@"orderLineCode"];
        [dictObject setObject:o.orderLineQuantity forKey:@"orderLineQuantity"];
        [dictObject setObject:o.orderLinePrice forKey:@"orderLinePrice"];
        [dictObject setObject:o.orderLineOriginalTotal forKey:@"orderLineTotal"];
        
        
        
        [dictObject setObject:o.orderLineDiscountDescription forKey:@"orderLineDiscountDescription"];
        [dictObject setObject:o.orderLineDiscount forKey:@"orderLineDiscount"];
        [dictObject setObject:o.orderLineFinalTotalPrice forKey:@"orderLineFinalTotalPrice"];
        [dictObject setObject:o.orderLineLinkingString forKey:@"orderLineLinkingString"];
        
        
        
        
        
        
        
        
        
        [arrayToSend addObject:dictObject];
    }
    [completeDataArray setObject:[NSNumber numberWithInteger:_editModeOrderObject.orderId] forKey:@"orderid"];
    
    [completeDataArray setObject:arrayToSend forKey:@"invoiceitems"];
    
    [completeDataArray setObject:[NSNumber numberWithFloat:[orderTotalFooterView getTotalCost]] forKey:@"totalAmount"];
    
    [completeDataArray setObject:_editModeOrderObject.orderReferenceNumber forKey:@"orderReferenceNumber"];
    
    
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerId] forKey:@"customerid"];
    
    
    [completeDataArray setObject:_editModeOrderObject.orderCustomerName forKey:@"customername"];
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerOrderCount] forKey:@"customerordercount"];
    
    [serverRequest setTag:kTagCreateOrder];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:completeDataArray forKey:@"orderobject"];
    
    NSString *jsonString = [dict JSONRepresentation];
    NSLog(@"edit OrderObject: %@", jsonString);
    
    
    [serverRequest appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestForEditOrder forKey:@"RequestType"]];
    
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
    
}
-(void)processOrder{
    NSLog(@"Process order method called");
    [SVProgressHUD showWithStatus:@"Processing Order" maskType:SVProgressHUDMaskTypeBlack];
    
    NSLog(@"Retrieve %@ method called", RequestForCreateOrder);
    NSString *urlString = [NSString stringWithFormat:@"%@/Secure/CB/%@", @"http://www.riabp.com/CB", RequestForCreateOrder];
    NSLog(@"url: %@", urlString);
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
    serverRequest = nil;
    serverRequest = [ASIFormDataRequest requestWithURL:url];
    [serverRequest addRequestHeader:@"Content-Type" value:@"    "];
    [serverRequest addRequestHeader:@"Request-Method" value:@"POST"];
    
    //    NSString *jsonString = [inputdictionary JSONRepresentation];
    
    NSMutableArray *arrayToSend = [[NSMutableArray alloc] init];
    NSMutableDictionary *completeDataArray = [[NSMutableDictionary alloc] init];
    
    for(OrderLineDataModel *o in _invoiceDataArray){
        NSMutableDictionary *dictObject = [[NSMutableDictionary alloc] init]; //could use serialisation
        
        /*        //Check if there are any duplicates
         for(int i = 0; i < [arrayToSend count]; i++){
         if([[[arrayToSend objectAtIndex:i] objectForKey:@"OrderLineCode"] integerValue] == [o.orderLineCode integerValue]){
         
         int quantity = [o.orderLineQuantity integerValue] + [[[arrayToSend objectAtIndex:i] objectForKey:@"orderLineQuantity"] integerValue];
         CGFloat price = [o.orderLinePrice floatValue] + [[[arrayToSend objectAtIndex:i] objectForKey:@"orderLinePrice"] floatValue];
         CGFloat total = [o.orderLineTotal floatValue] + [[[arrayToSend objectAtIndex:i] objectForKey:@"orderLineTotal"] floatValue];
         
         //product quantity
         [[arrayToSend objectAtIndex:i] setObject:[NSNumber numberWithFloat:quantity] forKey:@"orderLineQuantity"];
         
         //product total sold
         [[arrayToSend objectAtIndex:i] setObject:[NSNumber numberWithFloat:total] forKey:@"orderLineTotal"];
         }
         }
         
         {"orderobject":{"totalAmount":5095.10009765625,"customerid":37,"customername":"Pavan","customerordercount":0,"invoiceitems":[
         
         {"orderLinePrice":3.5,"orderLineTotal":38.5,"orderLineCode":"3333","orderLineQuantity":11},{"orderLinePrice":333,"orderLineTotal":4995,"orderLineCode":"6566","orderLineQuantity":15},{"orderLinePrice":5.5999999046325684,"orderLineTotal":61.599998474121094,"orderLineCode":"3333","orderLineQuantity":11}]}}
         */
        //product quantity
        [dictObject setObject:o.orderLineQuantity forKey:@"orderLineQuantity"];
        
        //product name
        [dictObject setObject:o.orderLineCode forKey:@"orderLineCode"];
        
        
        //product price
        [dictObject setObject:o.orderLinePrice forKey:@"orderLinePrice"];
        
        //product total sold
        [dictObject setObject:o.orderLineOriginalTotal forKey:@"orderLineTotal"];
        
        
        //Discount Description
        [dictObject setObject:o.orderLineDiscountDescription forKey:@"orderLineDiscountDescription"];
        
        
        //Discount
        [dictObject setObject:o.orderLineDiscount forKey:@"orderLineDiscount"];
        
        //Final Total Price
        [dictObject setObject:o.orderLineFinalTotalPrice forKey:@"orderLineFinalTotalPrice"];
        
        /*
         orderLineOrderId;
         
         orderLineQuantity;
         orderLineCode;
         orderLinePrice;
         orderLineOriginalTotal;
         
         orderLineDiscountDescription;
         orderLineDiscount;
         
         orderLineFinalTotalPrice;
         */
        [dictObject setObject:o.orderLineQuantity forKey:@"orderLineQuantity"];
        
        
        
        
        
        
        
        [arrayToSend addObject:dictObject];
    }
    
    [completeDataArray setObject:arrayToSend forKey:@"invoiceitems"];
    
    [completeDataArray setObject:[NSNumber numberWithFloat:[orderTotalFooterView getTotalCost]] forKey:@"totalAmount"];
    
    
    
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerId] forKey:@"customerid"];
    //    [completeDataArray setObject:agentDetails.agentPercentageDiscount forKey:@"customer"];
    
    
    [completeDataArray setObject:_customerDetails.customerName forKey:@"customername"];
    [completeDataArray setObject:[NSNumber numberWithInteger:_customerDetails.customerOrderCount] forKey:@"customerordercount"];
    
    
    
    
    
    /*
     OrderObject:
     {"orderobject" :
     {   "totalAmount":791.5,
     "customerid":37,
     "customername":"Pavan",
     "customerordercount":0,
     "invoiceitems":
     [{  "orderLinePrice":33, "orderLineTotal":396, "orderLineCode":"3333", "orderLineQuantity":12},
     
     {   "orderLinePrice":33,"orderLineTotal":363,"orderLineCode":"3366","orderLineQuantity":11},
     
     {   "orderLinePrice":2.5,"orderLineTotal":32.5,"orderLineCode":"3333","orderLineQuantity":13}]}}
     
     */
    
    
    
    [serverRequest setTag:kTagCreateOrder];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:completeDataArray forKey:@"orderobject"];
    
    //    NSLog(@"OrderObject: %@", dict);
    //        [dict setObject:[NSNumber numberWithInteger:[productQuantity.text intValue]] forKey:@"productquantityperbox"];
    //        [dict setObject:[NSNumber numberWithInteger:[productWeight.text intValue]] forKey:@"productweight"];
    NSString *jsonString = [dict JSONRepresentation];
    NSLog(@"OrderObject: %@", jsonString);
    
    
    [serverRequest appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [serverRequest setUserInfo:[NSDictionary dictionaryWithObject:RequestForCreateOrder forKey:@"RequestType"]];
    
    [serverRequest setDelegate:self];
    [serverRequest setDidFinishSelector:@selector(requestSucceeded:)];
    [serverRequest setDidFailSelector:@selector(requestFailed:)];
    [serverRequest startAsynchronous];
    
}
-(void)requestSucceeded:(ASIHTTPRequest *)request{
    //NSLog(@"Request succeeded");
    NSString *myString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"My string nsdata: %@", myString);
    //    NSLog(@"header information: %@", [request responseHeaders]);
    //    NSLog(@"StatusCode: %@", [[request responseHeaders] objectForKey:@"StatusCode"]);
    NSInteger statusCode = [[[request responseHeaders] objectForKey:@"StatusCode"] intValue];
    switch (statusCode) {
        case 400:
        case 401:
        {
            //            [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
            [SVProgressHUD dismiss];
            
            //NSLog(@"display error message");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[request.responseString JSONValue] objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            break;
        }
        case 200:{
            //{"Error":"0", "Message":"Successfully created agent", "OrderReferenceNumber":' . $referenceCode . '}
            NSDictionary *JSONDictionary = [myString JSONValue];
            [SVProgressHUD dismiss];
            NSLog(@"status code = 200 so successful");
            NSLog(@"200 userInfo: %@", [request userInfo]);
            if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestForCreateOrder]){
                referenceNumberFromServer = [JSONDictionary objectForKey:@"OrderReferenceNumber"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order successful" message:[NSString stringWithFormat:@"Order created"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                alert.tag = 45;
                [alert show];
                [SVProgressHUD dismiss];
            }
            else if([[[request userInfo] objectForKey:@"RequestType"] isEqualToString:RequestForEditOrder]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order edited sucessfully" message:[NSString stringWithFormat:@"Order updated"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                alert.tag = 20;
                [alert show];
                [SVProgressHUD dismiss];
            }
            
            break;
        }
        default:{
            //            [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
            [SVProgressHUD dismiss];
            NSLog(@"went to none userInfo: %@", [request userInfo]);
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Went to none" message:[NSString stringWithFormat:@"went to none: %ld or %@", (long)statusCode, [[request responseHeaders] objectForKey:@"StatusCode"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            [alert show];
            break;
        }
    }
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    if([request tag] == kTagCreateOrder){
        //        [[AppDelegate getAppDelegateReference] dismissLoadingAlert];
        [SVProgressHUD dismiss];
        
        //############# check internet connection over here
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Try again" message:@"Product order not sent, please send again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return ORDERLINE_CELL_ROW_HEIGHT;
    }
    else if(indexPath.section >= 1){
        return 40;
    }
    return 10;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows = 0;
    if(section == 0){
        rows = [[finalArray objectAtIndex:section] count];
    }
    else{
        //+1 for the total cell
        rows = [[finalArray objectAtIndex:section] count]+1;
    }
    return rows;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [finalArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"sec: %d, row: %d", (int)indexPath.section, (int)indexPath.row);
    if([finalArray count] == 0){
        static NSString *cellID = @"loadingcell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if(indexPath.row == 2){
            cell.textLabel.text = [NSString stringWithFormat:@"Loading Products"];
            [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        }
        return cell;
    }
    else if([finalArray count] > 0){
        
        
        if(indexPath.section == 0){
            static NSString *CellIdentifier = @"OrderLineCell";
            ReceiptCell *cell = (ReceiptCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            OrderLineDataModel *oldm = [[finalArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            
            [cell setOrderLineQuantityValue:[NSString stringWithFormat:@"%d", [oldm.orderLineQuantity intValue]]];//
            [cell setOrderLineCodeValue:[NSString stringWithFormat:@"%@", oldm.orderLineCode]];
            [cell setOrderLinePriceValue:[NSString stringWithFormat:@"%.2f",[oldm.orderLinePrice floatValue]]];
            
            [cell setOrderLineTotalValue:[NSString stringWithFormat:@"%@", oldm.orderLineOriginalTotal]];
            [cell setOrderLineDiscountTypeValue:oldm.orderLineDiscountDescription];
            [cell setOrderLineLineTotalValue:[NSString stringWithFormat:@"%.2f", [oldm.orderLineFinalTotalPrice floatValue]]];
            
            return cell;
        }
        
        else{
            NSArray *carray = [finalArray objectAtIndex:indexPath.section];
            if(indexPath.row < [carray count]){
                //            NSLog(@"accessing discount product: %d", (int)indexPath.row);
                static NSString *CellIdentifier = @"DiscountsLineCell";
                DiscountsCell *cell = (DiscountsCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DiscountsCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                //            NSLog(@"accessed item: %d", indexPath.row);
                
                OrderLineDataModel *currentOLDM = [carray objectAtIndex:indexPath.row];
                [cell setDiscountsLineQuantityValue:[NSString stringWithFormat:@"%@", currentOLDM.orderLineQuantity]];
                [cell setDiscountsLineCodeValue:currentOLDM.orderLineCode];
                NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
                [fmt setPositiveFormat:@"0.##"];
                [cell setDiscountsLinePriceValue:[NSString stringWithFormat:@"£%@", currentOLDM.orderLinePrice]];
                [cell setDiscountsLineTotalValue:[NSString stringWithFormat:@"£%@", [fmt stringFromNumber:currentOLDM.orderLineOriginalTotal]]];
                [cell setDiscountsLineDiscountTypeValue:currentOLDM.orderLineDiscountDescription];
                [cell setDiscountsLineLineTotalValue:[NSString stringWithFormat:@"%.2f", [currentOLDM.orderLineFinalTotalPrice floatValue]]];
                return cell;
            }
            //Show total field
            else{
                NSLog(@"");
                static NSString *CellIdentifier = @"TotalCrossDiscountCell";
                TotalCrossDiscountCell *cell = (TotalCrossDiscountCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                cell = (TotalCrossDiscountCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TotalCrossDiscountCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                NSLog(@"discountsTotalArray: %@", allDiscountsTotalArray);
                NSLog(@"discounts Count %d", [allDiscountsTotalArray count]);
                
                TotalCrossDiscountDataModel *currentOLDM = [allDiscountsTotalArray objectAtIndex:indexPath.section-1];
                [cell setTCDTotalDiscountValue:currentOLDM.tCDTotalDiscountValue];
                [cell setTCDTotalFinalValue:currentOLDM.tCDTotalFinalValue];
                [cell setTCDTotalQuantityValue:currentOLDM.tCDTotalQuantityValue];
                return cell;
                
            }
        }
    }
    return nil;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if(section == 0){
//        return ORDER_ITEM_ROW_HEIGHT;
//    }
//    return 0;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        //Only show header when there is one entry row + add product row
        if([_invoiceTable.dataSource tableView:_invoiceTable numberOfRowsInSection:0] > 1){
            return 50;
        }
        return 0;
    }
    else{
        return 50;
    }
    
}


//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if(section != 0){
//        return ROW_SECTION_HEIGHT;
//    }
//    return 0;
//}

/*-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UILabel *qua;
    UILabel *cod;
    UILabel *pri;
    UILabel *tot;
    
    UILabel *discountType;
    
    UILabel *lineTotal;
    int k = 30;
    qua = [[UILabel alloc] initWithFrame:CGRectMake(50, k, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
    cod = [[UILabel alloc] initWithFrame:CGRectMake(150, k, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
    pri = [[UILabel alloc] initWithFrame:CGRectMake(200, k, 70, APP_ORDER_ITEM_HEADER_HEIGHT)];
    //            [pri setBackgroundColor:[UIColor redColor]];
    [pri setTextAlignment:NSTextAlignmentCenter];
    tot = [[UILabel alloc] initWithFrame:CGRectMake(250, k, 100, APP_ORDER_ITEM_HEADER_HEIGHT)];
    discountType = [[UILabel alloc] initWithFrame:CGRectMake(360, k, 200, APP_ORDER_ITEM_HEADER_HEIGHT)];;
    lineTotal = [[UILabel alloc] initWithFrame:CGRectMake(565, k, 200, APP_ORDER_ITEM_HEADER_HEIGHT)];;
    
    [qua setText:@"Qty"];
    [cod setText:@"Code"];
    [pri setText:@"Price"];
    [tot setText:@"Total"];
    [discountType setText:@"- %"];
    [lineTotal setText:@"Line Total"];
    
    
    UIView *hV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, APP_ORDER_ITEM_HEADER_HEIGHT)];
    [hV setBackgroundColor:[UIColor whiteColor]];
    [hV addSubview:qua];
    [hV addSubview:cod];
    [hV addSubview:pri];
    [hV addSubview:tot];
    [hV addSubview:discountType];
    [hV addSubview:lineTotal];
    
    
    
    
    
    
    return hV;
}*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section];
    }
    
    else if(section >= 1){
        return [PreferencesConfig7 tableView:tableView viewForHeaderInSection:section];
    }
    else{
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 45){
        NSLog(@"button index pressed: %d", (int)buttonIndex);
//        [self.navigationController popToRootViewControllerAnimated:YES];
        
        CustomerDetailsReceiptVC *customerDetailsReceiptVC = [[CustomerDetailsReceiptVC alloc] initWithInvoiceDataArray:_invoiceDataArray andReferenceNumber:referenceNumberFromServer];
        //        [receiptVC setCustomerDetails:_customerDM];
        //        [receiptVC setInvoiceDataArray:orderLinesArray];
        [self.navigationController pushViewController:customerDetailsReceiptVC animated:YES];

    }
    else if(alertView.tag == 87){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if(alertView.tag == 20){
        NSLog(@"button index pressed: %d", (int)buttonIndex);
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

@end
